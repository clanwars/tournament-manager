using System.Runtime.Serialization;

namespace Clanwars.TournamentManager.UserStores;

public class UserAlreadyExistsException : ApplicationException
{
    public UserAlreadyExistsException()
    {
    }

    protected UserAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public UserAlreadyExistsException(string? message) : base(message)
    {
    }

    public UserAlreadyExistsException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}
