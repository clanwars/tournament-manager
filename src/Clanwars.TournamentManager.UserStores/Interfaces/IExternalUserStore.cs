using Clanwars.TournamentManager.Lib.Messages.UserStore;

namespace Clanwars.TournamentManager.UserStores.Interfaces;

public interface IExternalUserStore
{
    Task<IEnumerable<UserStoreResponse>> GetUsersAsync(string searchString);
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="firstName"></param>
    /// <param name="lastName"></param>
    /// <param name="email"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    /// <exception cref="UserAlreadyExistsException">User could not be created because the unique information for a user is already existent in the user store</exception>
    Task<UserStoreResponse> CreateUserAsync(string firstName, string lastName, string email, string password);
    
    Task SetUserPasswordAsync(string userId, string password, bool temporary);
}
