using System.Reflection;

namespace Clanwars.TournamentManager.Lib.Helper;

public static class AssemblyExtensions
{
    public static Version? GetTmVersion()
    {
        var version = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion.Split("+")[0];
        return version != null ? new Version(version) : null;
    }
}
