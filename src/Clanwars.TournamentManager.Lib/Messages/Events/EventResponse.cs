namespace Clanwars.TournamentManager.Lib.Messages.Events;

public record EventResponse
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public int ParticipantsCount { get; set; }

    public int TournamentsCount { get; set; }
}

public class EventComparer : IEqualityComparer<EventResponse>
{
    public bool Equals(EventResponse? e1, EventResponse? e2)
    {
        if (e2 is null && e1 is null)
            return true;
        if (e1 is null || e2 is null)
            return false;
        return e1.Id == e2.Id;
    }

    public int GetHashCode(EventResponse ev)
    {
        var hCode = ev.Id;
        return hCode.GetHashCode();
    }
}
