using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Events;

public record JoinEventMessage
{
    [Required]
    public int UserId { get; set; }
}
