namespace Clanwars.TournamentManager.Lib.Messages.News;

public record NewsResponse
{
    public int Id { get; set; }

    public string Headline { get; set; } = null!;

    public string AuthorName { get; set; } = null!;

    public DateTime CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public string Body { get; set; } = null!;
    
    private int? LanpartyScreenId { get; set; }
}
