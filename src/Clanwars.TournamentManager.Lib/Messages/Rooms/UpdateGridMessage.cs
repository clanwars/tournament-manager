using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums.Rooms;

namespace Clanwars.TournamentManager.Lib.Messages.Rooms;

public record UpdateGridMessage
{
    [Required]
    public GridCoordinate Grid { get; set; } = null!;

    [Required]
    public GridSquareRotation Rotation { get; set; }

    [Required]
    public GridSquareType Type { get; set; }
}
