using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Rooms;

public class GridCoordinate
{
    [Required]
    [Range(1, 1024)]
    public int X { get; set; }

    [Required]
    [Range(1, 1024)]
    public int Y { get; set; }
}
