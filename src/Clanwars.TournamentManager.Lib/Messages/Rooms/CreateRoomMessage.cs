using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Rooms;

public record CreateRoomMessage
{
    [Required]
    [MinLength(5)]
    public string Name { get; set; } = null!;

    [Required]
    [Range(1, 1024)]
    public int SizeX { get; set; }

    [Required]
    [Range(1, 1024)]
    public int SizeY { get; set; }

    [Required]
    public int EventId { get; set; }
}
