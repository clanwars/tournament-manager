using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Rooms;

public record PlaceUserMessage
{
    [Required]
    public GridCoordinate Grid { get; set; } = null!;

    public int? UserId { get; set; }
}
