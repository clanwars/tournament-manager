using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Shouts;

public record ShoutResponse
{
    public int Id { get; set; }

    public UserResponse User { get; set; } = null!;

    public string Text { get; set; } = null!;

    public DateTime CreatedAt { get; set; }
}
