﻿using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Admin;

public record UserImportResponse
{
    public List<UserResponse> ImportedUsers { get; set; } = null!;

    public List<UserResponse> SkippedUsers { get; set; } = null!;
}
