namespace Clanwars.TournamentManager.Lib.Messages.Feedback;

public class FeedbackResponse
{
    public int Id { get; set; }

    public string Username { get; set; } = null!;

    public int Rating { get; set; }

    public DateTime CreatedAt { get; set; }

    public string Features { get; set; } = null!;

    public string GoodBad { get; set; } = null!;

    public bool Resolved { get; set; }
}
