using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Feedback;

public class CreateFeedbackMessage
{
    [Required]
    public int Rating { get; set; }

    public string Features { get; set; } = null!;

    public string GoodBad { get; set; } = null!;
}
