using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages;

public record ImageUpload
{
    [Required]
    public string DataUrl { get; init; } = null!;
}
