using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Clans;

public record ClanResponse
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string ClanTag { get; set; } = null!;

    public string ImageUrl { get; set; } = null!;

    public DateTime CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public IList<UserPermissionResponse> Members { get; set; } = new List<UserPermissionResponse>();
}

public static class ClanExtensions
{
    public static bool IsClanOwner(this ClanResponse clan, UserResponse user)
    {
        return clan.Members.Any(m => m.UserId == user.Id && m.Permission == EntityPermission.Owner);
    }

    public static bool HasPendingJoins(this ClanResponse clan)
    {
        return clan.Members.Any(m => m.Permission == EntityPermission.Pending);
    }
}
