using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Clans;

public record ClanUserResponse : UserPermissionResponse
{
    public ClanResponse Clan { get; set; } = null!;
    public int ClanId { get; set; }
}
