using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Clans;

public record CreateClanMessage
{
    [Required]
    [StringLength(255, MinimumLength = 5)]
    public string Name { get; set; } = null!;

    [Required]
    [StringLength(50, MinimumLength = 1)]
    public string ClanTag { get; set; } = null!;
}
