﻿using Clanwars.TournamentManager.Lib.Enums;

namespace Clanwars.TournamentManager.Lib.Messages.Images;

public record ImageResponse
{
    public string HashId { get; set; } = null!;
    public ImageType Type { get; set; }
    public bool? ShowOnStream { get; set; }
}
