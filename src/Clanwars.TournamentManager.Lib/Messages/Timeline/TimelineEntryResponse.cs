namespace Clanwars.TournamentManager.Lib.Messages.Timeline;

public record TimelineEntryResponse
(
    uint Id,
    DateTime StartTime,
    string Duration,
    string Title,
    string? AdditionalInformation
);
