using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.UserStore;

public class CreateUserStoreUserMessage
{
    [Required]
    [StringLength(255, MinimumLength = 3)]
    public string FirstName { get; set; } = null!;

    [Required]
    [StringLength(255, MinimumLength = 3)]
    public string LastName { get; set; } = null!;

    [Required]
    [StringLength(255, MinimumLength = 3)]
    [EmailAddress]
    public string Email { get; set; } = null!;
}
