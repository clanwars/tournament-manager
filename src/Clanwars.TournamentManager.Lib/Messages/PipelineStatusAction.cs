namespace Clanwars.TournamentManager.Lib.Messages;

public enum PipelineStatusAction
{
    Run,
    Stop,
    Skip
}
