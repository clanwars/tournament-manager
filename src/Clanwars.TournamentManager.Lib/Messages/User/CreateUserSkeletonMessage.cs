using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Lib.Messages.User;

public class CreateUserSkeletonMessage
{
    [Required]
    [StringLength(255, MinimumLength = 3)]
    public string FirstName { get; set; } = null!;

    [Required]
    [StringLength(255, MinimumLength = 3)]
    public string LastName { get; set; } = null!;

    [Required]
    [StringLength(255, MinimumLength = 3)]
    [EmailAddress]
    public string Email { get; set; } = null!;
}
