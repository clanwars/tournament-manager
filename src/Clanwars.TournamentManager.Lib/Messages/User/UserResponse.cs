using Clanwars.TournamentManager.Lib.Messages.Clans;
using Clanwars.TournamentManager.Lib.Messages.Events;

namespace Clanwars.TournamentManager.Lib.Messages.User;

public record UserResponse
{
    public int Id { get; set; }

    public string Email { get; set; } = null!;
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Nickname { get; set; } = null!;
    public string? ImageUrl { get; set; }
    public ClanResponse? Clan { get; set; }

    public IList<EventResponse> Events { get; set; } = new List<EventResponse>();

    public DateTime CreatedAt { get; set; }
    
    public DateTime? UpdatedAt { get; set; }
    
    public bool IsIncompleteProfile => FirstName is null or "" || LastName is null or "" || Nickname is null or "";
}
