using Clanwars.TournamentManager.Lib.Enums.Tournaments;
using Clanwars.TournamentManager.Lib.Messages.Events;
using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public class TournamentResponse
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public TournamentMode Mode { get; set; }

    public string? ImageUrl { get; set; }

    public int? PlayerPerTeam { get; set; }

    public IList<TournamentTeamResponse> Teams { get; set; } = null!;

    public TournamentState State { get; set; }

    public IList<TournamentRoundResponse> Rounds { get; set; } = null!;
}

public static class TournamentResponseExtensions
{
    public static TournamentTeamResponse? GetUsersTeam(this TournamentResponse tournament, UserResponse user)
    {
        return tournament.Teams.FirstOrDefault(tt => tt.Members.Any(ttm => ttm.MemberId.Equals(user.Id)));
    }

    public static bool IsSingleplayer(this TournamentResponse tournament)
    {
        return tournament.PlayerPerTeam == 1;
    }

    public static bool IsMultiplayer(this TournamentResponse tournament)
    {
        return tournament.PlayerPerTeam > 1;
    }
}
