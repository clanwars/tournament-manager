using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public record UpdateTournamentMessage
{
    [Required]
    [StringLength(255, MinimumLength = 3)]
    public string Name { get; set; } = null!;

    public TournamentMode? Mode { get; set; }

    public TournamentState? State { get; set; }

    [Range(1, 255)]
    public int? PlayerPerTeam { get; set; }
}
