using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public class TournamentTeamResponse
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string? ImageUrl { get; set; }

    public IList<TournamentTeamMemberResponse> Members { get; set; } = null!;
}

public static class TournamentTeamResponseExtensions
{
    public const string ByeTeamPrefix = "bye::";
    public const string SinglePersonTeamPrefix = "single::";

    public static bool IsOwner(this TournamentTeamResponse tournamentTeam, UserResponse user)
    {
        return tournamentTeam.Members.Any(ttm => ttm.MemberId.Equals(user.Id) && ttm.Permission == EntityPermission.Owner);
    }

    public static bool IsJoinPending(this TournamentTeamResponse tournamentTeam, UserResponse user)
    {
        return tournamentTeam.Members.Any(ttm => ttm.MemberId.Equals(user.Id) && ttm.Permission == EntityPermission.Pending);
    }

    public static bool HasPendingJoins(this TournamentTeamResponse tournamentTeam)
    {
        return tournamentTeam.Members.Any(ttm => ttm.Permission == EntityPermission.Pending);
    }

    public static bool IsByeTeam(this TournamentTeamResponse tournamentTeam)
    {
        return tournamentTeam.Name.StartsWith(ByeTeamPrefix);
    }

    public static bool IsSinglePersonTeam(this TournamentTeamResponse tournamentTeam)
    {
        return tournamentTeam.Name.StartsWith(SinglePersonTeamPrefix);
    }

    public static string GetTeamName(this TournamentTeamResponse tournamentTeam)
    {
        return tournamentTeam.IsSinglePersonTeam() ? tournamentTeam.Members.First().Member.Nickname : tournamentTeam.Name;
    }

    public static TournamentTeamMemberResponse? GetTournamentTeamMember(this TournamentTeamResponse tournamentTeam, int userId)
    {
        return tournamentTeam.Members.SingleOrDefault(ttm => ttm.Id == userId);
    }
}
