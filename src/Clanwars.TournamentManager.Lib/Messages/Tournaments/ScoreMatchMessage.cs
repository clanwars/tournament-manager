using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public record ScoreMatchMessage
{
    [Required]
    [MinLength(2)]
    public IList<SetTeamScore> Scores { get; set; } = new List<SetTeamScore>();
}
