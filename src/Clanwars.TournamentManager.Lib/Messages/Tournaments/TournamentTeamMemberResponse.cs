using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public class TournamentTeamMemberResponse
{
    public int Id { get; set; }

    public TournamentTeamResponse TournamentTeam { get; set; } = null!;

    public UserResponse Member { get; set; } = null!;

    public int MemberId { get; set; }

    public EntityPermission Permission { get; set; }
}
