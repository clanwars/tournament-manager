using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public record UpdateTournamentTeamMessage
{
    [Required]
    [StringLength(255, MinimumLength = 5)]
    public string Name { get; set; } = null!;

    [Required]
    public int TournamentId { get; set; }
}
