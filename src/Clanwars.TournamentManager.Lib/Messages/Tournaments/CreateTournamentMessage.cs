using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public class CreateTournamentMessage
{
    [Required]
    [StringLength(255, MinimumLength = 3)]
    public string Name { get; set; } = null!;

    [Required]
    public int EventId { get; set; }

    [Required]
    public TournamentMode Mode { get; set; }

    public int? PlayerPerTeam { get; set; }
}
