using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public record SetTeamScore
{
    [Required]
    public int TournamentTeamId { get; set; }

    [Required]
    [Range(0, int.MaxValue)]
    public int Score { get; set; }
}
