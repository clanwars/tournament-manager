using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public class TournamentMatchResponse
{
    public int Id { get; set; }

    public int MatchIndex { get; set; }

    public IList<TournamentMatchScoreResponse> Scores { get; set; } = null!;
}

public static class TournamentMatchResponseExtensions
{
    public static bool IsScoreable(this TournamentMatchResponse tournamentMatchResponse)
    {
        return tournamentMatchResponse.Scores.All(s => s.Score is null && s.TournamentTeam != null);
    }
    
    public static bool IsFinished(this TournamentMatchResponse tournamentMatchResponse)
    {
        return tournamentMatchResponse.Scores.All(s => s.Score is not null && s.TournamentTeam is not null);
    }

    public static bool IsUsersMatch(this TournamentMatchResponse tournamentMatchResponse, UserResponse user)
    {
        return tournamentMatchResponse.Scores.Any(s => s.TournamentTeam != null && s.TournamentTeam.Members.Any(m => m.Member.Id.Equals(user.Id)));
    }
    
    public static string GetNameOnPlace(this TournamentMatchResponse tournamentMatchScoreResponses, int placeNumber)
    {
        if (!tournamentMatchScoreResponses.IsFinished())
        {
            return string.Empty;
        }
        
        var tournamentTeam = tournamentMatchScoreResponses.Scores.OrderByDescending(s => s.Score).Skip(placeNumber - 1).FirstOrDefault()?.TournamentTeam;

        if (tournamentTeam is null)
        {
            return string.Empty;
        }
        
        if (tournamentTeam.Members.Count == 1)
        {
            return tournamentTeam.Members.First().Member.Nickname;
        }

        return tournamentTeam.Name;
    }
}
