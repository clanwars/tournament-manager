using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Pipeline;

public record UpdateModuleTimeoutsMessage
{
    [Required]
    [Range(10, 3600)]
    public int TimeoutInitial { get; set; }

    [Required]
    [Range(10, 3600)]
    public int TimeoutRecurring { get; set; }
}
