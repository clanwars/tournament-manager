namespace Clanwars.TournamentManager.Lib.Messages.Pipeline;

public enum ModuleType
{
    News,
    Shouts,
    Tournaments,
    Matches,
    MatchResults,
    MatchResult,
    Images,
    WelcomeMessage
}
