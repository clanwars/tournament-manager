namespace Clanwars.TournamentManager.Lib.Messages.Pipeline;

public record PipelineModuleStateResponse
{
    public ModuleType Module { get; set; }
    public bool IsEnabled { get; set; } = true;
    public int TimeoutInitial { get; set; }
    public int TimeoutRecurring { get; set; }
}
