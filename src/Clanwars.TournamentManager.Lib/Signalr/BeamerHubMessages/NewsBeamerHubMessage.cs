using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

public record NewsBeamerHubMessage(int Id, bool IsNew = true) : IBeamerHubMessage
{
    public ModuleType ModuleType { get; init; } = ModuleType.News;
}
