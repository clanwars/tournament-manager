using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

public record TournamentMatchResultBeamerHubMessage(int TournamentId, int TournamentMatchId, bool IsNew = true) : IBeamerHubMessage
{
    public ModuleType ModuleType { get; init; } = ModuleType.MatchResult;
}
