using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

public interface IBeamerHubMessage
{
    public bool IsNew { get; init; }
    public ModuleType ModuleType { get; init; }
}
