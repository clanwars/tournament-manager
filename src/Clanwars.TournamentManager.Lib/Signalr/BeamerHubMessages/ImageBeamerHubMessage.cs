using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

public record ImageBeamerHubMessage(string ImageHashId, bool IsNew = true, int? DisplayTimeout = null) : IBeamerHubMessage
{
    public ModuleType ModuleType { get; init; } = ModuleType.Images;
}
