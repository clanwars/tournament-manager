using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

public record UpcomingMatchesBeamerHubMessage(int TournamentId, bool IsNew = true) : IBeamerHubMessage
{
    public ModuleType ModuleType { get; init; } = ModuleType.Matches;
}
