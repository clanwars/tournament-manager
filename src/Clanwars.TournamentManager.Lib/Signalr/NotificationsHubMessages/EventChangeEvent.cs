namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public enum EventChangeEvent
{
    Created,
    Changed,
    Deleted
}
