namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public record EventHubMessage : AbstractHubMessage
{
    public EventChangeEvent Event { get; init; }
}
