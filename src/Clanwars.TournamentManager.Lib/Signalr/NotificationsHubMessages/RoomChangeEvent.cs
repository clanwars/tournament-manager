namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public enum RoomChangeEvent
{
    Created,
    Changed,
    Deleted
}
