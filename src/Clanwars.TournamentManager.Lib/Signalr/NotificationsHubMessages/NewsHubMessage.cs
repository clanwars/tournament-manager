namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public record NewsHubMessage : AbstractHubMessage
{
    public NewsChangeEvent Event { get; init; }
}
