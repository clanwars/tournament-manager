namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public enum UserChangeEvent
{
    Created,
    Changed,
    Deleted
}
