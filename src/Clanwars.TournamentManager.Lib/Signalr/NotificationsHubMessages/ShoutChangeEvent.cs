namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public enum ShoutChangeEvent
{
    Created,
    Changed,
    Deleted
}
