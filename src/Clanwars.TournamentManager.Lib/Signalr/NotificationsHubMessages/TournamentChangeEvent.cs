namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public enum TournamentChangeEvent
{
    Created,
    Changed,
    Deleted,
    OpenForRegistration,
    Started,
    Finished,
    MatchFinished,
    MatchWaitingForYou,
    TeamCreated,
    TeamDeleted,
    TeamMembersChanged
}
