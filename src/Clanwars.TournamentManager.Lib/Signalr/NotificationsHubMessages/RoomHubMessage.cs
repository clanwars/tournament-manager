namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public record RoomHubMessage : AbstractHubMessage
{
    public RoomChangeEvent Event { get; init; }
}
