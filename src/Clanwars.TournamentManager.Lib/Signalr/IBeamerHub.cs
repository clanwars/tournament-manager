using Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

namespace Clanwars.TournamentManager.Lib.Signalr;

public interface IBeamerHub
{
    Task PublishNews(NewsBeamerHubMessage msg);
    Task PublishWelcome(WelcomeBeamerHubMessage msg);
    Task PublishShout(ShoutBeamerHubMessage msg);
    Task PublishImage(ImageBeamerHubMessage msg);
    Task PublishTournaments(TournamentBeamerHubMessage msg);
    Task PublishUpcomingMatches(UpcomingMatchesBeamerHubMessage msg);
    Task PublishFinishedMatches(FinishedMatchesBeamerHubMessage msg);
    Task PublishTournamentMatchResult(TournamentMatchResultBeamerHubMessage msg);
    Task AutoplayState(bool autoplayActive);
}
