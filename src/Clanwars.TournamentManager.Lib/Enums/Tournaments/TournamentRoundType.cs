using System.Text.Json.Serialization;

namespace Clanwars.TournamentManager.Lib.Enums.Tournaments;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum TournamentRoundType
{
    WinnerBracket,
    LoserBracket,
    Final,
}
