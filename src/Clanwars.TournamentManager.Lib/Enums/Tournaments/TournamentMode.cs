namespace Clanwars.TournamentManager.Lib.Enums.Tournaments;

public enum TournamentMode
{
    SingleElimination = 0,
    DoubleElimination = 1,
    Points = 2,
}
