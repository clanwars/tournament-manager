using System.Text.Json.Serialization;

namespace Clanwars.TournamentManager.Lib.Enums.Rooms;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum GridSquareType
{
    SeatDefault = 20,
    SeatTechSupport = 21,
    SeatTournamentSupport = 22,
    SeatCashier = 23,

    Floor = 1,
    WallLine = 2,
    WallT = 3,
    WallX = 4,
    WallCorner = 5,
    BarCounterLine = 6,
    BarCounterCorner = 7,
    BarIcon = 8,
    BeamerScreen = 9,
    StageCorner = 10,
    StageLine = 11,
    Stairs = 12,
    Door = 13,
    SleepIcon = 14
}
