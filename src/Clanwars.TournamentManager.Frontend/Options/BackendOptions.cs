namespace Clanwars.TournamentManager.Frontend.Options;

public record BackendOptions
{
    public const string OptionsKey = "Backend";

    public string BaseAddress { get; set; } = null!;

    public string? VideosBaseAddress { get; set; }
}
