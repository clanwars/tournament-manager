using System.Security.Claims;
using System.Text.Json;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication.Internal;

namespace Clanwars.TournamentManager.Frontend.Helpers;

/// <summary>
/// i copied this from the following blogpost: https://medium.com/@marcodesanctis2/role-based-security-with-blazor-and-identity-server-4-aba12da70049
/// </summary>
/// <typeparam name="TAccount"></typeparam>
public class ArrayClaimsPrincipalFactory<TAccount> : AccountClaimsPrincipalFactory<TAccount> where TAccount : RemoteUserAccount
{
    public ArrayClaimsPrincipalFactory(IAccessTokenProviderAccessor accessor)
        : base(accessor)
    {
    }

    // when a user belongs to multiple roles, IS4 returns a single claim with a serialised array of values
    // this class improves the original factory by deserializing the claims in the correct way
    public override async ValueTask<ClaimsPrincipal> CreateUserAsync(TAccount? account, RemoteAuthenticationUserOptions options)
    {
        var user = await base.CreateUserAsync(account!, options);

        if (account == null) return user;

        var claimsIdentity = (ClaimsIdentity) user.Identity!;


        foreach (var (name, value) in account.AdditionalProperties)
        {
            if (value is not JsonElement {ValueKind: JsonValueKind.Array} element) continue;

            claimsIdentity.RemoveClaim(claimsIdentity.FindFirst(name));

            var claims = element.EnumerateArray().Select(x => new Claim(name, x.ToString()));

            claimsIdentity.AddClaims(claims);
        }

        return user;
    }
}
