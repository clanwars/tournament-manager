using Clanwars.TournamentManager.Lib.Enums.Rooms;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace Clanwars.TournamentManager.Frontend.Shared.Components.Rooms.RoomGridSquares.Base;

public abstract class RoomGridSquareBase : MudComponentBase
{
    [Parameter]
    public GridSquareRotation Rotation { get; set; }
}
