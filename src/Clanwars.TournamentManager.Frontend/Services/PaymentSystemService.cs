using System.Net;
using System.Text.Json;
using System.Web;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;
using Clanwars.TournamentManager.Lib;
using MudBlazor;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

internal class PaymentSystemService(HttpClient httpClient, ISnackbar snackbar) : AbstractHttpService(httpClient, snackbar), IPaymentSystemService
{
    private const string BasePath = "api/v1/PaymentSystem";

    public Task<IPagedResult<TransactionResponse>> GetTransactionsAsync(SieveModel model)
    {
        return GetPagedAsync<TransactionResponse>(BasePath + "/current-transactions/me", model);
    }

    public Task<AccountInfo> GetAccountAsync(Guid id)
    {
        return GetAsync<AccountInfo>(BasePath + "/accounts?accountId=" + HttpUtility.UrlEncode(id.ToString()))!;
    }

    public async Task<AccountInfo?> GetAccountByEmailAsync(string email)
    {
        try
        {
            return await GetAsync<AccountInfo>(BasePath + "/accounts?email=" + HttpUtility.UrlEncode(email));
        }
        catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
        {
            return null;
        }
    }

    public async Task<AccountInfo?> GetMyAccount()
    {
        try
        {
            return await GetAsync<AccountInfo>(BasePath + "/accounts/me");
        }
        catch (JsonException)
        {
            return null;
        }
    }

    public Task<IList<PointOfSaleInfoWithCurrentCard>> GetCurrentCards()
    {
        return GetAsync<IList<PointOfSaleInfoWithCurrentCard>>(BasePath + "/points-of-sale/current-cards")!;
    }

    public Task<AccountInfo> CreateAsync(CreateAccountMessage createAccountMessage)
    {
        return PostAsync<CreateAccountMessage, AccountInfo>(BasePath + "/accounts", createAccountMessage);
    }

    public Task<AccountInfo> UpdateAsync(Guid id, UpdateAccountMessage updateAccountMessage)
    {
        return PutAsync<UpdateAccountMessage, AccountInfo>(BasePath + "/accounts/" + id, updateAccountMessage);
    }
}
