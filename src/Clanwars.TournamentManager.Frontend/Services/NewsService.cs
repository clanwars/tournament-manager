using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.News;
using MudBlazor;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public class NewsService : AbstractHttpService, INewsService
{
    private const string BasePath = "api/v1/news";

    public NewsService(HttpClient httpClient, ISnackbar snackbar) : base(httpClient, snackbar)
    {
    }

    public Task<IPagedResult<NewsResponse>> GetAsync(SieveModel model)
    {
        return GetPagedAsync<NewsResponse>(BasePath, model);
    }

    public async Task<NewsResponse> GetAsync(int id)
    {
        var pagedResult = await GetPagedAsync<NewsResponse>(BasePath, new SieveModel { Filters = $"Id=={id}", PageSize = 1 });
        if (pagedResult.TotalCount != 1)
        {
            throw new ArgumentException($"Cannot find news with id {id}", nameof(id));
        }

        return pagedResult.Results.Single();
    }

    public Task<IList<NewsResponse>> GetAllAsync(SieveModel model)
    {
        return GetAllAsync<NewsResponse>(BasePath, model.Filters, model.Sorts);
    }

    public Task<NewsResponse> CreateAsync(CreateNewsMessage createNewsMessage)
    {
        return PostAsync<CreateNewsMessage, NewsResponse>(BasePath, createNewsMessage);
    }

    public Task<NewsResponse> UpdateAsync(int newsId, UpdateNewsMessage updateNewsMessage)
    {
        return PutAsync<UpdateNewsMessage, NewsResponse>($"{BasePath}/{newsId}", updateNewsMessage);
    }

    public Task DeleteAsync(int newsId)
    {
        return DeleteAsync($"{BasePath}/{newsId}");
    }
}
