using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Rooms;
using MudBlazor;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public class RoomsService : AbstractHttpService, IRoomsService
{
    private const string BasePath = "api/v1/rooms";

    public RoomsService(HttpClient httpClient, ISnackbar snackbar) : base(httpClient, snackbar)
    {
    }

    public Task<IPagedResult<RoomResponse>> GetAsync(SieveModel model)
    {
        return GetPagedAsync<RoomResponse>(BasePath, model);
    }

    public async Task<RoomResponse> GetAsync(int id)
    {
        var pagedResult = await GetPagedAsync<RoomResponse>(BasePath, new SieveModel {Filters = $"Id=={id}", PageSize = 1});
        if (pagedResult.TotalCount != 1)
        {
            throw new ArgumentException($"Cannot find room with id {id}", nameof(id));
        }

        return pagedResult.Results.Single();
    }

    public Task<IList<RoomResponse>> GetAllAsync(SieveModel model)
    {
        return GetAllAsync<RoomResponse>(BasePath, model.Filters, model.Sorts);
    }

    public Task<RoomResponse> CreateAsync(CreateRoomMessage createRoomMessage)
    {
        return PostAsync<CreateRoomMessage, RoomResponse>(BasePath, createRoomMessage);
    }

    public Task<RoomResponse> CopyAsync(int roomIdToCopy, CopyRoomMessage copyRoomMessage)
    {
        return PostAsync<CopyRoomMessage, RoomResponse>($"{BasePath}/{roomIdToCopy}/copy", copyRoomMessage);
    }

    public Task<RoomResponse> UpdateAsync(int roomId, UpdateRoomMessage updateRoomMessage)
    {
        return PutAsync<UpdateRoomMessage, RoomResponse>($"{BasePath}/{roomId}", updateRoomMessage);
    }

    public Task DeleteAsync(int roomId)
    {
        return DeleteAsync($"{BasePath}/{roomId}");
    }

    public Task<GridSquareResponse> PlaceUserAsync(int roomId, PlaceUserMessage placeUserMessage)
    {
        return PutAsync<PlaceUserMessage, GridSquareResponse>($"{BasePath}/{roomId}/place-user", placeUserMessage);
    }

    public Task<GridSquareResponse> UpdateGridSquareAsync(int roomId, UpdateGridMessage updateGridMessage)
    {
        return PutAsync<UpdateGridMessage, GridSquareResponse>($"{BasePath}/{roomId}/update-grid", updateGridMessage);
    }

    public Task DeleteGridSquareAsync(int roomId, GridCoordinate deleteGridMessage)
    {
        return DeleteAsync($"{BasePath}/{roomId}/grid");
    }
}
