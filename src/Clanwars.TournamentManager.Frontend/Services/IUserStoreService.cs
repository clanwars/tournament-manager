using Clanwars.TournamentManager.Lib.Messages.Admin;
using Clanwars.TournamentManager.Lib.Messages.UserStore;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IUserStoreService
{
    Task<IEnumerable<UserStoreResponse>> FindExternalUsers(string search);
    Task<UserStoreResponse> Create(string email, string firstName, string lastName);
    Task ResetPassword(int userId);
    Task<UserImportResponse> ReimportUsersToIdpAsync();
}
