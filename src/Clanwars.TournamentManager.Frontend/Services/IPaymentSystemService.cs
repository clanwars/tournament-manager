using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IPaymentSystemService
{
    Task<IPagedResult<TransactionResponse>> GetTransactionsAsync(SieveModel model);
    Task<AccountInfo> GetAccountAsync(Guid id);
    Task<AccountInfo?> GetAccountByEmailAsync(string email);
    Task<AccountInfo?> GetMyAccount();
    Task<IList<PointOfSaleInfoWithCurrentCard>> GetCurrentCards();
    Task<AccountInfo> CreateAsync(CreateAccountMessage createAccountMessage);
    Task<AccountInfo> UpdateAsync(Guid id, UpdateAccountMessage updateAccountMessage);
}
