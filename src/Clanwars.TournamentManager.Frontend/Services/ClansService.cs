using Clanwars.TournamentManager.Lib.Messages.Clans;
using MudBlazor;

namespace Clanwars.TournamentManager.Frontend.Services;

public class ClansService(HttpClient httpClient, ISnackbar snackbar) : AbstractHttpService(httpClient, snackbar), IClansService
{
    private const string BasePath = "api/v1/clans";

    public async Task<ClanResponse> CreateClanAsync(CreateClanMessage createClanMessage)
    {
        var newClan = await PostAsync<CreateClanMessage, ClanResponse>($"{BasePath}", createClanMessage);
        return newClan;
    }

    public async Task<ClanResponse> UpdateClanAsync(int clanId, UpdateClanMessage updateClanMessage)
    {
        return await PutAsync<UpdateClanMessage, ClanResponse>($"{BasePath}/{clanId}", updateClanMessage);
    }

    public Task<IList<ClanResponse>> GetClansAsync()
    {
        return GetAllAsync<ClanResponse>(BasePath);
    }

    public async Task<ClanResponse> JoinClanAsync(int clanId)
    {
        return await PutAsync<ClanResponse>($"{BasePath}/{clanId}/join");
    }

    public async Task<ClanResponse> ApproveJoinAsync(int clanId, int targetUserId)
    {
        return await PutAsync<ClanResponse>($"{BasePath}/{clanId}/approve-join/{targetUserId}");
    }

    public async Task LeaveClanAsync(int clanId)
    {
        await PutAsync($"{BasePath}/{clanId}/leave");
    }

    public async Task RemoveUserFromClanAsync(int clanId, int targetUserId)
    {
        await PutAsync($"{BasePath}/{clanId}/remove-user/{targetUserId}");
    }

    public async Task RevokeOwnershipAsync(int clanId, int targetUserId)
    {
        await PutAsync($"{BasePath}/{clanId}/revoke-ownership/{targetUserId}");
    }

    public async Task GrantOwnershipAsync(int clanId, int targetUserId)
    {
        await PutAsync($"{BasePath}/{clanId}/grant-ownership/{targetUserId}");
    }
}
