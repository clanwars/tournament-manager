using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Events;
using MudBlazor;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public class EventsService : AbstractHttpService, IEventsService
{
    private EventResponse? _currentEvent;

    private const string BasePath = "api/v1/events";

    public EventsService(HttpClient httpClient, ISnackbar snackbar) : base(httpClient, snackbar)
    {
    }

    public async Task<EventResponse?> CurrentEventAsync(bool forceReload = false)
    {
        if (_currentEvent != null && !forceReload)
        {
            return _currentEvent;
        }

        var result = await GetPagedAsync<EventResponse>(BasePath, new SieveModel { Filters = $"EndDate>={DateTime.Now:O}", Sorts = "EndDate" });
        if (result.TotalCount == 0)
        {
            return null;
        }

        _currentEvent = result.Results[0];
        return _currentEvent;
    }

    public async Task<EventResponse?> GetEvent(int eventId)
    {
        var pagedResult = await GetPagedAsync<EventResponse>(BasePath, new SieveModel { Filters = $"Id=={eventId}", PageSize = 1});
        if (pagedResult.TotalCount != 1)
        {
            throw new ArgumentException($"Cannot find news with id {eventId}", nameof(eventId));
        }

        return pagedResult.Results.Single();
    }

    public Task<IPagedResult<EventResponse>> GetAsync(SieveModel model)
    {
        return GetPagedAsync<EventResponse>(BasePath, model);
    }

    public Task<IList<EventResponse>> GetAllAsync(SieveModel model)
    {
        return GetAllAsync<EventResponse>(BasePath, model.Filters, model.Sorts);
    }

    public Task<EventResponse> CreateAsync(CreateEventMessage createEventMessage)
    {
        return PostAsync<CreateEventMessage, EventResponse>(BasePath, createEventMessage);
    }

    public Task<EventResponse> UpdateAsync(int eventId, UpdateEventMessage updateEventMessage)
    {
        return PutAsync<UpdateEventMessage, EventResponse>($"{BasePath}/{eventId}", updateEventMessage);
    }

    public Task DeleteAsync(int eventId)
    {
        return DeleteAsync($"{BasePath}/{eventId}");
    }

    public Task JoinEventAsync(int eventId, JoinEventMessage joinEventMessage)
    {
        return PutAsync($"{BasePath}/{eventId}/join", joinEventMessage);
    }

    public Task LeaveEventAsync(int eventId, LeaveEventMessage leaveEventMessage)
    {
        return PutAsync($"{BasePath}/{eventId}/leave", leaveEventMessage);
    }
}
