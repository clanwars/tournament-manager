using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Shouts;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IShoutsService
{
    Task<IList<ShoutResponse>> GetAllAsync(SieveModel model);
    Task<IPagedResult<ShoutResponse>> GetAsync(SieveModel model);
    Task<ShoutResponse> GetAsync(int id);

    Task<ShoutResponse> CreateAsync(CreateShoutMessage createShoutMessage);

    Task<ShoutResponse> UpdateAsync(int shoutId, UpdateShoutMessage updateShoutMessage);

    Task DeleteAsync(int shoutId);
}
