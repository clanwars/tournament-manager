using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.User;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IUsersService
{
    Task<UserResponse> GetAsync(int id);
    Task<IPagedResult<UserResponse>> GetAsync(SieveModel model);
    Task<IList<UserResponse>> GetAllAsync(SieveModel model);
    Task<UserResponse> Update(int userId, string firstName, string lastName, string nickname);
    Task<UserResponse> CreateUserSkeletonAsync(string email, string firstName, string lastName);
}
