using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface ICurrentUserService
{
    Task<UserResponse> GetCurrentUser();
    Task<bool> CurrentUserIsManager();
}
