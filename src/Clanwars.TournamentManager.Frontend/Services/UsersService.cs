using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.User;
using MudBlazor;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public class UsersService : AbstractHttpService, IUsersService
{
    private const string BasePath = "api/v1/users";

    public UsersService(HttpClient httpClient, ISnackbar snackbar) : base(httpClient, snackbar)
    {
    }

    public Task<IPagedResult<UserResponse>> GetAsync(SieveModel model)
    {
        return GetPagedAsync<UserResponse>(BasePath, model);
    }

    public async Task<UserResponse> GetAsync(int id)
    {
        var pagedResult = await GetPagedAsync<UserResponse>(BasePath, new SieveModel {Filters = $"Id=={id}", PageSize = 1});
        if (pagedResult.TotalCount != 1)
        {
            throw new ArgumentException($"Cannot find user with id {id}", nameof(id));
        }

        return pagedResult.Results.Single();
    }

    public Task<IList<UserResponse>> GetAllAsync(SieveModel model)
    {
        return GetAllAsync<UserResponse>(BasePath, model.Filters, model.Sorts);
    }

    public async Task<UserResponse> Update(int userId, string firstName, string lastName, string nickname)
    {
        var userUpdateMessage = new UpdateUserMessage
        {
            FirstName = firstName,
            LastName = lastName,
            Nickname = nickname
        };
        return await PutAsync<UpdateUserMessage, UserResponse>($"{BasePath}/me", userUpdateMessage);
    }

    public async Task<UserResponse> CreateUserSkeletonAsync(string email, string firstName, string lastName)
    {
        var createUserSkeletonMessage = new CreateUserSkeletonMessage
        {
            FirstName = firstName,
            LastName = lastName,
            Email = email
        };
        return await PostAsync<CreateUserSkeletonMessage, UserResponse>($"{BasePath}", createUserSkeletonMessage);
    }
}
