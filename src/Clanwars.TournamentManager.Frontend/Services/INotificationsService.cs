using Clanwars.TournamentManager.Frontend.Options;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Options;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface INotificationsService
{
    public event Func<object, Version?, Task>? TmVersionChanged;
    public event Func<object, EventHubMessage, Task>? EventsChanged;
    public event Func<object, NewsHubMessage, Task>? NewsChanged;
    public event Func<object, TournamentHubMessage, Task>? TournamentsChanged;
    public event Func<object, ClanHubMessage, Task>? ClansChanged;
    public event Func<object, RoomHubMessage, Task>? RoomsChanged;
    public event Func<object, UserHubMessage, Task>? UserChanged;
    public event Func<object, ShoutHubMessage, Task>? ShoutsChanged;
}

internal sealed class NotificationsService : INotificationsService, IDisposable
{
    public event Func<object, Version?, Task>? TmVersionChanged;
    public event Func<object, EventHubMessage, Task>? EventsChanged;
    public event Func<object, NewsHubMessage, Task>? NewsChanged;
    public event Func<object, TournamentHubMessage, Task>? TournamentsChanged;
    public event Func<object, ClanHubMessage, Task>? ClansChanged;
    public event Func<object, RoomHubMessage, Task>? RoomsChanged;
    public event Func<object, UserHubMessage, Task>? UserChanged;
    public event Func<object, ShoutHubMessage, Task>? ShoutsChanged;

    private readonly HubConnection _hubConnection;

    public NotificationsService(
        IOptions<BackendOptions> backendOptions
    )
    {
        _hubConnection = new HubConnectionBuilder()
            .WithUrl(backendOptions.Value.BaseAddress.TrimEnd('/') + "/notifications")
            .WithAutomaticReconnect()
            .Build();

        _hubConnection.On<Version?>(nameof(INotificationsSender.SendTmVersion), async msg => { await InvokeSubscribers(TmVersionChanged, msg); });
        _hubConnection.On<EventHubMessage>(nameof(INotificationsSender.SendEventsChanged), async msg => { await InvokeSubscribers(EventsChanged, msg); });
        _hubConnection.On<NewsHubMessage>(nameof(INotificationsSender.SendNewsChanged), async msg => { await InvokeSubscribers(NewsChanged, msg); });
        _hubConnection.On<TournamentHubMessage>(nameof(INotificationsSender.SendTournamentsChanged), async msg => { await InvokeSubscribers(TournamentsChanged, msg); });
        _hubConnection.On<ClanHubMessage>(nameof(INotificationsSender.SendClansChanged), async msg => { await InvokeSubscribers(ClansChanged, msg); });
        _hubConnection.On<RoomHubMessage>(nameof(INotificationsSender.SendRoomsChanged), async msg => { await InvokeSubscribers(RoomsChanged, msg); });
        _hubConnection.On<UserHubMessage>(nameof(INotificationsSender.SendUserChanged), async msg => { await InvokeSubscribers(UserChanged, msg); });
        _hubConnection.On<ShoutHubMessage>(nameof(INotificationsSender.SendShoutsChanged), async msg => { await InvokeSubscribers(ShoutsChanged, msg); });

        _hubConnection.StartAsync();
    }

    public void Dispose()
    {
        _hubConnection.StopAsync();
    }

    private async Task InvokeSubscribers<T1>(Func<object, T1, Task>? eventHandler, T1 eventMessage)
    {
        if (eventHandler == null)
        {
            return;
        }

        // debounce event propagation a little so not all clients rush the backend at the same time
        var rnd = new Random(DateTime.Now.Millisecond);
        Thread.Sleep(rnd.Next(0, 500));

        var invocationList = eventHandler.GetInvocationList();
        var handlerTasks = new Task[invocationList.Length];

        for (var i = 0; i < invocationList.Length; i++)
        {
            handlerTasks[i] = ((Func<object, T1, Task>)invocationList[i])(this, eventMessage);
        }

        await Task.WhenAll(handlerTasks);
    }
}
