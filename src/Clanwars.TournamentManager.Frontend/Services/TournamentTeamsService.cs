using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using MudBlazor;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public class TournamentTeamsService : AbstractHttpService, ITournamentTeamsService
{
    private const string BasePath = "api/v1/tournamentTeams";

    public TournamentTeamsService(HttpClient httpClient, ISnackbar snackbar) : base(httpClient, snackbar)
    {
    }

    public Task<IPagedResult<TournamentTeamResponse>> GetAsync(SieveModel model)
    {
        return GetPagedAsync<TournamentTeamResponse>(BasePath, model);
    }

    public async Task<TournamentTeamResponse> GetAsync(int id)
    {
        var pagedResult = await GetPagedAsync<TournamentTeamResponse>(BasePath, new SieveModel { Filters = $"Id=={id}", PageSize = 1});
        if (pagedResult.TotalCount != 1)
        {
            throw new ArgumentException($"Cannot find tournament team with id {id}", nameof(id));
        }

        return pagedResult.Results.Single();
    }

    public Task<IList<TournamentTeamResponse>> GetAllAsync(SieveModel model)
    {
        return GetAllAsync<TournamentTeamResponse>(BasePath, model.Filters, model.Sorts);
    }

    public Task<TournamentTeamResponse> CreateAsync(CreateTournamentTeamMessage createTournamentTeamMessage)
    {
        return PostAsync<CreateTournamentTeamMessage, TournamentTeamResponse>(BasePath, createTournamentTeamMessage);
    }

    public Task<TournamentTeamResponse> UpdateAsync(int tournamentTeamId, UpdateTournamentTeamMessage updateTournamentTeamMessage)
    {
        return PutAsync<UpdateTournamentTeamMessage, TournamentTeamResponse>($"{BasePath}/{tournamentTeamId}", updateTournamentTeamMessage);
    }

    public Task DeleteAsync(int tournamentTeamId)
    {
        return DeleteAsync($"{BasePath}/{tournamentTeamId}");
    }

    public Task<TournamentTeamResponse> JoinAsync(int tournamentTeamId)
    {
        return PostAsync<TournamentTeamResponse>($"{BasePath}/{tournamentTeamId}/join");
    }

    public Task LeaveAsync(int tournamentTeamId)
    {
        return PostAsync($"{BasePath}/{tournamentTeamId}/leave");
    }

    public Task<TournamentTeamResponse> ApproveJoinAsync(int tournamentTeamId, int targetUserId)
    {
        return PutAsync<TournamentTeamResponse>($"{BasePath}/{tournamentTeamId}/approve-join/{targetUserId}");
    }

    public Task RemoveUserAsync(int tournamentTeamId, int targetUserId)
    {
        return PutAsync($"{BasePath}/{tournamentTeamId}/remove-user/{targetUserId}");
    }

    public Task GrantOwnershipAsync(int tournamentTeamId, int targetUserId)
    {
        return PutAsync($"{BasePath}/{tournamentTeamId}/grant-ownership/{targetUserId}");
    }

    public Task RevokeOwnershipAsync(int tournamentTeamId, int targetUserId)
    {
        return PutAsync($"{BasePath}/{tournamentTeamId}/revoke-ownership/{targetUserId}");
    }
}
