using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Events;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IEventsService
{
    Task<EventResponse?> CurrentEventAsync(bool forceReload = false);
    Task<EventResponse?> GetEvent(int eventId);
    Task<IPagedResult<EventResponse>> GetAsync(SieveModel model);
    Task<IList<EventResponse>> GetAllAsync(SieveModel model);
    Task<EventResponse> CreateAsync(CreateEventMessage createEventMessage);
    Task<EventResponse> UpdateAsync(int eventId, UpdateEventMessage updateEventMessage);
    Task DeleteAsync(int eventId);
    Task JoinEventAsync(int eventId, JoinEventMessage joinEventMessage);
    Task LeaveEventAsync(int eventId, LeaveEventMessage leaveEventMessage);
}
