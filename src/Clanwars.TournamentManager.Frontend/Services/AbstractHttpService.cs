using System.Net;
using System.Net.Http.Json;
using System.Text.Json;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Extensions;
using Microsoft.AspNetCore.WebUtilities;
using MudBlazor;
using Sieve.Models;
using static System.String;

namespace Clanwars.TournamentManager.Frontend.Services;

public abstract class AbstractHttpService(HttpClient httpClient, ISnackbar snackbar)
{
    protected async Task<TResponseBody?> GetAsync<TResponseBody>(string path) where TResponseBody : class
    {
        return await httpClient.GetFromJsonAsync<TResponseBody?>(path, JsonSerializerExtensions.JsonSerializerOptions());
    }

    protected async Task<IPagedResult<TResponseBody>> GetPagedAsync<TResponseBody>(string path, SieveModel sieveModel) where TResponseBody : class
    {
        var queryParams = new Dictionary<string, string?>
        {
            ["pageSize"] = !sieveModel.PageSize.HasValue ? "10" : sieveModel.PageSize!.ToString()!,
            ["page"] = !sieveModel.Page.HasValue ? "1" : sieveModel.Page!.ToString()!
        };

        if (sieveModel.Filters is { Length: > 0 })
        {
            queryParams["filters"] = sieveModel.Filters;
        }

        if (sieveModel.Sorts is { Length: > 0 })
        {
            queryParams["sorts"] = sieveModel.Sorts;
        }

        var url = QueryHelpers.AddQueryString(path, queryParams);

        var resp = await httpClient.GetAsync(url);

        var result = new PagedResult<TResponseBody>
        {
            Results = (await resp.Content.ReadFromJsonAsync<List<TResponseBody>>(JsonSerializerExtensions.JsonSerializerOptions()))!,
            CurrentPage = resp.Headers.GetValues("x-current-page").Select(int.Parse).FirstOrDefault(1),
            PageCount = resp.Headers.GetValues("x-page-count").Select(int.Parse).FirstOrDefault(0),
            PageSize = resp.Headers.GetValues("x-page-size").Select(int.Parse).FirstOrDefault(10),
            TotalCount = resp.Headers.GetValues("x-total-count").Select(int.Parse).FirstOrDefault(0)
        };

        return result;
    }

    protected async Task<IList<TResponseBody>> GetAllAsync<TResponseBody>(string path, string filters = "", string sorts = "") where TResponseBody : class
    {
        var output = new List<TResponseBody>();

        var page = 1;
        while (true)
        {
            var resp = await GetPagedAsync<TResponseBody>(path, new SieveModel { Page = page, PageSize = 20, Filters = filters, Sorts = sorts });
            output.AddRange(resp.Results);

            if (resp.PageCount > resp.CurrentPage)
            {
                page += 1;
                continue;
            }

            break;
        }

        return output;
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task<TResponseBody> PostAsync<TRequestBody, TResponseBody>(string path, TRequestBody? content)
        where TResponseBody : class, new()
        where TRequestBody : class
    {
        var resp = await httpClient.PostAsJsonAsync(path, content, JsonSerializerExtensions.JsonSerializerOptions());
        try
        {
            resp.EnsureSuccessStatusCode();
            return (await resp.Content.ReadFromJsonAsync<TResponseBody>(JsonSerializerExtensions.JsonSerializerOptions()))!;
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task<TResponseBody> PostAsync<TResponseBody>(string path)
    {
        var resp = await httpClient.PostAsync(path, null);
        try
        {
            resp.EnsureSuccessStatusCode();
            return (await resp.Content.ReadFromJsonAsync<TResponseBody>(JsonSerializerExtensions.JsonSerializerOptions()))!;
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task PostAsync<TRequestBody>(string path, TRequestBody content)
    {
        var resp = await httpClient.PostAsJsonAsync(path, content, JsonSerializerExtensions.JsonSerializerOptions());
        try
        {
            resp.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task PostAsync(string path)
    {
        var resp = await httpClient.PostAsync(path, null);

        try
        {
            resp.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task<TResponseBody> PutAsync<TRequestBody, TResponseBody>(string path, TRequestBody? content)
        where TResponseBody : class, new()
        where TRequestBody : class
    {
        var resp = await httpClient.PutAsJsonAsync(path, content, JsonSerializerExtensions.JsonSerializerOptions());
        try
        {
            resp.EnsureSuccessStatusCode();
            return (await resp.Content.ReadFromJsonAsync<TResponseBody>(JsonSerializerExtensions.JsonSerializerOptions()))!;
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task PutAsync<TRequestBody>(string path, TRequestBody? content)
        where TRequestBody : class
    {
        var resp = await httpClient.PutAsJsonAsync(path, content, JsonSerializerExtensions.JsonSerializerOptions());
        try
        {
            resp.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task<TResponseBody> PutAsync<TResponseBody>(string path)
        where TResponseBody : class, new()
    {
        var resp = await httpClient.PutAsync(path, null);
        try
        {
            resp.EnsureSuccessStatusCode();
            return (await resp.Content.ReadFromJsonAsync<TResponseBody>(JsonSerializerExtensions.JsonSerializerOptions()))!;
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task PutAsync(string path)
    {
        var resp = await httpClient.PutAsync(path, null);
        try
        {
            resp.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    protected async Task DeleteAsync(string path)
    {
        var resp = await httpClient.DeleteAsync(path);
        try
        {
            resp.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException exception)
        {
            await ShowBadRequestErrorInSnackbar(exception, resp);
            throw;
        }
    }

    private async Task ShowBadRequestErrorInSnackbar(HttpRequestException exception, HttpResponseMessage resp)
    {
        if (exception.StatusCode == HttpStatusCode.BadRequest && resp.Content.Headers.ContentType?.MediaType == "application/problem+json")
        {
            var badRequestJson = await resp.Content.ReadAsStringAsync();
            var problemDetails = JsonSerializer.Deserialize<ProblemDetailsWithErrors>(badRequestJson, JsonSerializerExtensions.JsonSerializerOptions())!;
            snackbar.Add("Could not save element, because of: <br>" + Join("<br>", problemDetails.Errors.Select(e => e.Value).SelectMany(e => e)), Severity.Error,
                options => { options.VisibleStateDuration = 5000; });
        }

        if (exception.StatusCode == HttpStatusCode.InternalServerError)
        {
            snackbar.Add("Could not save element, because of an INTERNAL SERVER ERROR", Severity.Error, options => { options.VisibleStateDuration = 5000; });
        }
    }
}

public class ProblemDetailsWithErrors
{
    public string Type { get; set; } = null!;
    public string Title { get; set; } = null!;
    public int Status { get; set; }
    public string TraceId { get; set; } = null!;
    public Dictionary<string, string[]> Errors { get; set; } = new();
}
