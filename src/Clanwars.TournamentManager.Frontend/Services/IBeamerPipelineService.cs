using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IBeamerPipelineService
{
    Task<IList<PipelineModuleStateResponse>> GetAllModuleStatesAsync();
    Task Run();
    Task Stop();
    Task Skip();
    Task SetModuleTimeout(ModuleType module, int timeoutInitial, int timeoutRecurring);
    Task EnableModule(ModuleType module);
    Task DisableModule(ModuleType module);

    public void RegisterAutoplayStateObserver(IAutoplayStateObserver observer);
    public void UnregisterAutoplayStateObserver(IAutoplayStateObserver observer);

    public void RegisterBeamerHubMessageObserver(IBeamerHubMessageObserver observer);
    public void UnregisterBeamerHubMessageObserver(IBeamerHubMessageObserver observer);
}
