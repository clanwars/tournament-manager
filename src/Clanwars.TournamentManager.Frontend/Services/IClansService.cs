using Clanwars.TournamentManager.Lib.Messages.Clans;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IClansService
{
    Task<ClanResponse> CreateClanAsync(CreateClanMessage createClanMessage);
    Task<ClanResponse> UpdateClanAsync(int clanId, UpdateClanMessage updateClanMessage);
    Task<IList<ClanResponse>> GetClansAsync(); // todo make them paginated and sieve filtered
    Task<ClanResponse> JoinClanAsync(int clanId);
    Task<ClanResponse> ApproveJoinAsync(int clanId, int targetUserId);
    Task LeaveClanAsync(int clanId);
    Task RemoveUserFromClanAsync(int clanId, int targetUserId);
    Task RevokeOwnershipAsync(int clanId, int targetUserId);
    Task GrantOwnershipAsync(int clanId, int targetUserId);
}
