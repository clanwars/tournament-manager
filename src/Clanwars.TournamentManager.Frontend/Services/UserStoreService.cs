using Clanwars.TournamentManager.Lib.Messages.Admin;
using Clanwars.TournamentManager.Lib.Messages.UserStore;
using MudBlazor;

namespace Clanwars.TournamentManager.Frontend.Services;

public class UserStoreService(HttpClient httpClient, ISnackbar snackbar) : AbstractHttpService(httpClient, snackbar), IUserStoreService
{
    private const string BasePath = "api/v1/userstore";

    public Task<IEnumerable<UserStoreResponse>> FindExternalUsers(string search)
    {
        return GetAsync<IEnumerable<UserStoreResponse>>(BasePath + $"?search={search}")!;
    }

    public Task<UserStoreResponse> Create(string email, string firstName, string lastName)
    {
        var cm = new CreateUserStoreUserMessage
        {
            Email = email,
            FirstName = firstName,
            LastName = lastName
        };
        return PostAsync<CreateUserStoreUserMessage, UserStoreResponse>(BasePath, cm);
    }
    
    
    public async Task ResetPassword(int userId)
    {
        await PutAsync($"{BasePath}/{userId}/reset-password");
    }
    
    public async Task<UserImportResponse> ReimportUsersToIdpAsync()
    {
        return await PostAsync<UserImportResponse>($"{BasePath}/reimport-users-to-idp");
    }
}
