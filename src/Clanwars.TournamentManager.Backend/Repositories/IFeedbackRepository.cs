using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface IFeedbackRepository
{
    IQueryable<Feedback> GetFeedbacks();

    Task<PagedResult<Feedback>> GetFeedbacksAsync(SieveModel sieveModel);

    Task AddFeedbackAsync(Feedback feedback);
    Task<Feedback> GetFeedbackByIdAsync(int id);
    Task UpdateFeedbackAsync(Feedback feedback);
}
