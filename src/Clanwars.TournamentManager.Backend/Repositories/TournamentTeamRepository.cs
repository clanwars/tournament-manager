using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class TournamentTeamRepository : ITournamentTeamRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public TournamentTeamRepository(
        TournamentManagerDbContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<TournamentTeam> GetTournamentTeams()
    {
        var result = _context
            .TournamentTeams
            .Include(tt => tt.Image)
            .Include(tt => tt.Members).ThenInclude(ttm => ttm.Member).ThenInclude(u => u.Clan)
            .Include(tt => tt.Members).ThenInclude(ttm => ttm.Member).ThenInclude(u => u.Image)
            .Include(tt => tt.Members).ThenInclude(ttm => ttm.Member).ThenInclude(u => u.Events).ThenInclude(eu => eu.Event).ThenInclude(e => e.Participants)
            .Include(tt => tt.Members).ThenInclude(ttm => ttm.Member).ThenInclude(u => u.Events).ThenInclude(eu => eu.Event).ThenInclude(e => e.Tournaments)
            .Include(tt => tt.Tournament)
            .Include(tt => tt.Tournament).ThenInclude(t => t.Rounds).ThenInclude(tr => tr.Matches).ThenInclude(tm => tm.Scores).ThenInclude(tms => tms.TournamentTeam)
            .Include(tt => tt.Tournament).ThenInclude(t => t.Teams);
        return result;
    }

    public async Task<PagedResult<TournamentTeam>> GetTournamentTeamsAsync(SieveModel sieveModel)
    {
        var result = GetTournamentTeams()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public Task<TournamentTeam?> GetTournamentTeamByIdAsync(int tournamentTeamId)
    {
        var result = GetTournamentTeams()
            .Where(tt => tt.Id == tournamentTeamId);
        return result.SingleOrDefaultAsync();
    }

    public async Task AddTournamentTeamAsync(TournamentTeam team)
    {
        _context.TournamentTeams.Add(team);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateTournamentTeamAsync(TournamentTeam team)
    {
        await _context.SaveChangesAsync();
    }

    public async Task DeleteTournamentTeamAsync(TournamentTeam team)
    {
        _context.TournamentTeams.Remove(team);
        await _context.SaveChangesAsync();
    }

    public async Task AddUserToTeamAsync(TournamentTeam tournamentTeam, int userId, EntityPermission permission)
    {
        tournamentTeam.Members.Add(new TournamentTeamMember
        {
            TournamentTeamId = tournamentTeam.Id,
            MemberId = userId,
            Permission = permission
        });

        await _context.SaveChangesAsync();
    }

    public async Task RemoveUserFromTeamAsync(TournamentTeam tournamentTeam, TournamentTeamMember member)
    {
        tournamentTeam.Members.Remove(member);
        await _context.SaveChangesAsync();
    }

    public async Task SealTeamAsync(TournamentTeam tournamentTeam)
    {
        tournamentTeam.Sealed = true;
        await _context.SaveChangesAsync();
    }

    public async Task AddPseudoTeamsToEliminationTournamentsAsync(Tournament tournament)
    {
        int targetNumberOfTeams;
        var n = tournament.Teams.Count;
        if (n < 2)
        {
            targetNumberOfTeams = 1;
        }
        else
        {
            targetNumberOfTeams = (int) Math.Pow(2, (int) Math.Log(n - 1, 2) + 1);
        }

        var teamsDiff = targetNumberOfTeams - n;
        for (var i = 0; i < teamsDiff; i++)
        {
            await AddTournamentTeamAsync(new TournamentTeam
            {
                TournamentId = tournament.Id,
                Name = $"{TournamentTeamResponseExtensions.ByeTeamPrefix}{Guid.NewGuid()}"
            });
        }
    }
}
