using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

internal class NewsRepository(
    TournamentManagerDbContext context,
    ISieveProcessor sieveProcessor,
    IOptions<SieveOptions> sieveOptions,
    IEventService eventService
)
    : INewsRepository
{
    private readonly SieveOptions _sieveOptions = sieveOptions.Value;


    private IQueryable<News> GetNews()
    {
        var result = context
            .News
            .Include(n => n.Author);
        return result;
    }

    public Task<News?> GetNewsByIdAsync(int newsId)
    {
        var result = GetNews()
            .Where(c => c.Id == newsId);
        return result.SingleOrDefaultAsync();
    }

    public async Task<PagedResult<News>> GetNewsAsync(SieveModel sieveModel)
    {
        var result = GetNews()
            .AsNoTrackingWithIdentityResolution();
        return await sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }


    public async Task AddNewsAsync(News news)
    {
        context.News.Add(news);
        await context.SaveChangesAsync();
    }

    public async Task DeleteNewsAsync(News news)
    {
        context.News.Remove(news);
        await context.SaveChangesAsync();
    }

    public async Task UpdateNewsAsync(News news)
    {
        await context.SaveChangesAsync();
    }

    public async Task<News?> GetRandomAsync()
    {
        var currentEvent = await eventService.GetRunningEventAsync();
        if (currentEvent is null)
        {
            return null;
        }

        var newsCount = await GetNews()
            .Where(n => n.CreatedAt.Date > currentEvent.StartDate.Date - TimeSpan.FromDays(7))
            .CountAsync();
        if (newsCount == 0)
        {
            return null;
        }

        var randomIndex = new Random().Next(newsCount);

        var randomEntry = await GetNews()
            .Where(n => n.CreatedAt.Date > currentEvent.StartDate.Date - TimeSpan.FromDays(7))
            .Skip(randomIndex)
            .FirstAsync();
        return randomEntry;
    }
}
