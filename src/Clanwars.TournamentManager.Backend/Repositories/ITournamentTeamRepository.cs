using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface ITournamentTeamRepository
{
    IQueryable<TournamentTeam> GetTournamentTeams();
    Task<PagedResult<TournamentTeam>> GetTournamentTeamsAsync(SieveModel sieveModel);
    Task<TournamentTeam?> GetTournamentTeamByIdAsync(int tournamentTeamId);
    Task AddTournamentTeamAsync(TournamentTeam team);
    Task UpdateTournamentTeamAsync(TournamentTeam team);
    Task DeleteTournamentTeamAsync(TournamentTeam team);

    Task AddUserToTeamAsync(TournamentTeam tournamentTeam, int userId, EntityPermission permission);
    Task RemoveUserFromTeamAsync(TournamentTeam tournamentTeam, TournamentTeamMember member);
    Task SealTeamAsync(TournamentTeam tournamentTeam);
    Task AddPseudoTeamsToEliminationTournamentsAsync(Tournament tournament);
}
