using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class TournamentRepository : ITournamentRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public TournamentRepository(
        TournamentManagerDbContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<Tournament> GetTournaments()
    {
        var result = _context
            .Tournaments
            .Include(t => t.Image)
            .Include(t => t.Rounds).ThenInclude(tr => tr.Matches).ThenInclude(tm => tm.Scores).ThenInclude(tms => tms.TournamentTeam).ThenInclude(tt => tt.Members)
            .Include(t => t.Teams).ThenInclude(tt => tt.Members).ThenInclude(ttm => ttm.Member).ThenInclude(m => m.Image);
        return result;
    }

    public Task<PagedResult<Tournament>> GetTournamentsAsync(SieveModel sieveModel)
    {
        var result = GetTournaments()
            .AsNoTrackingWithIdentityResolution();
        return _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<Tournament?> GetTournamentByIdAsync(int tournamentId)
    {
        var result = GetTournaments()
            .Where(t => t.Id == tournamentId);
        return await result.SingleOrDefaultAsync();
    }

    public async Task AddTournamentAsync(Tournament tournament)
    {
        await _context.Tournaments.AddAsync(tournament);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateTournamentAsync(Tournament tournament)
    {
        await _context.SaveChangesAsync();
    }

    public async Task DeleteTournamentAsync(Tournament tournament)
    {
        _context.Tournaments.Remove(tournament);
        await _context.SaveChangesAsync();
    }
}
