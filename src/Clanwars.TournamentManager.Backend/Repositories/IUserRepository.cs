using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface IUserRepository
{
    IQueryable<User> GetUsers();
    Task<PagedResult<User>> GetUsersAsync(SieveModel sieveModel);
    Task<User?> GetUserByIdAsync(int userId);
    Task<User?> GetUserByEmailAsync(string email);
    Task AddUserAsync(User user);
    Task UpdateUserAsync(User user);
    Task SetUsersClanAsync(User user, int clanId);
}
