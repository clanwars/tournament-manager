using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface INewsRepository
{
    Task<PagedResult<News>> GetNewsAsync(SieveModel sieveModel);
    Task<News?> GetNewsByIdAsync(int newsId);
    Task AddNewsAsync(News news);
    Task DeleteNewsAsync(News news);
    Task UpdateNewsAsync(News news);
    Task<News?> GetRandomAsync();
}
