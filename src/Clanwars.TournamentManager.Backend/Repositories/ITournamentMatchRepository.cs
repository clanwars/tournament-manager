using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface ITournamentMatchRepository
{
    IQueryable<TournamentMatch> GetTournamentMatches();
    IQueryable<TournamentMatchScore> GetTournamentMatchScores();
    Task<PagedResult<TournamentMatch>> GetTournamentMatchesAsync(SieveModel sieveModel);
    Task<TournamentMatch?> GetTournamentMatchByIdAsync(int tournamentMatchId);
    Task ScoreMatchAsync(TournamentMatch score);
}
