using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class UserRepository : IUserRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public UserRepository(
        TournamentManagerDbContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<User> GetUsers()
    {
        var result = _context
            .Users
            .Include(u => u.Image)
            .Include(u => u.Events).ThenInclude(eu => eu.Event)
            .Include(u => u.Clan).ThenInclude(c => c!.Members).ThenInclude(cu => cu.User).ThenInclude(u => u.Image);
        return result;
    }

    public async Task<PagedResult<User>> GetUsersAsync(SieveModel sieveModel)
    {
        var result = GetUsers()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<User?> GetUserByIdAsync(int userId)
    {
        var results = GetUsers()
            .Where(u => u.Id == userId);
        return await results.SingleOrDefaultAsync();
    }

    public async Task<User?> GetUserByEmailAsync(string email)
    {
        var results = GetUsers()
            .Where(u => u.Email == email);
        return await results.SingleOrDefaultAsync();
    }

    public async Task SetUsersClanAsync(User user, int clanId)
    {
        user.ClanId = clanId;
        await _context.SaveChangesAsync();
    }

    public async Task AddUserAsync(User user)
    {
        await _context.Users.AddAsync(user);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateUserAsync(User user)
    {
        await _context.SaveChangesAsync();
    }
}
