using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class ClanRepository : IClanRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public ClanRepository(
        TournamentManagerDbContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<Clan> GetClans()
    {
        var result = _context
                .Clans
                .Include(c => c.Image)
                .Include(c => c.Members).ThenInclude(cu => cu.User).ThenInclude(u => u.Image)
                .Include(c => c.Members).ThenInclude(cu => cu.User).ThenInclude(u => u.Events).ThenInclude(eu => eu.Event).ThenInclude(e => e.Participants)
                .Include(c => c.Members).ThenInclude(cu => cu.User).ThenInclude(u => u.Events).ThenInclude(eu => eu.Event).ThenInclude(e => e.Tournaments);
        return result;
    }
    
    public Task<Clan?> GetClanByIdAsync(int clanId)
    {
        var result = GetClans()
            .Where(c => c.Id == clanId);
        return result.SingleOrDefaultAsync();
    }

    public async Task<PagedResult<Clan>> GetClansAsync(SieveModel sieveModel)
    {
        var result = GetClans()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task AddClanAsync(Clan clan)
    {
        await _context.Clans.AddAsync(clan);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteClanAsync(Clan clan)
    {
        _context.Clans.Remove(clan);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateClanAsync(Clan clan)
    {
        await _context.SaveChangesAsync();
    }

    public async Task AddUserToClanAsync(Clan clan, User user, EntityPermission permission)
    {
        clan.Members.Add(new ClanUser
        {
            Clan = clan,
            User = user,
            Permission = permission
        });

        user.Clan = clan;

        await _context.SaveChangesAsync();
    }

    public async Task RemoveUserFromClanAsync(Clan clan, User user)
    {
        var uce = clan.Members.SingleOrDefault(uc => uc.UserId == user.Id);
        if (uce == null)
        {
            return;
        }

        clan.Members.Remove(uce);
        
        user.ClanId = null;

        await _context.SaveChangesAsync();
    }

    public async Task RemoveClanUserAsync(ClanUser clanUser)
    {
        clanUser.User.ClanId = null;
        clanUser.User.Clan = null;
        _context.UserClans.Remove(clanUser);
        await _context.SaveChangesAsync();
    }
}
