using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface IPipelineRepository
{
    Task<bool> IsModuleActivatedAsync(ModuleType type);
    Task<IList<PipelineModuleState>> ActivationStatesAsync();
    Task EnableModuleAsync(ModuleType type);
    Task DisableModuleAsync(ModuleType type);
    Task<PipelineModuleState> SetModuleTimeoutsAsync(ModuleType type, uint timeoutInitial, uint timeoutRecurring);
    Task<(uint Initial, uint Recurring)> GetTimeoutForModuleAsync(ModuleType module);
}
