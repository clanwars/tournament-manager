using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib.Messages.Pipeline;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class PipelineRepository : IPipelineRepository
{
    private readonly TournamentManagerDbContext _context;

    public PipelineRepository(TournamentManagerDbContext context)
    {
        _context = context;
    }

    public async Task<bool> IsModuleActivatedAsync(ModuleType type)
    {
        return await _context.ModuleStates.SingleOrDefaultAsync(s => s.Module == type && s.IsEnabled) != null;
    }

    public async Task<IList<PipelineModuleState>> ActivationStatesAsync()
    {
        return await _context.ModuleStates
            .OrderBy(m => m.Module)
            .ToListAsync();
    }

    public async Task EnableModuleAsync(ModuleType type)
    {
        var found = await _context.ModuleStates.FirstOrDefaultAsync(m => m.Module == type);
        if (found != null)
        {
            found.IsEnabled = true;
        }
        else
        {
            var e = new PipelineModuleState { Module = type, IsEnabled = true };
            await _context.ModuleStates.AddAsync(e);
        }

        await _context.SaveChangesAsync();
    }

    public async Task DisableModuleAsync(ModuleType type)
    {
        var found = await _context.ModuleStates.FirstOrDefaultAsync(m => m.Module == type);
        if (found != null)
        {
            found.IsEnabled = false;
        }
        else
        {
            var e = new PipelineModuleState { Module = type, IsEnabled = false };
            await _context.ModuleStates.AddAsync(e);
        }

        await _context.SaveChangesAsync();
    }

    public async Task<PipelineModuleState> SetModuleTimeoutsAsync(ModuleType type, uint timeoutInitial, uint timeoutRecurring)
    {
        var found = await _context.ModuleStates.FirstOrDefaultAsync(m => m.Module == type);
        if (found != null)
        {
            found.TimeoutInitial = timeoutInitial;
            found.TimeoutRecurring = timeoutRecurring;
            await _context.SaveChangesAsync();
            return found;
        }

        var newModule = new PipelineModuleState
        {
            Module = type,
            IsEnabled = false,
            TimeoutInitial = timeoutInitial,
            TimeoutRecurring = timeoutRecurring
        };
        await _context.ModuleStates.AddAsync(newModule);
        await _context.SaveChangesAsync();
        return newModule;
    }


    public async Task<(uint Initial, uint Recurring)> GetTimeoutForModuleAsync(ModuleType module)
    {
        var mod = await _context.ModuleStates.FirstOrDefaultAsync(m => m.Module == module);
        var timeoutInitial = mod.TimeoutInitial == 0 ? GetDefaultTimeoutForModule(module, true) : mod.TimeoutInitial;
        var timeoutRecurring = mod.TimeoutRecurring == 0 ? GetDefaultTimeoutForModule(module, false) : mod.TimeoutRecurring;

        return (timeoutInitial, timeoutRecurring);
    }

    private static uint GetDefaultTimeoutForModule(ModuleType module, bool isInitial = false)
    {
        return module switch
        {
            ModuleType.News => isInitial ? 15 * 60u : 2 * 60u,
            ModuleType.WelcomeMessage => 2 * 60,
            ModuleType.Tournaments => 1 * 60,
            ModuleType.Matches => 1 * 60,
            ModuleType.MatchResults => 1 * 60,
            ModuleType.MatchResult => 1 * 60,
            ModuleType.Shouts => isInitial ? 2 * 60u : 45,
            _ => 60
        };
    }
}
