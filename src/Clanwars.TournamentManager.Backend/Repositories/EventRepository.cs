using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class EventRepository : IEventRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public EventRepository(
        TournamentManagerDbContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<Event> GetEvents()
    {
        var result = _context
            .Events
            .Include(e => e.Participants)
            .Include(e => e.Tournaments);
        return result;
    }

    private IQueryable<EventUser> GetRegistrations()
    {
        var result = _context
            .EventUsers
            .Include(eu => eu.Event)
            .Include(eu => eu.User);
        return result;
    }

    public Task<PagedResult<Event>> GetEventsAsync(SieveModel sieveModel)
    {
        var result = GetEvents()
            .AsNoTrackingWithIdentityResolution();
        return _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public Task<Event?> GetEventByIdAsync(int eventId)
    {
        var result = GetEvents()
            .Where(e => e.Id == eventId);
        return result.SingleOrDefaultAsync();
    }

    public Task<PagedResult<EventUser>> GetRegistrationsAsync(SieveModel sieveModel)
    {
        var result = GetRegistrations()
            .AsNoTrackingWithIdentityResolution();
        return _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task AddEventAsync(Event @event)
    {
        await _context.Events.AddAsync(@event);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteEventAsync(Event @event)
    {
        _context.Events.Remove(@event);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateEventAsync(Event @event)
    {
        await _context.SaveChangesAsync();
    }

    public async Task<EventUser> AddUserToEventAsync(Event @event, int userId)
    {
        var newEventUser = new EventUser
        {
            EventId = @event.Id,
            UserId = userId,
            CheckinTime = DateTime.Now
        };
        @event.Participants.Add(newEventUser);

        await _context.EventUsers.AddAsync(newEventUser);
        await _context.SaveChangesAsync();

        return await GetRegistrations().SingleAsync(eu => eu.Id == newEventUser.Id);
    }

    public async Task RemoveUserFromEventAsync(Event @event, int userId)
    {
        var uce = @event.Participants.SingleOrDefault(uc => uc.UserId == userId);
        if (uce is null)
        {
            return;
        }

        @event.Participants.Remove(uce);
        _context.EventUsers.Remove(uce);

        await _context.SaveChangesAsync();
    }
}
