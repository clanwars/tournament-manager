using Clanwars.TournamentManager.Lib.Helper;
using Clanwars.TournamentManager.Lib.Signalr;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Clanwars.TournamentManager.Backend.Hubs;

[AllowAnonymous]
public class NotificationsHub : Hub<INotificationsSender>
{
    public override async Task OnConnectedAsync()
    {
        await Clients.Caller.SendTmVersion(AssemblyExtensions.GetTmVersion());

        await base.OnConnectedAsync();
    }
}
