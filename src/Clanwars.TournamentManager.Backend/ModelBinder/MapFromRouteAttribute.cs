namespace Clanwars.TournamentManager.Backend.ModelBinder;

[AttributeUsage(AttributeTargets.Property)]
public class MapFromRouteAttribute : Attribute;
