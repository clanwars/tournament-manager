using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace Clanwars.TournamentManager.Backend.ModelBinder;

public class MapToModelModelBinderProvider : IModelBinderProvider
{
    private readonly BodyModelBinderProvider _bodyProvider;

    public MapToModelModelBinderProvider(BodyModelBinderProvider bodyProvider)
    {
        _bodyProvider = bodyProvider;
    }

    public IModelBinder? GetBinder(ModelBinderProviderContext context)
    {
        ArgumentNullException.ThrowIfNull(context);

        var modelTypeProperties = context.Metadata.ModelType.GetProperties();
        var anyPropertyHasMapFromRoute = modelTypeProperties.ToList().Exists(p => p.GetCustomAttribute<MapFromRouteAttribute>() != null);

        return anyPropertyHasMapFromRoute ? new MapToModelModelBinder(_bodyProvider.GetBinder(context)!) : null;
    }
}
