using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface IImageService
{
    Task<PagedResult<Image>> GetImagesAsync(SieveModel sieveModel);
    Task<ImageWithContent?> GetImageWithContentByIdAsync(int imageId);
    Task<Image?> GetImageByIdAsync(int imageId);
    Task UpdateImageAsync(Image image);
    Task<ImageWithContent?> GetImageWithContentByHashAndImageTypeAsync(byte[] hash, ImageType imageType);
    Task<ImageWithContent> AddImageWithContentAsync(ImageType type, string content);
    Task RemoveImageAsync(int imageId);
    Task<Image?> GetRandomImageAsync(ImageType imageType);
}
