using System.Security.Claims;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.ClaimsPrincipal;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;
    private readonly IHubContext<NotificationsHub, INotificationsSender> _notificationsHub;
    private readonly IBeamerContentProvider _beamerContentProvider;
    private readonly ILogger<UserService> _logger;

    public UserService(
        IUserRepository userRepository,
        IHubContext<NotificationsHub, INotificationsSender> notificationsHub,
        IBeamerContentProvider beamerContentProvider,
        ILogger<UserService> logger
    )
    {
        _userRepository = userRepository;
        _notificationsHub = notificationsHub;
        _beamerContentProvider = beamerContentProvider;
        _logger = logger;
    }

    public Task<PagedResult<User>> GetUsersAsync(SieveModel sieveModel)
    {
        return _userRepository.GetUsersAsync(sieveModel);
    }

    public async Task<IList<User>> GetAllUsersAsync()
    {
        return await _userRepository
            .GetUsers()
            .AsNoTrackingWithIdentityResolution()
            .ToListAsync();
    }

    public Task<User?> GetByIdAsync(int userId)
    {
        return _userRepository.GetUserByIdAsync(userId);
    }

    public async Task<User> GetByClaimsPrincipalAsync(ClaimsPrincipal principal)
    {
        var user = await _userRepository.GetUserByEmailAsync(principal.GetEmail());
        if (user is not null)
        {
            return user;
        }

        return await CreateUserSkeletonAsync(
            principal.GetEmail(),
            principal.FindFirstValue(ClaimTypes.GivenName),
            principal.FindFirstValue(ClaimTypes.Surname)
        );
    }

    public async Task UpdateUserAsync(User user)
    {
        await _userRepository.UpdateUserAsync(user);
        await _notificationsHub.Clients.All.SendUserChanged(new UserHubMessage // TODO send message to only affected user (when hub is authenticated)
        {
            Id = user.Id,
            Event = UserChangeEvent.Changed
        });
    }

    public async Task<User> CreateUserSkeletonAsync(string email, string firstName, string lastName)
    {
        _logger.LogError("{Name}: creating new user", nameof(CreateUserSkeletonAsync));
        var userEntity = new User
        {
            Email = email,
            FirstName = firstName,
            LastName = lastName,
            CreatedAt = DateTime.Now
        };
        await _userRepository.AddUserAsync(userEntity);
        await _notificationsHub.Clients.All.SendUserChanged(new UserHubMessage
        {
            Id = userEntity.Id,
            Event = UserChangeEvent.Created
        });

        return userEntity;
    }

    public async Task ResetPassword(int userId)
    {
        var user = await _userRepository.GetUserByIdAsync(userId);

    }

    public async Task<bool> IsUniqueNickname(int userId, string nickname)
    {
        var result = await _userRepository
            .GetUsers()
            .Where(user => user.Id != userId)
            .FirstOrDefaultAsync(user => user.Nickname == nickname);

        return result is null;
    }
}
