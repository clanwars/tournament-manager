using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class NewsService : INewsService
{
    private readonly ILogger _logger;
    private readonly INewsRepository _newsRepository;
    private readonly IHubContext<NotificationsHub, INotificationsSender> _notificationsHub;
    private readonly IBeamerContentProvider _pipelinePublisher;

    public NewsService(
        ILogger<NewsService> logger,
        INewsRepository newsRepository,
        IHubContext<NotificationsHub, INotificationsSender> notificationsHub,
        IBeamerContentProvider pipelinePublisher
    )
    {
        _logger = logger;
        _newsRepository = newsRepository;
        _notificationsHub = notificationsHub;
        _pipelinePublisher = pipelinePublisher;
    }

    public Task<PagedResult<News>> GetNewsAsync(SieveModel sieveModel)
    {
        return _newsRepository.GetNewsAsync(sieveModel);
    }

    public Task<News?> GetNewsByIdAsync(int newsId)
    {
        return _newsRepository.GetNewsByIdAsync(newsId);
    }

    public async Task AddNewsAsync(News news)
    {
        _logger.LogDebug("Start adding news");
        await _newsRepository.AddNewsAsync(news);
        await _notificationsHub.Clients.All.SendNewsChanged(new NewsHubMessage
        {
            Id = news.Id,
            Event = NewsChangeEvent.Created
        });

        _logger.LogDebug("Publishing news");
        _pipelinePublisher.QueueNewsMessage(news.Id);
    }

    public async Task DeleteNewsAsync(News news)
    {
        await _newsRepository.DeleteNewsAsync(news);
        await _notificationsHub.Clients.All.SendNewsChanged(new NewsHubMessage
        {
            Id = news.Id,
            Event = NewsChangeEvent.Deleted
        });
    }


    public async Task UpdateNewsAsync(News news)
    {
        await _newsRepository.UpdateNewsAsync(news);
        await _notificationsHub.Clients.All.SendNewsChanged(new NewsHubMessage
        {
            Id = news.Id,
            Event = NewsChangeEvent.Changed
        });
    }

    public Task<News?> GetRandomAsync()
    {
        return _newsRepository.GetRandomAsync();
    }
}
