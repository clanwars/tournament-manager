using System.Collections.Concurrent;
using System.Security.Cryptography;
using Clanwars.TournamentManager.Backend.Helpers.Exceptions;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;
using Clanwars.TournamentManager.Lib.Messages.Pipeline;
using Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;
using HashidsNet;

namespace Clanwars.TournamentManager.Backend.Services.Pipeline;

public class BeamerContentProvider(
    ILogger<BeamerContentProvider> logger,
    IServiceProvider services,
    IHashids hashId
) : IBeamerContentProvider
{
    private readonly ConcurrentQueue<IBeamerHubMessage> _messageQueue = new();
    private IServiceProvider Services { get; } = services;

    private async Task<int> CalculateModuleTimeoutAsync(ModuleType module, bool isNew)
    {
        using var scope = Services.CreateScope();
        var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
        var (initial, recurring) = await pipelineRepository.GetTimeoutForModuleAsync(module);
        return (int)(isNew ? initial : recurring) * 1000;
    }

    public void QueueNewsMessage(int newsId)
    {
        logger.LogDebug(nameof(QueueNewsMessage));
        QueueMessage(new NewsBeamerHubMessage(newsId));
    }

    public void QueueShoutMessage(int shoutId)
    {
        logger.LogDebug(nameof(QueueShoutMessage));
        QueueMessage(new ShoutBeamerHubMessage(shoutId));
    }

    public void QueueWelcomeMessage(int userId)
    {
        logger.LogDebug(nameof(QueueWelcomeMessage));
        QueueMessage(new WelcomeBeamerHubMessage(userId));
    }

    public void QueueImageMessage(string beamerImageHashId, int timeout)
    {
        logger.LogDebug(nameof(QueueImageMessage));
        QueueMessage(new ImageBeamerHubMessage(beamerImageHashId, DisplayTimeout: timeout));
    }

    public void QueueTournamentsMessage(IList<int> tournamentIds)
    {
        logger.LogDebug(nameof(QueueTournamentsMessage));
        QueueMessage(new TournamentBeamerHubMessage(tournamentIds));
    }

    public void QueueTournamentMatch(int tournamentId, int tournamentMatchId)
    {
        logger.LogDebug(nameof(QueueTournamentMatch));
        QueueMessage(new TournamentMatchResultBeamerHubMessage(tournamentId, tournamentMatchId));
    }

    private void QueueMessage(IBeamerHubMessage message)
    {
        _messageQueue.Enqueue(message);
    }

    public async Task<(IBeamerHubMessage beamerHubMessage, int suggestedDisplayTime)> Dequeue()
    {
        _messageQueue.TryDequeue(out var next);
        // IDEA filter dequeue for disabled modules?!? wtf
        // IDEA use different queue type (e.g. List)

        next ??= await FindRandomElementAsync();

        int suggestedDisplayTime;

        if (next is ImageBeamerHubMessage { DisplayTimeout: not null } imageBeamerHubMessage)
        {
            suggestedDisplayTime = imageBeamerHubMessage.DisplayTimeout.Value;
        }
        else
        {
            suggestedDisplayTime = await CalculateModuleTimeoutAsync(next.ModuleType, next.IsNew);
        }

        return (next, suggestedDisplayTime);
    }

    private async Task<IBeamerHubMessage> FindRandomElementAsync()
    {
        logger.LogDebug(nameof(FindRandomElementAsync));

        using var scope = Services.CreateScope();
        var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();

        IBeamerHubMessage? next = null;

        var moduleWeights = new Dictionary<ModuleType, double>
        {
            { ModuleType.News, 1.0 },
            { ModuleType.Shouts, 1.0 },
            { ModuleType.Tournaments, 1.0 },
            { ModuleType.Matches, 1.0 },
            { ModuleType.MatchResults, 1.0 },
            { ModuleType.Images, 1.0 }
        };

        const int maxAttempts = 10;
        var attempts = 0;
        
        while (attempts < maxAttempts && moduleWeights.Count > 0)
        {
            attempts++;
            
            var decidedModule = GetRandomModuleBasedOnWeights(moduleWeights);

            logger.LogInformation("Weighted random content decision: {ModuleType}", decidedModule);

            if (!await pipelineRepository.IsModuleActivatedAsync(decidedModule))
            {
                logger.LogInformation("Module type {ModuleType} is disabled and thus skipped!", decidedModule);
                moduleWeights.Remove(decidedModule); // Remove disabled module
                continue;
            }

            next = decidedModule switch
            {
                ModuleType.News => await GetRandomNewsFromDbAsync(scope),
                ModuleType.Shouts => await GetRandomShoutFromDbAsync(scope),
                ModuleType.Tournaments => await GetTournamentsFromDbAsync(scope),
                ModuleType.Matches => await GetTournamentMatchesFromDbAsync(scope, isFinished: false),
                ModuleType.MatchResults => await GetTournamentMatchesFromDbAsync(scope, isFinished: true),
                ModuleType.Images => await GetRandomImageFromDbAsync(scope),
                _ => next
            };
            
            if (next == null)
            {
                logger.LogInformation("No data found for module type {ModuleType}", decidedModule);
                moduleWeights.Remove(decidedModule); // Remove module with no data
            }
            else
            {
                break; // Exit loop if valid data is found
            }
        }
        
        if (next is null)
        {
            throw new NoBeamerContentAvailableException("Failed to find any content after exhausting options.");
        }

        return next;
    }

    private async Task<ShoutBeamerHubMessage?> GetRandomShoutFromDbAsync(IServiceScope scope)
    {
        logger.LogDebug(nameof(GetRandomShoutFromDbAsync));

        var shoutService = scope.ServiceProvider.GetRequiredService<IShoutService>();
        var randomShout = await shoutService.GetRandomAsync();

        if (randomShout is null)
        {
            return null;
        }

        return new ShoutBeamerHubMessage(randomShout.Id, false);
    }

    private async Task<NewsBeamerHubMessage?> GetRandomNewsFromDbAsync(IServiceScope scope)
    {
        logger.LogDebug(nameof(GetRandomNewsFromDbAsync));

        var newsService = scope.ServiceProvider.GetRequiredService<INewsService>();
        var news = await newsService.GetRandomAsync();

        if (news is null)
        {
            return null;
        }

        return new NewsBeamerHubMessage(news.Id, false);
    }

    private async Task<TournamentBeamerHubMessage> GetTournamentsFromDbAsync(IServiceScope scope)
    {
        logger.LogDebug(nameof(GetTournamentsFromDbAsync));

        var tournamentService = scope.ServiceProvider.GetRequiredService<ITournamentService>();
        var tournamentIds = (await tournamentService.GetTournamentsFromRunningEvent()).Select(t => t.Id).ToList();

        return new TournamentBeamerHubMessage(tournamentIds, false);
    }

    private async Task<IBeamerHubMessage?> GetTournamentMatchesFromDbAsync(IServiceScope scope, bool isFinished = false)
    {
        logger.LogDebug(nameof(GetTournamentMatchesFromDbAsync));

        var tournamentService = scope.ServiceProvider.GetRequiredService<ITournamentService>();
        var tournaments = (await tournamentService.GetTournamentsFromRunningEvent()).Where(t => t.State == TournamentState.Running).ToList();

        if (tournaments.Count == 0)
        {
            return null;
        }

        var random = new Random();
        var rand = random.Next(0, tournaments.Count);

        if (isFinished)
        {
            return new FinishedMatchesBeamerHubMessage(tournaments[rand].Id, false);
        }

        return new UpcomingMatchesBeamerHubMessage(tournaments[rand].Id, false);
    }

    private async Task<ImageBeamerHubMessage?> GetRandomImageFromDbAsync(IServiceScope scope)
    {
        logger.LogDebug(nameof(GetRandomImageFromDbAsync));

        var imageService = scope.ServiceProvider.GetRequiredService<IImageService>();
        var beamerImage = await imageService.GetRandomImageAsync(ImageType.Beamer);

        if (beamerImage is null)
        {
            return null;
        }

        return new ImageBeamerHubMessage(hashId.Encode(beamerImage.Id), false);
    }

    private ModuleType GetRandomModuleBasedOnWeights(Dictionary<ModuleType, double> modulesWithWeights)
    {
        var totalWeight = modulesWithWeights.Values.Sum();
        var random = new Random();
        var randomNumber = random.NextDouble() * totalWeight;

        foreach (var module in modulesWithWeights)
        {
            if (randomNumber < module.Value)
            {
                return module.Key;
            }

            randomNumber -= module.Value;
        }

        throw new InvalidOperationException("Should never get here if weights are set up correctly.");
    }
}
