using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface ITournamentTeamService
{
    Task<PagedResult<TournamentTeam>> GetTournamentTeamsAsync(SieveModel model);
    Task<TournamentTeam?> GetTournamentTeamByIdAsync(int tournamentTeamId);
    Task AddTournamentTeamAsync(TournamentTeam tournamentTeam);
    Task UpdateTournamentTeamAsync(TournamentTeam tournamentTeam);
    Task DeleteTournamentTeamAsync(TournamentTeam tournamentTeam);

    Task AddUserToTeamAsync(TournamentTeam tournamentTeam, int userId, EntityPermission permission);
    Task RemoveUserFromTeamAsync(TournamentTeam tournamentTeam, TournamentTeamMember member);
    Task SealTeamAsync(TournamentTeam tournamentTeam);
    Task AddPseudoTeamsToEliminationTournamentsAsync(Tournament tournament);
}
