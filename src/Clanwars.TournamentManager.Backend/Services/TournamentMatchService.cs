using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class TournamentMatchService : ITournamentMatchService
{
    private readonly ITournamentMatchRepository _tournamentMatchRepository;
    private readonly IHubContext<NotificationsHub, INotificationsSender> _notificationsHub;
    private readonly IBeamerContentProvider _beamerContentProvider;

    public TournamentMatchService(
        ITournamentMatchRepository tournamentMatchRepository,
        IHubContext<NotificationsHub, INotificationsSender> notificationsHub,
        IBeamerContentProvider beamerContentProvider
    )
    {
        _tournamentMatchRepository = tournamentMatchRepository;
        _notificationsHub = notificationsHub;
        _beamerContentProvider = beamerContentProvider;
    }

    public Task<PagedResult<TournamentMatch>> GetTournamentMatchesAsync(SieveModel sieveModel)
    {
        return _tournamentMatchRepository.GetTournamentMatchesAsync(sieveModel);
    }

    public Task<TournamentMatch?> GetTournamentMatchByIdAsync(int tournamentMatchId)
    {
        return _tournamentMatchRepository.GetTournamentMatchByIdAsync(tournamentMatchId);
    }

    public async Task ScoreMatchAsync(TournamentMatch match)
    {
        await _tournamentMatchRepository.ScoreMatchAsync(match);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = match.TournamentRound.TournamentId,
            Event = TournamentChangeEvent.MatchFinished
        });

        if (!match.IsByeMatch())
        {
            _beamerContentProvider.QueueTournamentMatch(match.TournamentRound.TournamentId, match.Id);
        }
    }
}
