using System.Text.Json;
using System.Web;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;
using Clanwars.TournamentManager.Backend.Services.PaymentSystem.OidcTokenAuthenticationClient;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Extensions;
using Microsoft.AspNetCore.WebUtilities;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services.PaymentSystem.Client;

internal class PaymentSystemClient : IPaymentSystemClient
{
    private readonly ILogger<PaymentSystemClient> _logger;
    private readonly HttpClient _httpClient;
    private readonly IOidcTokenAuthenticationClient _oidcTokenAuthenticationClient;

    private JsonSerializerOptions SerializerOptions => JsonSerializerExtensions.JsonSerializerOptions();

    public PaymentSystemClient(
        ILogger<PaymentSystemClient> logger,
        HttpClient httpClient,
        IOidcTokenAuthenticationClient oidcTokenAuthenticationClient
    )
    {
        _logger = logger;
        _httpClient = httpClient;
        _oidcTokenAuthenticationClient = oidcTokenAuthenticationClient;
    }

    public async Task<AccountInfo?> GetAccountAsync(Guid accountId)
    {
        var url = "api/v1/Accounts?Filters=" + HttpUtility.UrlEncode("id==" + accountId);
        var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);

        await _oidcTokenAuthenticationClient.AddAuthorizationHeaderToHttpRequestAsync(requestMessage);

        _logger.LogInformation("Calling {HttpMethod} {Url}", requestMessage.Method, url);
        var results = await SendRequestAsync<IList<AccountInfo>>(requestMessage);
        _logger.LogInformation("Calling {HttpMethod} {Url} completed, response message: {ResponseMessage}", requestMessage.Method, url, results);

        return results.SingleOrDefault();
    }

    public async Task<AccountInfo?> GetAccountByEmailAsync(string email)
    {
        var url = "api/v1/Accounts?Filters=" + HttpUtility.UrlEncode("email==" + email);
        var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);

        await _oidcTokenAuthenticationClient.AddAuthorizationHeaderToHttpRequestAsync(requestMessage);

        _logger.LogInformation("Calling {HttpMethod} {Url}", requestMessage.Method, url);
        var results = await SendRequestAsync<IList<AccountInfo>>(requestMessage);
        _logger.LogInformation("Calling {HttpMethod} {Url} completed, response message: {ResponseMessage}", requestMessage.Method, url, results);

        return results.SingleOrDefault();
    }

    public async Task<AccountInfo> CreateAccountAsync(CreateAccountMessage createAccountMessage)
    {
        const string url = "api/v1/Accounts";
        var requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
        await _oidcTokenAuthenticationClient.AddAuthorizationHeaderToHttpRequestAsync(requestMessage);
        requestMessage.Content = JsonContent.Create(createAccountMessage);

        _logger.LogInformation("Calling {HttpMethod} {Url}", requestMessage.Method, url);
        var result = await SendRequestAsync<AccountInfo>(requestMessage);
        _logger.LogInformation("Calling {HttpMethod} {Url} completed, response message: {ResponseMessage}", requestMessage.Method, url, result);

        return result;
    }

    public async Task<AccountInfo> UpdateAccountAsync(Guid accountId, UpdateAccountMessage updateAccountMessage)
    {
        var url = $"api/v1/Accounts/{accountId}";
        var requestMessage = new HttpRequestMessage(HttpMethod.Put, url);
        await _oidcTokenAuthenticationClient.AddAuthorizationHeaderToHttpRequestAsync(requestMessage);
        requestMessage.Content = JsonContent.Create(updateAccountMessage);

        _logger.LogInformation("Calling {HttpMethod} {Url}", requestMessage.Method, url);
        var result = await SendRequestAsync<AccountInfo>(requestMessage);
        _logger.LogInformation("Calling {HttpMethod} {Url} completed, response message: {ResponseMessage}", requestMessage.Method, url, result);

        return result;
    }

    public async Task<IList<PointOfSaleInfoWithCurrentCard>> GetCurrentCardsAsync()
    {
        const string url = "api/v1/PointsOfSale/current-cards";
        var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);

        await _oidcTokenAuthenticationClient.AddAuthorizationHeaderToHttpRequestAsync(requestMessage);

        _logger.LogInformation("Calling {HttpMethod} {Url}", requestMessage.Method, url);
        var results = await SendRequestAsync<IList<PointOfSaleInfoWithCurrentCard>>(requestMessage);
        _logger.LogInformation("Calling {HttpMethod} {Url} completed, response message: {ResponseMessage}", requestMessage.Method, url, results);

        return results;
    }

    public async Task<IPagedResult<TransactionResponse>> GetTransactionsAsync(SieveModel sieveModel)
    {
        const string url = "api/v1/Transactions";
        var results = await GetPagedAsync<TransactionResponse>(url, sieveModel);

        return results;
    }

    protected async Task<T> SendRequestAsync<T>(HttpRequestMessage requestMessage)
    {
        using var response = await _httpClient.SendAsync(requestMessage).ConfigureAwait(false);
        response.EnsureSuccessStatusCode();

        await using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
        return (await JsonSerializer.DeserializeAsync<T>(responseStream, SerializerOptions).ConfigureAwait(false))!;
    }

    protected async Task<IPagedResult<TResponseBody>> GetPagedAsync<TResponseBody>(string path, SieveModel sieveModel) where TResponseBody : class
    {
        var queryParams = new Dictionary<string, string?>
        {
            ["pageSize"] = !sieveModel.PageSize.HasValue ? "10" : sieveModel.PageSize!.ToString()!,
            ["page"] = !sieveModel.Page.HasValue ? "1" : sieveModel.Page!.ToString()!
        };

        if (sieveModel.Filters is {Length: > 0})
        {
            queryParams["filters"] = sieveModel.Filters;
        }

        if (sieveModel.Sorts is {Length: > 0})
        {
            queryParams["sorts"] = sieveModel.Sorts;
        }

        var url = QueryHelpers.AddQueryString(path, queryParams);

        var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);

        await _oidcTokenAuthenticationClient.AddAuthorizationHeaderToHttpRequestAsync(requestMessage);

        _logger.LogInformation("Calling {HttpMethod} {Url}", requestMessage.Method, url);
        using var resp = await _httpClient.SendAsync(requestMessage).ConfigureAwait(false);
        resp.EnsureSuccessStatusCode();
        _logger.LogInformation("Calling {HttpMethod} {Url} completed, response message: {ResponseMessage}", requestMessage.Method, url, resp);

        var result = new PagedResult<TResponseBody>
        {
            Results = (await resp.Content.ReadFromJsonAsync<List<TResponseBody>>(SerializerOptions))!,
            CurrentPage = resp.Headers.GetValues("x-current-page").Select(int.Parse).FirstOrDefault(1),
            PageCount = resp.Headers.GetValues("x-page-count").Select(int.Parse).FirstOrDefault(0),
            PageSize = resp.Headers.GetValues("x-page-size").Select(int.Parse).FirstOrDefault(10),
            TotalCount = resp.Headers.GetValues("x-total-count").Select(int.Parse).FirstOrDefault(0),
        };

        return result;
    }
}
