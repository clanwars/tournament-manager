namespace Clanwars.TournamentManager.Backend.Services.PaymentSystem.OidcTokenAuthenticationClient;

public class OidcOptions
{
    public Uri IdpUrl { get; set; } = default!;
    public string ClientId { get; set; } = default!;
    public string ClientSecret { get; set; } = default!;
}
