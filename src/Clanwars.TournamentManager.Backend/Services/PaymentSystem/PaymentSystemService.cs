using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;
using Clanwars.TournamentManager.Backend.Services.PaymentSystem.Client;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services.PaymentSystem;

internal class PaymentSystemService : IPaymentSystemService
{
    private readonly IPaymentSystemClient _paymentSystemClient;

    public PaymentSystemService(
        IPaymentSystemClient paymentSystemClient
    )
    {
        _paymentSystemClient = paymentSystemClient;
    }

    public Task<AccountInfo?> GetAccountAsync(Guid accountId)
    {
        return _paymentSystemClient.GetAccountAsync(accountId);
    }

    public Task<AccountInfo?> GetAccountByEmailAsync(string email)
    {
        return _paymentSystemClient.GetAccountByEmailAsync(email);
    }

    public Task<AccountInfo> CreateAccountAsync(CreateAccountMessage createAccountMessage)
    {
        return _paymentSystemClient.CreateAccountAsync(createAccountMessage);
    }

    public Task<AccountInfo> UpdateAccountAsync(Guid accountId, UpdateAccountMessage updateAccountMessage)
    {
        return _paymentSystemClient.UpdateAccountAsync(accountId, updateAccountMessage);
    }

    public Task<IList<PointOfSaleInfoWithCurrentCard>> GetCurrentCardsAsync()
    {
        return _paymentSystemClient.GetCurrentCardsAsync();
    }

    public Task<IPagedResult<TransactionResponse>> GetTransactionsAsync(SieveModel sieveModel)
    {
        return _paymentSystemClient.GetTransactionsAsync(sieveModel);
    }
}
