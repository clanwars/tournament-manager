using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Entities.Tournaments.Extensions;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Sieve.Models;
using TournamentMode = Clanwars.TournamentManager.Lib.Enums.Tournaments.TournamentMode;
using TournamentState = Clanwars.TournamentManager.Lib.Enums.Tournaments.TournamentState;

namespace Clanwars.TournamentManager.Backend.Services;

public class TournamentService : ITournamentService
{
    private readonly ITournamentRepository _tournamentRepository;
    private readonly ITournamentTeamService _tournamentTeamService;
    private readonly ITournamentMatchService _tournamentMatchService;
    private readonly IEventService _eventService;
    private readonly IHubContext<NotificationsHub, INotificationsSender> _notificationsHub;

    public TournamentService(
        ITournamentRepository tournamentRepository,
        ITournamentTeamService tournamentTeamService,
        ITournamentMatchService tournamentMatchService,
        IHubContext<NotificationsHub, INotificationsSender> notificationsHub,
        IEventService eventService
    )
    {
        _tournamentRepository = tournamentRepository;
        _tournamentTeamService = tournamentTeamService;
        _tournamentMatchService = tournamentMatchService;
        _notificationsHub = notificationsHub;
        _eventService = eventService;
    }

    public Task<PagedResult<Tournament>> GetTournamentsAsync(SieveModel sieveModel)
    {
        return _tournamentRepository.GetTournamentsAsync(sieveModel);
    }

    public Task<Tournament?> GetTournamentByIdAsync(int tournamentId)
    {
        return _tournamentRepository.GetTournamentByIdAsync(tournamentId);
    }

    public async Task AddTournamentAsync(Tournament tournament)
    {
        await _tournamentRepository.AddTournamentAsync(tournament);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournament.Id,
            Event = TournamentChangeEvent.Created
        });

        // if (_lanpartyScreenOptions.Enabled)
        // {
        //     await SendCreateTournamentEntryToLps(tournament);
        // }
    }

    public async Task StartTournamentAsync(Tournament tournament)
    {
        switch (tournament)
        {
            case { Mode: TournamentMode.SingleElimination }:
                await _tournamentTeamService.AddPseudoTeamsToEliminationTournamentsAsync(tournament);
                tournament.GenerateSingleEliminationMatches();
                break;
            case { Mode: TournamentMode.DoubleElimination }:
                await _tournamentTeamService.AddPseudoTeamsToEliminationTournamentsAsync(tournament);
                tournament.GenerateDoubleEliminationMatches();
                break;
            case { Mode: TournamentMode.Points }:
                tournament.GeneratePointsMatches();
                break;
        }

        tournament.UpdatedAt = DateTime.Now;
        tournament.State = TournamentState.Running;

        await _tournamentRepository.UpdateTournamentAsync(tournament);

        foreach (var team in tournament.Teams)
        {
            await _tournamentTeamService.SealTeamAsync(team);
        }

        // if (_lanpartyScreenOptions.Enabled)
        // {
        //     await SendUpdateTournamentEntryForStartToLps(tournament);
        // }

        // Score matches with pseudo teams
        await ScorePseudoMatches(tournament.Rounds[0].Matches);

        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournament.Id,
            Event = TournamentChangeEvent.Started
        });
    }

    public async Task UpdateTournamentAsync(Tournament tournament)
    {
        await _tournamentRepository.UpdateTournamentAsync(tournament);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournament.Id,
            Event = TournamentChangeEvent.Changed
        });

        // if (_lanpartyScreenOptions.Enabled)
        // {
        //     await SendUpdateTournamentEntryToLps(tournament);
        // }
    }

    public async Task DeleteTournamentAsync(Tournament tournament)
    {
        await _tournamentRepository.DeleteTournamentAsync(tournament);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournament.Id,
            Event = TournamentChangeEvent.Deleted
        });

        // if (_lanpartyScreenOptions.Enabled)
        // {
        //     await SendDeleteTournamentEntryToLps(tournament);
        // }
    }

    public async Task<IList<Tournament>> GetTournamentsFromRunningEvent()
    {
        var tournamentsQueryable = await GetTournamentsFromRunningEventAsQueryable();
        if (tournamentsQueryable is null)
        {
            return new List<Tournament>();
        }

        return await tournamentsQueryable.ToListAsync();
    }

    public async Task<IList<Tournament>> GetTournamentsWithFinishedMatches()
    {
        var tournamentsQueryable = await GetTournamentsFromRunningEventAsQueryable();
        if (tournamentsQueryable is null)
        {
            return new List<Tournament>();
        }

        var tournaments = await tournamentsQueryable.Where(
            t => t.State == TournamentState.Running && t.Rounds.Any(
                r => r.Matches.Any(
                    m => m.IsFinishedMatch()
                )
            )
        ).ToListAsync();

        return tournaments;
    }

    public async Task<IList<Tournament>> GetTournamentsWithUpcomingMatches()
    {
        var tournamentsQueryable = await GetTournamentsFromRunningEventAsQueryable();
        if (tournamentsQueryable is null)
        {
            return new List<Tournament>();
        }

        var tournaments = await tournamentsQueryable.Where(
            t => t.State == TournamentState.Running && t.Rounds.Any(
                r => r.Matches.Any(
                    m => !m.IsFinishedMatch()
                )
            )
        ).ToListAsync();

        return tournaments;
    }

    private async Task<IQueryable<Tournament>?> GetTournamentsFromRunningEventAsQueryable()
    {
        var runningEvent = await _eventService.GetRunningEventAsync();
        if (runningEvent is null)
        {
            return null;
        }

        return _tournamentRepository.GetTournaments().Where(t => t.EventId == runningEvent.Id);
    }

    private async Task ScorePseudoMatches(IEnumerable<TournamentMatch> matches)
    {
        foreach (var tournamentMatch in matches)
        {
            if (!tournamentMatch.IsByeMatch()) continue;

            foreach (var tournamentMatchScore in tournamentMatch.Scores)
            {
                tournamentMatchScore.Score = tournamentMatchScore.TournamentTeam.IsByeTeam ? 0 : 1;
            }

            await _tournamentMatchService.ScoreMatchAsync(tournamentMatch);
        }
    }

    // private async Task SendUpdateTournamentEntryToLps(Tournament tournament)
    // {
    //     var updateTournamentEntry = new UpdateTournamentEntry
    //     {
    //         Mode = tournament.Mode.ToLps(),
    //         State = tournament.State.ToLps(),
    //         Name = tournament.Name,
    //         TeamLimit = _lanpartyScreenOptions.TeamLimit,
    //         StartTime = tournament.CreatedAt,
    //         PlayersPerTeam = tournament.PlayerPerTeam ?? 1,
    //         Teams = tournament.Teams.Count(tt => !tt.IsByeTeam)
    //     };
    //     await _tournamentsClient.TournamentsPUTAsync(tournament.LanpartyScreenId!.Value, updateTournamentEntry);
    // }

    // private async Task SendCreateTournamentEntryToLps(Tournament tournament)
    // {
    //     var createTournamentEntry = new CreateTournamentEntry
    //     {
    //         Mode = tournament.Mode.ToLps(),
    //         State = tournament.State.ToLps(),
    //         Name = tournament.Name,
    //         TeamLimit = _lanpartyScreenOptions.TeamLimit,
    //         StartTime = DateTime.Now,
    //         PlayersPerTeam = tournament.PlayerPerTeam ?? 1,
    //         Teams = 0,
    //         Matches = new List<Match>()
    //     };
    //
    //     var tournamentEntryResponse = await _tournamentsClient.TournamentsPOSTAsync(createTournamentEntry);
    //     tournament.LanpartyScreenId = tournamentEntryResponse.Id;
    //     await _tournamentRepository.UpdateTournamentAsync(tournament);
    // }

    // private async Task SendDeleteTournamentEntryToLps(Tournament tournament)
    // {
    //     await _tournamentsClient.TournamentsDELETEAsync(tournament.LanpartyScreenId!.Value);
    // }

    // private async Task SendUpdateTournamentEntryForStartToLps(Tournament tournament)
    // {
    //     foreach (var tournamentRound in tournament.Rounds)
    //     {
    //         foreach (var tournamentMatch in tournamentRound.Matches)
    //         {
    //             var teams = new List<Team>();
    //             foreach (var tournamentMatchScore in tournamentMatch.Scores)
    //             {
    //                 if (tournamentMatchScore.TournamentTeamId is null)
    //                 {
    //                     continue;
    //                 }
    //
    //                 teams.Add(new Team {Name = TeamName(tournamentMatchScore), Points = 0});
    //             }
    //
    //             var match = await _tournamentsClient.MatchesPOSTAsync(tournament.LanpartyScreenId!.Value, teams);
    //
    //             tournamentMatch.LanpartyScreenMatchId = match.Id;
    //             await _tournamentRepository.UpdateTournamentAsync(tournament);
    //
    //             await SendUpdateTournamentEntryToLps(tournament);
    //         }
    //     }
    // }

    public static string TeamName(TournamentMatchScore tournamentMatchScore)
    {
        string teamName;
        if (tournamentMatchScore.TournamentTeam.IsByeTeam)
        {
            teamName = "Freilos";
        }
        else if (tournamentMatchScore.TournamentTeam.IsSinglePersonTeam)
        {
            teamName = tournamentMatchScore.TournamentTeam.Members[0].Member.Nickname!;
        }
        else
        {
            teamName = tournamentMatchScore.TournamentTeam.Name;
        }

        return teamName;
    }
}
