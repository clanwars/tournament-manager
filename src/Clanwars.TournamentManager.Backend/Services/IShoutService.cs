using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface IShoutService
{
    Task<PagedResult<Shout>> GetShoutsAsync(SieveModel sieveModel);
    Task<Shout?> GetShoutByIdAsync(int shoutId);
    Task AddShoutAsync(Shout shout);
    Task DeleteShoutAsync(Shout shout);
    Task UpdateShoutAsync(Shout shout);

    Task<Shout?> GetRandomAsync();
}
