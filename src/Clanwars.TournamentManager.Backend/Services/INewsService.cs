using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface INewsService
{
    Task<PagedResult<News>> GetNewsAsync(SieveModel sieveModel);
    Task<News?> GetNewsByIdAsync(int newsId);
    Task AddNewsAsync(News news);
    Task DeleteNewsAsync(News news);
    Task UpdateNewsAsync(News news);

    Task<News?> GetRandomAsync();
}
