using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class EventService(
    IEventRepository eventRepository,
    IHubContext<NotificationsHub, INotificationsSender> notificationsHub,
    IBeamerContentProvider beamerContentProvider
) : IEventService
{
    public Task<PagedResult<Event>> GetEventsAsync(SieveModel sieveModel)
    {
        return eventRepository.GetEventsAsync(sieveModel);
    }

    public async Task<Event?> GetRunningEventAsync()
    {
        var runningEvent = await eventRepository.GetEvents()
            .Where(e => e.EndDate >= DateTime.Now)
            .FirstOrDefaultAsync();

        return runningEvent;
    }

    public Task<Event?> GetEventByIdAsync(int eventId)
    {
        return eventRepository.GetEventByIdAsync(eventId);
    }

    public async Task AddEventAsync(Event @event)
    {
        await eventRepository.AddEventAsync(@event);
        await notificationsHub.Clients.All.SendEventsChanged(new EventHubMessage
        {
            Id = @event.Id,
            Event = EventChangeEvent.Created
        });
    }

    public async Task DeleteEventAsync(Event @event)
    {
        await eventRepository.DeleteEventAsync(@event);
        await notificationsHub.Clients.All.SendEventsChanged(new EventHubMessage
        {
            Id = @event.Id,
            Event = EventChangeEvent.Deleted
        });
    }

    public async Task UpdateEventAsync(Event @event)
    {
        await eventRepository.UpdateEventAsync(@event);
        await notificationsHub.Clients.All.SendEventsChanged(new EventHubMessage
        {
            Id = @event.Id,
            Event = EventChangeEvent.Changed
        });
    }

    public async Task AddUserToEventAsync(Event @event, int userId)
    {
        var createdEventUser = await eventRepository.AddUserToEventAsync(@event, userId);
        await notificationsHub.Clients.All.SendEventsChanged(new EventHubMessage
        {
            Id = @event.Id,
            Event = EventChangeEvent.Changed
        });

        beamerContentProvider.QueueWelcomeMessage(createdEventUser.UserId);
    }

    public async Task RemoveUserFromEventAsync(Event @event, int userId)
    {
        await eventRepository.RemoveUserFromEventAsync(@event, userId);
        await notificationsHub.Clients.All.SendEventsChanged(new EventHubMessage
        {
            Id = @event.Id,
            Event = EventChangeEvent.Changed
        });
    }

    public Task<PagedResult<EventUser>> GetRegistrationsAsync(SieveModel sieveModel)
    {
        return eventRepository.GetRegistrationsAsync(sieveModel);
    }
}
