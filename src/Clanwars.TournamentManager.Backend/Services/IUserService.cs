using System.Security.Claims;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface IUserService
{
    Task<PagedResult<User>> GetUsersAsync(SieveModel sieveModel);
    Task<IList<User>> GetAllUsersAsync();
    Task<User?> GetByIdAsync(int userId);
    Task<User> GetByClaimsPrincipalAsync(ClaimsPrincipal principal);
    Task<bool> IsUniqueNickname(int userId, string nickname);
    Task UpdateUserAsync(User user);
    Task<User> CreateUserSkeletonAsync(string email, string firstName, string lastName);
    Task ResetPassword(int userId);
}
