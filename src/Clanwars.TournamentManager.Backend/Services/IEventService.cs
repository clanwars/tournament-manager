using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface IEventService
{
    Task<PagedResult<Event>> GetEventsAsync(SieveModel sieveModel);
    Task<Event?> GetEventByIdAsync(int eventId);
    Task<Event?> GetRunningEventAsync();
    Task AddEventAsync(Event @event);
    Task DeleteEventAsync(Event @event);
    Task UpdateEventAsync(Event @event);
    Task AddUserToEventAsync(Event @event, int userId);
    Task RemoveUserFromEventAsync(Event @event, int userId);
    Task<PagedResult<EventUser>> GetRegistrationsAsync(SieveModel sieveModel);
}
