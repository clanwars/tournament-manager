using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class ClanService : IClanService
{
    private readonly IClanRepository _clanRepository;
    private readonly IHubContext<NotificationsHub, INotificationsSender> _notificationsHub;

    public ClanService(
        IClanRepository clanRepository,
        IHubContext<NotificationsHub, INotificationsSender> notificationsHub
    )
    {
        _clanRepository = clanRepository;
        _notificationsHub = notificationsHub;
    }

    public Task<PagedResult<Clan>> GetClansAsync(SieveModel sieveModel)
    {
        return _clanRepository.GetClansAsync(sieveModel);
    }

    public Task<Clan?> GetClanByIdAsync(int clanId)
    {
        return _clanRepository.GetClanByIdAsync(clanId);
    }

    public async Task AddClanAsync(Clan clan)
    {
        clan.CreatedAt = DateTime.Now;
        await _clanRepository.AddClanAsync(clan);
        await _notificationsHub.Clients.All.SendClansChanged(new ClanHubMessage
        {
            Id = clan.Id,
            Event = ClanChangeEvent.Created
        });
    }

    public async Task DeleteClanAsync(Clan clan)
    {
        await _clanRepository.DeleteClanAsync(clan);
        await _notificationsHub.Clients.All.SendClansChanged(new ClanHubMessage
        {
            Id = clan.Id,
            Event = ClanChangeEvent.Deleted
        });
    }

    public async Task UpdateClanAsync(Clan clan)
    {
        clan.UpdatedAt = DateTime.Now;
        await _clanRepository.UpdateClanAsync(clan);
        await _notificationsHub.Clients.All.SendClansChanged(new ClanHubMessage
        {
            Id = clan.Id,
            Event = ClanChangeEvent.Changed
        });
    }

    public async Task AddUserToClanAsync(Clan clan, User user, EntityPermission permission)
    {
        clan.UpdatedAt = DateTime.Now;
        await _clanRepository.AddUserToClanAsync(clan, user, permission);
        await _notificationsHub.Clients.All.SendClansChanged(new ClanHubMessage
        {
            Id = clan.Id,
            Event = ClanChangeEvent.Changed
        });
    }

    public async Task RemoveClanUserAsync(ClanUser clanUser)
    {
        var clanId = clanUser.Clan.Id;
        clanUser.Clan.UpdatedAt = DateTime.Now;
        await _clanRepository.RemoveClanUserAsync(clanUser);
        await _notificationsHub.Clients.All.SendClansChanged(new ClanHubMessage
        {
            Id = clanId,
            Event = ClanChangeEvent.Changed
        });
    }
}
