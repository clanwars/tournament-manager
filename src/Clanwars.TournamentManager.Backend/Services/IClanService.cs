using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface IClanService
{
    Task<PagedResult<Clan>> GetClansAsync(SieveModel sieveModel);
    Task<Clan?> GetClanByIdAsync(int clanId);
    Task AddClanAsync(Clan clan);
    Task DeleteClanAsync(Clan clan);
    Task UpdateClanAsync(Clan clan);
    Task AddUserToClanAsync(Clan clan, User user, EntityPermission permission);
    Task RemoveClanUserAsync(ClanUser clanUser);
}
