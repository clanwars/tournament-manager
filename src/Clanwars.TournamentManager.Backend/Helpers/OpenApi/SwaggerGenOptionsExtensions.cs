using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Clanwars.TournamentManager.Backend.Helpers.OpenApi;

public static class SwaggerGenOptionsExtensions
{
    /// <summary>
    /// Adds special Operation, Schema, Document filters to Swashbuckle filter pipeline
    /// </summary>
    public static void UseCommonFilters(this SwaggerGenOptions options)
    {
        options.OperationFilter<AuthorizeCheckOperationFilter>();
        options.OperationFilter<FilterableEntityOperationFilter>();
    }
}
