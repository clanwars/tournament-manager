using System.Security.Claims;

namespace Clanwars.TournamentManager.Backend.Helpers.ClaimsPrincipal;

public static class ClaimsPrincipalExtensions
{
    /// <exception cref="ArgumentNullException">When no EmailAddress claim was found in ClaimsPrincipal</exception>
    public static string GetEmail(this System.Security.Claims.ClaimsPrincipal claimsIdentity)
    {
        var email = claimsIdentity.FindFirstValue(ClaimTypes.Email);
        if (email == null)
        {
            throw new ArgumentNullException(nameof(claimsIdentity), "No email was found in ClaimsIdentity");
        }

        return email;
    }
}
