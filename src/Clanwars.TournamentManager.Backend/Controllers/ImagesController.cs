using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.ClaimsPrincipal;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Extensions;
using Clanwars.TournamentManager.Lib.Messages;
using Clanwars.TournamentManager.Lib.Messages.Images;
using HashidsNet;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class ImagesController : ControllerBase
{
    private readonly ILogger<ImagesController> _logger;
    private readonly IImageService _imageService;
    private readonly ITournamentService _tournamentService;
    private readonly ITournamentTeamService _tournamentTeamService;
    private readonly IClanService _clanService;
    private readonly IUserService _userService;
    private readonly IHashids _hashids;
    private readonly IMapper _mapper;

    public ImagesController(
        ILogger<ImagesController> logger,
        IImageService imageService,
        ITournamentService tournamentService,
        ITournamentTeamService tournamentTeamService,
        IClanService clanService,
        IUserService userService,
        IHashids hashids, IMapper mapper)
    {
        _logger = logger;
        _imageService = imageService;
        _tournamentService = tournamentService;
        _tournamentTeamService = tournamentTeamService;
        _clanService = clanService;
        _userService = userService;
        _hashids = hashids;
        _mapper = mapper;
    }

    [Authorize(Roles = "tournament-moderator")]
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<ImageResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(Image))]
    public async Task<ActionResult<IEnumerable<ImageResponse>>> GetImages([FromQuery] SieveModel sieveModel)
    {
        var pagedResult = await _imageService.GetImagesAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<ImageResponse>>(pagedResult));
    }

    [AllowAnonymous]
    [HttpGet("{imageHashId}")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ResponseCache(Duration = 2592000, Location = ResponseCacheLocation.Client)]
    public async Task<IActionResult> GetImage(
        [FromRoute] string imageHashId
    )
    {
        var rawId = _hashids.Decode(imageHashId);

        if (rawId.Length == 0)
        {
            return NotFound();
        }

        var image = await _imageService.GetImageWithContentByIdAsync(rawId[0]);
        if (image == null)
        {
            return NotFound();
        }

        var match = Regex.Match(image.Content, "data:(?<type>.+?);base64,(?<data>.+)");
        if (!match.Success)
        {
            return NotFound();
        }

        var base64Data = match.Groups["data"].Value;
        var mimeType = match.Groups["type"].Value;
        var binData = Convert.FromBase64String(base64Data);

        return File(binData, mimeType);
    }

    [HttpPut("{imageHashId}/beamer/hide")]
    [Authorize(Roles = "tournament-moderator")]
    public async Task<IActionResult> SetHideOnStream([FromRoute] string imageHashId)
    {
        return await ShowImageOnStream(imageHashId, false);
    }
    
    
    [HttpPut("{imageHashId}/beamer/show")]
    [Authorize(Roles = "tournament-moderator")]
    public async Task<IActionResult> SetShowOnStream([FromRoute] string imageHashId)
    {
        return await ShowImageOnStream(imageHashId, true);
    }

    private async Task<IActionResult> ShowImageOnStream(string imageHashId, bool showOnStream)
    {
        var rawId = _hashids.Decode(imageHashId);

        if (rawId.Length == 0)
        {
            return NotFound();
        }

        var image = await _imageService.GetImageByIdAsync(rawId[0]);
        if (image == null)
        {
            return NotFound();
        }

        image.ShowOnStream = showOnStream;
        await _imageService.UpdateImageAsync(image);

        return NoContent();
    }

    [Authorize(Roles = "tournament-moderator")]
    [HttpDelete("{imageHashId}")]
    public async Task<IActionResult> DeleteImage(
        [FromRoute] string imageHashId
    )
    {
        var imageId = _hashids.Decode(imageHashId)[0];
        var image = await _imageService.GetImageWithContentByIdAsync(imageId);

        if (image is null)
        {
            return NotFound();
        }

        await _imageService.RemoveImageAsync(imageId);
        return NoContent();
    }

    [Authorize(Roles = "tournament-moderator")]
    [HttpPost("upload/tournament/{tournamentId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> SaveTournamentImage(
        [FromRoute] int tournamentId,
        [FromBody] ImageUpload imageUpload
    )
    {
        var tournament = await _tournamentService.GetTournamentByIdAsync(tournamentId);

        if (tournament is null)
        {
            return NotFound();
        }

        var imageId = await CheckPicture(ImageType.TournamentImage, imageUpload.DataUrl);

        if (imageId is null)
        {
            var image = await _imageService.AddImageWithContentAsync(ImageType.TournamentImage, imageUpload.DataUrl);
            tournament.ImageId = image.Id;
        }
        else
        {
            tournament.ImageId = imageId;
        }

        await _tournamentService.UpdateTournamentAsync(tournament);

        return NoContent();
    }

    // TODO check permissions
    [HttpPost("upload/tournament-team/{tournamentTeamId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> SaveTournamentTeamImage(
        [FromRoute] int tournamentTeamId,
        [FromBody] ImageUpload imageUpload
    )
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);

        if (tournamentTeam is null)
        {
            return NotFound();
        }

        var imageId = await CheckPicture(ImageType.TournamentTeamImage, imageUpload.DataUrl);

        if (imageId is null)
        {
            var image = await _imageService.AddImageWithContentAsync(ImageType.TournamentTeamImage, imageUpload.DataUrl);
            tournamentTeam.ImageId = image.Id;
        }
        else
        {
            tournamentTeam.ImageId = imageId;
        }

        await _tournamentTeamService.UpdateTournamentTeamAsync(tournamentTeam);

        return NoContent();
    }

    // TODO check permissions
    [HttpPost("upload/clan/{clanId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> SaveClanImage(
        [FromRoute] int clanId,
        [FromBody] ImageUpload imageUpload
    )
    {
        var clan = await _clanService.GetClanByIdAsync(clanId);

        if (clan is null)
        {
            return NotFound();
        }

        var imageId = await CheckPicture(ImageType.ClanImage, imageUpload.DataUrl);

        if (imageId is null)
        {
            var image = await _imageService.AddImageWithContentAsync(ImageType.ClanImage, imageUpload.DataUrl);
            clan.ImageId = image.Id;
        }
        else
        {
            clan.ImageId = imageId;
        }

        await _clanService.UpdateClanAsync(clan);

        return NoContent();
    }

    [HttpPost("upload/user/{userId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> SaveUserImage(
        [FromRoute] int userId,
        [FromBody] ImageUpload imageUpload
    )
    {
        var user = await _userService.GetByIdAsync(userId);

        if (user is null)
        {
            return NotFound();
        }

        if (User.GetEmail() != user.Email)
        {
            return Forbid();
        }

        var imageId = await CheckPicture(ImageType.UserImage, imageUpload.DataUrl);

        if (imageId is null)
        {
            var image = await _imageService.AddImageWithContentAsync(ImageType.UserImage, imageUpload.DataUrl);
            user.ImageId = image.Id;
        }
        else
        {
            user.ImageId = imageId;
        }

        await _userService.UpdateUserAsync(user);

        return NoContent();
    }

    [Authorize(Roles = "tournament-moderator")]
    [HttpPost("upload/beamer")]
    public async Task<IActionResult> SaveBeamerImage([FromBody] ImageUpload imageUpload)
    {
        var imageId = await CheckPicture(ImageType.Beamer, imageUpload.DataUrl);

        if (imageId is null)
        {
            await _imageService.AddImageWithContentAsync(ImageType.Beamer, imageUpload.DataUrl);
        }

        return NoContent();
    }

    private async Task<int?> CheckPicture(ImageType imageType, string newImageDataUrl)
    {
        var newHash = newImageDataUrl.Sha256();
        var image = await _imageService.GetImageWithContentByHashAndImageTypeAsync(newHash, imageType);
        return image?.Id;
    }
}
