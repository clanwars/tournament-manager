using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Models;
using Clanwars.TournamentManager.Backend.Models.BackendValidations.User;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Messages.Clans;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class ClansController : ControllerBase
{
    private readonly IClanService _clanService;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public ClansController(
        IClanService clanService,
        IUserService userService,
        IMapper mapper
    )
    {
        _clanService = clanService;
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<ClanResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(Clan))]
    public async Task<ActionResult<IEnumerable<ClanResponse>>> GetClans(
        [FromQuery] SieveModel sieveModel
    )
    {
        var pagedResult = await _clanService.GetClansAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<ClanResponse>>(pagedResult));
    }

    /// <response code="409">Conflict - You are already member of a clan.</response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<ClanResponse>> CreateClan(
        [FromBody] CreateClanMessage createClanMessage
    )
    {
        var user = await _userService.GetByClaimsPrincipalAsync(User);
        if (user.ClanId != null)
        {
            return Conflict();
        }

        var newClan = _mapper.Map<Clan>(createClanMessage);
        await _clanService.AddClanAsync(newClan);
        await _clanService.AddUserToClanAsync(newClan, user, EntityPermission.Owner);

        return CreatedAtAction(nameof(GetClans), new { filters = $"id=={newClan.Id}" }, _mapper.Map<ClanResponse>(newClan));
    }


    [HttpPut("{clanId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<ClanResponse>> UpdateClan(
        [FromBody] ClanUpdateRequest updateRequest
    )
    {
        var clan = await _clanService.GetClanByIdAsync(updateRequest.ClanId);

        if (clan is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!clan.IsClanOwner(currentUser))
        {
            return Forbid();
        }

        var updatedClan = _mapper.Map(updateRequest, clan);
        await _clanService.UpdateClanAsync(updatedClan);

        return Ok(_mapper.Map<ClanResponse>(updatedClan));
    }

    /// <response code="409">Conflict - You are already member of a clan.</response>
    [HttpPut("{clanId:int}/join")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<ClanResponse>> JoinClan(
        [FromRoute] int clanId
    )
    {
        var clan = await _clanService.GetClanByIdAsync(clanId);

        if (clan is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (currentUser.ClanId != null)
        {
            return Conflict("You are already member of a clan.");
        }

        await _clanService.AddUserToClanAsync(clan, currentUser, EntityPermission.Pending);

        return Ok(_mapper.Map<ClanResponse>(clan));
    }

    [HttpPut("{clanId:int}/approve-join/{targetUserId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<ClanResponse>> ApproveJoinClan(
        [FromRoute] int clanId,
        [IsValidUserId] [FromRoute] int targetUserId
    )
    {
        var clan = await _clanService.GetClanByIdAsync(clanId);

        if (clan is null)
        {
            return NotFound();
        }

        var targetUser = clan.GetClanMember(targetUserId);

        if (targetUser is null)
        {
            return BadRequest();
        }

        if (targetUser.Permission != EntityPermission.Pending)
        {
            return BadRequest();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!clan.IsClanOwner(currentUser))
        {
            return Forbid();
        }

        targetUser.Permission = EntityPermission.Member;
        targetUser.User.ClanId = clanId;
        targetUser.User.Clan = clan;
        await _clanService.UpdateClanAsync(clan);

        return Ok(_mapper.Map<ClanResponse>(clan));
    }

    [HttpPut("{clanId:int}/leave")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> LeaveClan(
        [FromRoute] int clanId
    )
    {
        var clan = await _clanService.GetClanByIdAsync(clanId);

        if (clan is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (currentUser.ClanId != clanId)
        {
            return Forbid();
        }

        var currentClanUser = clan.GetClanMember(currentUser.Id)!;

        clan.Members.Remove(currentClanUser);
        await _clanService.RemoveClanUserAsync(currentClanUser);

        if (clan.Members.Where(cu => cu.Permission != EntityPermission.Pending).ToList().Count == 0)
        {
            var pendingClanMembers = clan.Members.Where(cu => cu.Permission == EntityPermission.Pending).Select(cu => cu.User);
            foreach (var pendingClanMember in pendingClanMembers)
            {
                pendingClanMember.Clan = null;
                pendingClanMember.ClanId = null;
            }

            await _clanService.DeleteClanAsync(clan);
            return NoContent();
        }

        if (clan.Members.All(cu => cu.Permission != EntityPermission.Owner))
        {
            clan.Members.First(cu => cu.Permission == EntityPermission.Member).Permission = EntityPermission.Owner;
            await _clanService.UpdateClanAsync(clan);
        }

        return NoContent();
    }

    [HttpPut("{clanId:int}/remove-user/{targetUserId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> RemoveUserFromClan(
        [FromRoute] int clanId,
        [IsValidUserId] [FromRoute] int targetUserId
    )
    {
        var clan = await _clanService.GetClanByIdAsync(clanId);
        if (clan is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!CanModifyClanUser(currentUser, clan, targetUserId))
        {
            return Forbid();
        }

        var targetClanUser = clan.GetClanMember(targetUserId);

        if (targetClanUser is null)
        {
            return NotFound();
        }

        clan.Members.Remove(targetClanUser);
        await _clanService.RemoveClanUserAsync(targetClanUser);

        return NoContent();
    }

    [HttpPut("{clanId:int}/revoke-ownership/{targetUserId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> RevokeOwnerPermission(
        [FromRoute] int clanId,
        [FromRoute] [IsValidUserId] int targetUserId
    )
    {
        var clan = await _clanService.GetClanByIdAsync(clanId);
        if (clan is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!CanModifyClanUser(currentUser, clan, targetUserId))
        {
            return Forbid();
        }

        var targetUser = clan.GetClanMember(targetUserId);

        // targetUser is not part of clan
        if (targetUser is null)
        {
            return Forbid();
        }

        // check if targetUser is a owner
        if (targetUser.Permission != EntityPermission.Owner)
        {
            return NoContent();
        }

        targetUser.Permission = EntityPermission.Member;
        await _clanService.UpdateClanAsync(clan);

        return NoContent();
    }

    [HttpPut("{clanId:int}/grant-ownership/{targetUserId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> GrantOwnerPermission(
        [FromRoute] int clanId,
        [FromRoute] [IsValidUserId] int targetUserId
    )
    {
        var clan = await _clanService.GetClanByIdAsync(clanId);

        if (clan is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!CanModifyClanUser(currentUser, clan, targetUserId))
        {
            return Forbid();
        }

        var targetClanUser = clan.GetClanMember(targetUserId);

        // check if targetUser is not part of the specified clan
        if (targetClanUser is null)
        {
            return Forbid();
        }

        // make targetUser an owner
        targetClanUser.Permission = EntityPermission.Owner;

        await _clanService.UpdateClanAsync(clan);

        return NoContent();
    }

    private bool CanModifyClanUser(
        User currentUser,
        Clan clan,
        int targetUserId
    )
    {
        // user who is calling this function tries to revoke its owner permissions
        if (currentUser.Id == targetUserId)
        {
            return false;
        }

        // user who is calling this function is not part of this clan
        if (currentUser.ClanId != clan.Id)
        {
            return false;
        }

        // user who is calling this function is not owner of this clan
        if (!clan.IsClanOwner(currentUser))
        {
            return false;
        }

        return true;
    }
}
