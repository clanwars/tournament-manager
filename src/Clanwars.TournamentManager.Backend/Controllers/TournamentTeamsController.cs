using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Entities.Tournaments.Extensions;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Models;
using Clanwars.TournamentManager.Backend.Models.BackendValidations.User;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class TournamentTeamsController : ControllerBase
{
    private readonly ITournamentTeamService _tournamentTeamService;
    private readonly ITournamentService _tournamentService;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public TournamentTeamsController(
        ITournamentTeamService tournamentTeamService,
        ITournamentService tournamentService,
        IUserService userService,
        IMapper mapper
    )
    {
        _tournamentTeamService = tournamentTeamService;
        _tournamentService = tournamentService;
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<TournamentTeamResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(TournamentTeam))]
    public async Task<ActionResult<IEnumerable<TournamentTeamResponse>>> GetTournamentTeams(
        [FromQuery] SieveModel sieveModel
    )
    {
        var pagedResult = await _tournamentTeamService.GetTournamentTeamsAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<TournamentTeamResponse>>(pagedResult));
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<TournamentResponse>> CreateTournamentTeam(
        [FromBody] CreateTournamentTeamMessage createTournamentTeamMessage
    )
    {
        var user = await _userService.GetByClaimsPrincipalAsync(User);

        var tournament = (await _tournamentService.GetTournamentByIdAsync(createTournamentTeamMessage.TournamentId))!;

        var hasTeam = tournament.Teams.Any(tt => tt.Members.Any(ttm => ttm.MemberId == user.Id));

        if (hasTeam)
        {
            return Conflict("you are already member of a team for this tournament");
        }

        var newTournamentTeam = _mapper.Map<TournamentTeam>(createTournamentTeamMessage);
        newTournamentTeam.Tournament = tournament;
        
        await _tournamentTeamService.AddTournamentTeamAsync(newTournamentTeam);
        await _tournamentTeamService.AddUserToTeamAsync(newTournamentTeam, user.Id, EntityPermission.Owner);

        return CreatedAtAction(nameof(GetTournamentTeams), new {filters = $"id=={newTournamentTeam.Id}"}, _mapper.Map<TournamentTeamResponse>(newTournamentTeam));
    }

    [HttpPut("{tournamentTeamId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<TournamentTeamResponse>> UpdateTournamentTeam(
        [FromBody] UpdateTournamentTeamRequest updateTournamentTeamRequest,
        [FromRoute] int tournamentTeamId)
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);

        if (tournamentTeam is null)
        {
            return NotFound();
        }

        var updatedTournamentTeam = _mapper.Map(updateTournamentTeamRequest, tournamentTeam);

        await _tournamentTeamService.UpdateTournamentTeamAsync(tournamentTeam);
        return Ok(_mapper.Map<TournamentTeamResponse>(updatedTournamentTeam));
    }

    [HttpDelete("{tournamentTeamId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> DeleteTournamentTeam(
        [FromRoute] int tournamentTeamId
    )
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);

        if (tournamentTeam is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!tournamentTeam.Members.Any(m => m.MemberId == currentUser.Id && m.Permission == EntityPermission.Owner))
        {
            return Forbid();
        }

        if (tournamentTeam.Sealed)
        {
            return BadRequest("TournamentTeam is already sealed");
        }

        await _tournamentTeamService.DeleteTournamentTeamAsync(tournamentTeam);
        return Ok();
    }


    [HttpPost("{tournamentTeamId:int}/join")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<TournamentTeamResponse>> JoinTournamentTeam(
        [FromRoute] int tournamentTeamId
    )
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);

        if (tournamentTeam is null)
        {
            return NotFound();
        }

        if (tournamentTeam.Sealed)
        {
            return BadRequest("Tournament Team is already sealed");
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        await _tournamentTeamService.AddUserToTeamAsync(tournamentTeam, currentUser.Id, EntityPermission.Pending);

        return Ok(_mapper.Map<TournamentTeamResponse>(tournamentTeam));
    }

    [HttpPut("{tournamentTeamId:int}/approve-join/{targetUserId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<TournamentTeamResponse>> ApproveJoinTournamentTeam(
        [FromRoute] int tournamentTeamId,
        [FromRoute] [IsValidUserId] int targetUserId
    )
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);

        if (tournamentTeam is null)
        {
            return NotFound();
        }

        var targetUser = tournamentTeam.GetTournamentTeamMember(targetUserId);

        if (targetUser is null)
        {
            return BadRequest();
        }

        if (targetUser.Permission != EntityPermission.Pending)
        {
            return BadRequest();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!tournamentTeam.IsOwner(currentUser))
        {
            return Forbid();
        }

        targetUser.Permission = EntityPermission.Member;

        await _tournamentTeamService.UpdateTournamentTeamAsync(tournamentTeam);

        return Ok(_mapper.Map<TournamentTeamResponse>(tournamentTeam));
    }


    [HttpPost("{tournamentTeamId:int}/leave")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> LeaveTournamentTeam(
        [FromRoute] int tournamentTeamId
    )
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);

        if (tournamentTeam is null)
        {
            return NotFound();
        }

        if (tournamentTeam.Sealed)
        {
            return BadRequest("TournamentTeam is already sealed");
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        var currentTournamentTeamMember = tournamentTeam.GetTournamentTeamMember(currentUser.Id);

        if (currentTournamentTeamMember is null)
        {
            return Forbid();
        }

        await _tournamentTeamService.RemoveUserFromTeamAsync(tournamentTeam, currentTournamentTeamMember);

        if (tournamentTeam.Members.Where(cu => cu.Permission != EntityPermission.Pending).ToList().Count == 0)
        {
            await _tournamentTeamService.DeleteTournamentTeamAsync(tournamentTeam);
            return NoContent();
        }

        if (tournamentTeam.Members.All(cu => cu.Permission != EntityPermission.Owner))
        {
            tournamentTeam.Members.First(ttm => ttm.Permission == EntityPermission.Member).Permission = EntityPermission.Owner;
            await _tournamentTeamService.UpdateTournamentTeamAsync(tournamentTeam);
        }

        return NoContent();
    }

    [HttpPut("{tournamentTeamId:int}/remove-user/{targetUserId:int}")]
    public async Task<ActionResult> RemoveUserFromTournamentTeam(
        [FromRoute] int tournamentTeamId,
        [FromRoute] [IsValidUserId] int targetUserId
    )
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);
        if (tournamentTeam is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!CanModifyTournamentTeamMember(currentUser, tournamentTeam, targetUserId))
        {
            return Forbid();
        }

        var targetTournamentTeamMember = tournamentTeam.GetTournamentTeamMember(targetUserId);

        if (targetTournamentTeamMember is null)
        {
            return NotFound();
        }

        await _tournamentTeamService.RemoveUserFromTeamAsync(tournamentTeam, targetTournamentTeamMember);

        return NoContent();
    }

    [HttpPut("{tournamentTeamId:int}/revoke-ownership/{targetUserId:int}")]
    public async Task<ActionResult> RevokeOwnerPermission(
        [FromRoute] int tournamentTeamId,
        [FromRoute] [IsValidUserId] int targetUserId
    )
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);
        if (tournamentTeam is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!CanModifyTournamentTeamMember(currentUser, tournamentTeam, targetUserId))
        {
            return Forbid();
        }

        var targetUser = tournamentTeam.GetTournamentTeamMember(targetUserId);

        if (targetUser is null)
        {
            return Forbid();
        }

        if (targetUser.Permission != EntityPermission.Owner)
        {
            return NoContent();
        }

        targetUser.Permission = EntityPermission.Member;
        await _tournamentTeamService.UpdateTournamentTeamAsync(tournamentTeam);

        return NoContent();
    }

    [HttpPut("{tournamentTeamId:int}/grant-ownership/{targetUserId:int}")]
    public async Task<ActionResult> GrantOwnerPermission(
        [FromRoute] int tournamentTeamId,
        [FromRoute] [IsValidUserId] int targetUserId
    )
    {
        var tournamentTeam = await _tournamentTeamService.GetTournamentTeamByIdAsync(tournamentTeamId);

        if (tournamentTeam is null)
        {
            return NotFound();
        }

        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!CanModifyTournamentTeamMember(currentUser, tournamentTeam, targetUserId))
        {
            return Forbid();
        }

        var targetTournamentTeamMember = tournamentTeam.GetTournamentTeamMember(targetUserId);

        // check if targetUser is not part of the specified clan
        if (targetTournamentTeamMember is null)
        {
            return Forbid();
        }

        // make targetUser an owner
        targetTournamentTeamMember.Permission = EntityPermission.Owner;

        await _tournamentTeamService.UpdateTournamentTeamAsync(tournamentTeam);

        return NoContent();
    }

    private bool CanModifyTournamentTeamMember(
        User currentUser,
        TournamentTeam tournamentTeam,
        int targetUserId
    )
    {
        // user who is calling this function tries to revoke its owner permissions
        if (currentUser.Id == targetUserId)
        {
            return false;
        }

        var currentTournamentTeamMember = tournamentTeam.GetTournamentTeamMember(currentUser.Id);

        // user who is calling this function is not part of this tournament team
        if (currentTournamentTeamMember is null)
        {
            return false;
        }

        // user who is calling this function is not owner of this tournamentTeam
        if (!tournamentTeam.IsOwner(currentUser))
        {
            return false;
        }

        return true;
    }
}
