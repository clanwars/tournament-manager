using Clanwars.TournamentManager.Backend.Entities.Rooms;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Models;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Rooms;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class RoomsController : ControllerBase
{
    private readonly IRoomService _roomService;
    private readonly IMapper _mapper;

    public RoomsController(
        IRoomService roomService,
        IMapper mapper
    )
    {
        _roomService = roomService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<RoomResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(Room))]
    public async Task<ActionResult<PagedResult<RoomResponse>>> GetRooms(
        [FromQuery] SieveModel sieveModel
    )
    {
        var pagedResult = await _roomService.GetRoomsAsync(sieveModel);
        return Ok(_mapper.Map<PagedResult<RoomResponse>>(pagedResult));
    }

    [HttpPost]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(RoomResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<RoomResponse>> CreateRoom(
        [FromBody] CreateRoomMessage createRoomMessage
    )
    {
        var room = _mapper.Map<Room>(createRoomMessage);

        await _roomService.AddRoomAsync(room);

        return CreatedAtAction(nameof(GetRooms), new {filters = $"id=={room.Id}"}, _mapper.Map<RoomResponse>(room));
    }

    [HttpPut("{roomId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<RoomResponse>> UpdateRoom(
        [FromBody] UpdateRoomRequest updateRoom,
        [FromRoute] int roomId
    )
    {
        var room = await _roomService.GetRoomByIdAsync(roomId);

        if (room is null)
        {
            return NotFound();
        }

        _mapper.Map(updateRoom, room);

        await _roomService.UpdateRoomAsync(room);
        return Ok(_mapper.Map<RoomResponse>(room));
    }

    [HttpDelete("{roomId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteRoom(
        [FromRoute] int roomId
    )
    {
        var room = await _roomService.GetRoomByIdAsync(roomId);

        if (room is null)
        {
            return NotFound();
        }

        await _roomService.DeleteRoomAsync(room);
        return NoContent();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="updateGrid"></param>
    /// <param name="roomId"></param>
    /// <returns></returns>
    /// <response code="204">Empty result on deleting a gridSquare</response>
    [HttpPut("{roomId:int}/update-grid")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<GridSquareResponse>> UpdateSquare(
        [FromBody] UpdateGridRequest updateGrid,
        [FromRoute] int roomId
    )
    {
        var room = await _roomService.GetRoomByIdAsync(roomId);
        if (room is null)
        {
            return NotFound();
        }

        var grid = await _roomService.GetGridSquareByCoordinatesAsync(roomId, updateGrid.Grid.X, updateGrid.Grid.Y);

        if (grid == null)
        {
            await _roomService.AddGridSquareAsync(_mapper.Map<GridSquare>(updateGrid));
        }
        else
        {
            grid.Type = updateGrid.Type;
            grid.Rotation = updateGrid.Rotation;
            await _roomService.UpdateGridSquareAsync(grid);
        }

        var updatedGridSquare = (await _roomService.GetGridSquareByCoordinatesAsync(roomId, updateGrid.Grid.X, updateGrid.Grid.Y))!;

        return Ok(_mapper.Map<GridSquareResponse>(updatedGridSquare));
    }

    [HttpDelete("{roomId:int}/grid")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<GridSquareResponse>> DeleteSquare(
        [FromBody] GridCoordinate grid,
        [FromRoute] int roomId
    )
    {
        var room = await _roomService.GetRoomByIdAsync(roomId);
        if (room is null)
        {
            return NotFound();
        }

        var gridSquare = await _roomService.GetGridSquareByCoordinatesAsync(roomId, grid.X, grid.Y);
        if (gridSquare != null)
        {
            await _roomService.DeleteGridSquareAsync(gridSquare);
        }

        return NoContent();
    }

    [HttpPut("{roomId:int}/place-user")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<GridSquareResponse>> PlaceUser(
        [FromBody] PlaceUserRequest placeUser,
        [FromRoute] int roomId
    )
    {
        // TODO implement this comment, is Authorize(Roles ...) already enough??
        // if (placeUser.UserId != int.Parse(User.Identity.Name) && !User.IsInRole(Role.Manager.ToString()))
        // {
        //     return Forbid();
        // }

        var room = await _roomService.GetRoomByIdAsync(roomId);
        if (room is null)
        {
            return NotFound();
        }

        var grid = await _roomService.GetGridSquareByCoordinatesAsync(roomId, placeUser.Grid.X, placeUser.Grid.Y);
        if (grid is null)
        {
            return NotFound();
        }

        grid.UserId = placeUser.UserId;
        await _roomService.PlaceUserAsync(grid);

        return Ok(_mapper.Map<GridSquareResponse>(grid));
    }

    [HttpPost("{roomId:int}/copy")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<RoomResponse>> CopyRoom(
        [FromBody] CopyRoomMessage copyRoomMessage,
        [FromRoute] int roomId
    )
    {
        var room = await _roomService.GetRoomByIdAsync(roomId);
        if (room is null)
        {
            return NotFound();
        }

        var newRoom = await _roomService.CopyRoomAsync(copyRoomMessage.Name, room, copyRoomMessage.TargetEventId);

        return Ok(_mapper.Map<RoomResponse>(newRoom));
    }
}
