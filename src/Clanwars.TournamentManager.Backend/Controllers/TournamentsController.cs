using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Backend.Models;
using Clanwars.TournamentManager.Backend.Models.BackendValidations;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class TournamentsController : ControllerBase
{
    private readonly ITournamentService _tournamentService;
    private readonly ITournamentMatchService _tournamentMatchService;
    private readonly IMapper _mapper;
    private readonly IUserService _userService;

    public TournamentsController(
        ITournamentService tournamentService,
        ITournamentMatchService tournamentMatchService,
        IMapper mapper, IUserService userService)
    {
        _tournamentService = tournamentService;
        _tournamentMatchService = tournamentMatchService;
        _mapper = mapper;
        _userService = userService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<TournamentResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(Tournament))]
    public async Task<ActionResult<PagedResult<TournamentResponse>>> GetTournaments(
        [FromQuery] SieveModel sieveModel
    )
    {
        var pagedResult = await _tournamentService.GetTournamentsAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<TournamentResponse>>(pagedResult));
    }

    [HttpPost]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<TournamentResponse>> CreateTournament(
        [FromBody] CreateTournamentMessage createTournamentMessage
    )
    {
        var tournament = _mapper.Map<Tournament>(createTournamentMessage);

        await _tournamentService.AddTournamentAsync(tournament);

        return CreatedAtAction(nameof(GetTournaments), new {filters = $"id=={tournament.Id}"}, _mapper.Map<TournamentResponse>(tournament));
    }

    [HttpPut("{tournamentId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<TournamentResponse>> UpdateTournament(
        [FromBody] UpdateTournamentRequest updateTournament
    )
    {
        var tournament = await _tournamentService.GetTournamentByIdAsync(updateTournament.TournamentId);

        if (tournament is null)
        {
            return NotFound();
        }

        var updatedTournament = _mapper.Map(updateTournament, tournament);

        // Todo: check how we update images!

        await _tournamentService.UpdateTournamentAsync(updatedTournament);
        return Ok(_mapper.Map<TournamentResponse>(updatedTournament));
    }

    [HttpPut("{tournamentId:int}/start")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<TournamentResponse>> StartTournament(
        [FromRoute] [TournamentIsReadyToStart] int tournamentId
    )
    {
        var tournament = await _tournamentService.GetTournamentByIdAsync(tournamentId);

        if (tournament is null)
        {
            return NotFound();
        }

        await _tournamentService.StartTournamentAsync(tournament);

        return Ok(_mapper.Map<TournamentResponse>(tournament));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="tournamentMatchId">The ID of the match to be scored</param>
    /// <param name="scoreMatch"></param>
    /// <returns></returns>
    /// <response code="403">Could be, that a user who is not tournament-moderator wants to score a "Points Tournament"</response>
    [HttpPut("score-match/{tournamentMatchId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<TournamentMatchResponse>> ScoreMatch(
        [FromRoute] int tournamentMatchId,
        [FromBody] ScoreMatchRequest scoreMatch
    )
    {
        var tournamentMatch = await _tournamentMatchService.GetTournamentMatchByIdAsync(tournamentMatchId);

        if (tournamentMatch is null)
        {
            return NotFound();
        }

        _mapper.Map(scoreMatch, tournamentMatch);

        var tournament = await _tournamentService.GetTournamentByIdAsync(tournamentMatch.TournamentRound.TournamentId);

        // check if the scoring user is in the losers team (or the user is tournament-moderator...)
        var user = await _userService.GetByClaimsPrincipalAsync(User);
        var losersTeamScore = scoreMatch.Scores.MinBy(s => s.Score);
        if (!User.IsInRole("tournament-moderator") && tournament!.Teams.SingleOrDefault(t => t.Id == losersTeamScore!.TournamentTeamId)!.Members.All(tm => tm.MemberId != user.Id))
        {
            return Forbid();
        }

        // Todo: check if all teams gets scored (or something like this...)

        // Point Tournaments can only be scored by Tournament-Moderators
        if (tournamentMatch.TournamentRound.Tournament.Mode == TournamentMode.Points && !User.IsInRole("tournament-moderator"))
        {
            return Forbid();
        }

        await _tournamentMatchService.ScoreMatchAsync(tournamentMatch);

        if (tournament!.Rounds.All(r => r.Matches.All(m => m.IsFinishedMatch())))
        {
            // set finished
            tournament.State = TournamentState.Finished;
        }

        tournament.UpdatedAt = DateTime.Now;
        await _tournamentService.UpdateTournamentAsync(tournament);

        return Ok(_mapper.Map<TournamentMatchResponse>(tournamentMatch));
    }

    [HttpDelete("{tournamentId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteTournament(
        [FromRoute] int tournamentId
    )
    {
        var tournament = await _tournamentService.GetTournamentByIdAsync(tournamentId);

        if (tournament is null)
        {
            return NotFound();
        }

        await _tournamentService.DeleteTournamentAsync(tournament);
        return NoContent();
    }
}
