using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib.Messages.Admin;
using Clanwars.TournamentManager.Lib.Messages.User;
using Clanwars.TournamentManager.Lib.Messages.UserStore;
using Clanwars.TournamentManager.UserStores;
using Clanwars.TournamentManager.UserStores.Interfaces;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class UserStoreController(IExternalUserStore userStore, IUserService userService, IMapper mapper) : ControllerBase
{
    [HttpGet]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(IEnumerable<UserStoreResponse>), StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<UserStoreResponse>>> FindExternalUsers(
        [FromQuery] string search
    )
    {
        var users = await userStore.GetUsersAsync(search);

        return Ok(users);
    }

    [HttpPost]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(UserStoreResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<UserStoreResponse>> CreateExternalUser(
        [FromBody] CreateUserStoreUserMessage request
    )
    {
        try
        {
            var newUser = await userStore.CreateUserAsync(request.FirstName, request.LastName, request.Email, request.Email);
            return Ok(newUser);
        }
        catch (UserAlreadyExistsException)
        {
            return Conflict();
        }
    }
    
    [HttpPut("{userId:int}/reset-password")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ResetPassword(int userId)
    {
        var user = await userService.GetByIdAsync(userId);

        if (user is null)
        {
            return NotFound();
        }

        var idpUser = (await userStore.GetUsersAsync(user.Email)).SingleOrDefault();

        if (idpUser is null)
        {
            return Conflict();
        }
        
        await userStore.SetUserPasswordAsync(idpUser.Id, user.Email, true);

        return NoContent();
    }
    
    [HttpPost("reimport-users-to-idp")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<UserImportResponse>> ImportUser()
    {
        var users = await userService.GetAllUsersAsync();

        var importedUsers = new List<User>();
        var skippedUsers = new List<User>();

        foreach (var user in users.Where(u => u.FirstName is not null && u.LastName is not null))
        {
            try
            {
                var randomPw = Guid.NewGuid().ToString("N");
                await userStore.CreateUserAsync(user.FirstName!, user.LastName!, user.Email, randomPw);
                
                importedUsers.Add(user);
            }
            catch (UserAlreadyExistsException)
            {
                skippedUsers.Add(user);
            }
        }
        
        return Ok(new UserImportResponse
        {
            ImportedUsers = mapper.Map<List<UserResponse>>(importedUsers),
            SkippedUsers = mapper.Map<List<UserResponse>>(skippedUsers),
        });
    }
}
