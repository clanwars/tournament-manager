using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Models;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Shouts;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class ShoutsController : ControllerBase
{
    private readonly IShoutService _shoutService;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public ShoutsController(
        IShoutService shoutService,
        IUserService userService,
        IMapper mapper
    )
    {
        _shoutService = shoutService;
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<ShoutResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(News))]
    public async Task<ActionResult<IEnumerable<ShoutResponse>>> GetShouts([FromQuery] SieveModel sieveModel)
    {
        var pagedResult = await _shoutService.GetShoutsAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<ShoutResponse>>(pagedResult));
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<ShoutResponse>> CreateShout(
        [FromBody] CreateShoutMessage createShoutMessage
    )
    {
        var newShout = _mapper.Map<Shout>(createShoutMessage);
        newShout.User = await _userService.GetByClaimsPrincipalAsync(User);

        await _shoutService.AddShoutAsync(newShout);

        return CreatedAtAction(nameof(GetShouts), new {filters = $"id=={newShout.Id}"}, _mapper.Map<ShoutResponse>(newShout));
    }

    [HttpPut("{shoutId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<ShoutResponse>> UpdateShout(
        [FromBody] UpdateShoutRequest updateRequest,
        [FromRoute] int shoutId
    )
    {
        var shout = await _shoutService.GetShoutByIdAsync(shoutId);

        if (shout is null)
        {
            return NotFound();
        }
        
        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);
        if (shout.UserId != currentUser.Id && !User.IsInRole("tournament-moderator"))
        {
            return Forbid();
        }

        var updatedShout = _mapper.Map(updateRequest, shout);
        await _shoutService.UpdateShoutAsync(shout);

        return Ok(_mapper.Map<ShoutResponse>(updatedShout));
    }

    [HttpDelete("{shoutId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> DeleteShout(
        [FromRoute] int shoutId
    )
    {
        var shout = await _shoutService.GetShoutByIdAsync(shoutId);

        if (shout is null)
        {
            return NotFound();
        }

        await _shoutService.DeleteShoutAsync(shout);
        return NoContent();
    }
}
