using Clanwars.PaymentSystem.Lib;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;
using Clanwars.TournamentManager.Backend.Helpers.ClaimsPrincipal;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Backend.Services.PaymentSystem;
using Clanwars.TournamentManager.Lib.Messages.UserStore;
using Flurl.Util;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class PaymentSystemController : ControllerBase
{
    private readonly IPaymentSystemService? _paymentSystemService;

    public PaymentSystemController(
        IPaymentSystemService? paymentSystemService = null
    )
    {
        _paymentSystemService = paymentSystemService;
    }

    [HttpGet("accounts")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(IEnumerable<UserStoreResponse>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<IEnumerable<UserStoreResponse>>> FindExistingAccount(
        [FromQuery] string? email,
        [FromQuery] Guid? accountId
    )
    {
        if (_paymentSystemService is null)
        {
            return NotFound("Payment System module is not enabled");
        }

        if (email is not null && accountId is not null)
        {
            return Conflict("you cannot ask for email and accountId at the same time");
        }

        if (email is not null)
        {
            var account = await _paymentSystemService.GetAccountByEmailAsync(email);
            if (account is null)
            {
                return NotFound();
            }

            return Ok(account);
        }

        if (accountId is not null)
        {
            var account = await _paymentSystemService.GetAccountAsync(accountId.Value);
            if (account is null)
            {
                return NotFound();
            }

            return Ok(account);
        }

        return NotFound();
    }

    [HttpGet("accounts/me")]
    [ProducesResponseType(typeof(UserStoreResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<UserStoreResponse>> MyAccount()
    {
        if (_paymentSystemService is null)
        {
            return NotFound("Payment System module is not enabled");
        }

        var account = await _paymentSystemService.GetAccountByEmailAsync(User.GetEmail());

        return Ok(account);
    }

    [HttpPost("accounts")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(UserStoreResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<UserStoreResponse>> CreateAccount(
        [FromBody] CreateAccountMessage request
    )
    {
        if (_paymentSystemService is null)
        {
            return NotFound("Payment System module is not enabled");
        }

        var newAccount = await _paymentSystemService.CreateAccountAsync(request);
        return Ok(newAccount);
    }

    [HttpPut("accounts/{accountId:guid}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(UserStoreResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<UserStoreResponse>> UpdateAccount(
        [FromRoute] Guid accountId,
        [FromBody] UpdateAccountMessage request
    )
    {
        if (_paymentSystemService is null)
        {
            return NotFound("Payment System module is not enabled");
        }

        var newAccount = await _paymentSystemService.UpdateAccountAsync(accountId, request);
        return Ok(newAccount);
    }

    [HttpGet("points-of-sale/current-cards")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(IList<PointOfSaleInfoWithCurrentCard>), StatusCodes.Status200OK)]
    public async Task<ActionResult<IList<PointOfSaleInfoWithCurrentCard>>> GetCurrentCards()
    {
        if (_paymentSystemService is null)
        {
            return NotFound("Payment System module is not enabled");
        }

        var results = await _paymentSystemService.GetCurrentCardsAsync();
        return Ok(results);
    }

    [HttpGet("current-transactions/me")]
    [ProducesResponseType(typeof(IEnumerable<TransactionResponse>), StatusCodes.Status200OK)]
    public async Task<ActionResult<PagedResult<TransactionResponse>>> GetMyTransactions([FromQuery] SieveModel sieveModel)
    {
        if (_paymentSystemService is null)
        {
            return NotFound("Payment System module is not enabled");
        }

        var currentDate = DateTime.Now.AddDays(-7);
        sieveModel.Filters = $"account.email=={User.GetEmail()},timestamp>={currentDate.ToInvariantString()}";

        var results = await _paymentSystemService.GetTransactionsAsync(sieveModel);
        return Ok(results);
    }
}
