using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Backend.Services.PaymentSystem.OidcTokenAuthenticationClient;

namespace Clanwars.TournamentManager.Backend.Options;

public class PaymentSystemOptions
{
    public const string Option = "PaymentSystem";

    [Required]
    public bool Enabled { get; set; }

    public Uri? Api { get; set; }

    public OidcOptions? OidcOptions { get; set; }
}
