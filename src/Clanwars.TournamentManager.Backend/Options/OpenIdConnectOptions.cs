using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Backend.Options;

public class OpenIdConnectOptions
{
    public const string Option = "OpenIdConnect";

    [Required]
    public string Authority { get; set; } = null!;

    [Required]
    public string SignedOutRedirectUri { get; set; } = null!;

    [Required]
    public string ClientId { get; set; } = null!;

    [Required]
    public string ClientSecret { get; set; } = null!;

    [Required]
    public string ResponseType { get; set; } = null!;

    [Required]
    public bool SaveTokens { get; set; }

    [Required]
    public bool RequireHttpsMetadata { get; set; }
}
