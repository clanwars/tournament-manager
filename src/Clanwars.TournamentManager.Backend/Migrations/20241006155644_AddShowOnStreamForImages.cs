﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clanwars.TournamentManager.Backend.Migrations
{
    /// <inheritdoc />
    public partial class AddShowOnStreamForImages : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ShowOnStream",
                table: "Images",
                type: "tinyint(1)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShowOnStream",
                table: "Images");
        }
    }
}
