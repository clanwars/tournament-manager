using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Clanwars.TournamentManager.Backend.Filters;

public class CheckPasswordChangeRequiredFilter : IAuthorizationFilter, IOrderedFilter
{
    public int Order => 200;

    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var user = context.HttpContext.User;
        if (user.HasClaim("PasswordChangeRequired", "true")
            && !context.RouteData.Values.Any(kvp => kvp is { Key: "action", Value: "UpdateUserPassword" })
           )
        {
            context.Result = new StatusCodeResult(StatusCodes.Status423Locked);
        }
    }
}
