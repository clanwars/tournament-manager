using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Entities.Rooms;
using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.TournamentManager.Backend.Context;

public class TournamentManagerDbContext(DbContextOptions<TournamentManagerDbContext> options) : DbContext(options)
{
    public DbSet<User> Users { get; init; }
    public DbSet<ClanUser> UserClans { get; init; }
    public DbSet<Clan> Clans { get; init; }
    public DbSet<Event> Events { get; init; }
    public DbSet<EventUser> EventUsers { get; init; }
    public DbSet<Tournament> Tournaments { get; init; }
    public DbSet<TournamentRound> TournamentRounds { get; init; }
    public DbSet<TournamentMatch> TournamentMatches { get; init; }
    public DbSet<TournamentMatchScore> TournamentMatchScores { get; init; }
    public DbSet<TournamentTeam> TournamentTeams { get; init; }
    public DbSet<TournamentTeamMember> TournamentTeamMembers { get; init; }
    public DbSet<Room> Rooms { get; init; }
    public DbSet<GridSquare> GridSquares { get; init; }
    public DbSet<Image> Images { get; init; }
    public DbSet<ImageWithContent> ImagesWithContent { get; init; }
    public DbSet<News> News { get; init; }
    public DbSet<ChatMessage> ChatMessages { get; init; }
    public DbSet<Shout> Shouts { get; init; }

    public DbSet<Feedback> Feedbacks { get; init; }

    public DbSet<PipelineModuleState> ModuleStates { get; init; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TournamentTeamMember>()
            .HasIndex(m => new { m.MemberId, m.TournamentTeamId })
            .IsUnique();

        modelBuilder.Entity<TournamentTeam>()
            .HasIndex(tt => new { tt.Name, tt.TournamentId })
            .IsUnique();

        modelBuilder.Entity<EventUser>()
            .HasIndex(e => new { e.UserId, e.EventId })
            .IsUnique();

        modelBuilder.Entity<ClanUser>()
            .HasIndex(c => c.UserId)
            .IsUnique();

        modelBuilder.Entity<Event>()
            .HasIndex(e => e.Name)
            .IsUnique();

        modelBuilder.Entity<User>()
            .HasIndex(e => e.Email)
            .IsUnique();

        modelBuilder.Entity<User>()
            .HasIndex(e => e.Nickname)
            .IsUnique();

        modelBuilder.Entity<Clan>()
            .HasIndex(e => e.Name)
            .IsUnique();

        modelBuilder.Entity<Image>(ib =>
        {
            ib.ToTable("Images");
            ib.Property(i => i.Hash).HasColumnName("Hash");
            ib.Property(i => i.Type).HasColumnName("Type");
            ib.HasIndex(img => new { img.Type, img.Hash }).IsUnique();
            ib.HasOne(i => i.ImageWithContent).WithOne().HasForeignKey<ImageWithContent>(i => i.Id);
        });

        modelBuilder.Entity<ImageWithContent>(icb =>
        {
            icb.ToTable("Images");
            icb.Property(i => i.Hash).HasColumnName("Hash");
            icb.Property(i => i.Type).HasColumnName("Type");
        });

        modelBuilder.Entity<GridSquare>()
            .HasAlternateKey(gs => new { gs.RoomId, gs.X, gs.Y });
    }
}
