using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Clanwars.TournamentManager.Backend;

public class AuthorizeCheckOperationFilter : IOperationFilter
{
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        var hasAnonymousAccess = context
            .MethodInfo
            .GetCustomAttributes(true)
            .OfType<AllowAnonymousAttribute>()
            .Any();

        if (hasAnonymousAccess)
        {
            operation.Security = null;
        }
        else
        {
            var requiredRoles = context.MethodInfo
                .GetCustomAttributes(true)
                .OfType<AuthorizeAttribute>()
                .Select(attr => attr.Roles)
                .Distinct().ToList();

            operation.Responses.TryAdd("401", new OpenApiResponse {Description = "Unauthorized"});
            operation.Responses.TryAdd("403", new OpenApiResponse {Description = "Forbidden"});

            if (requiredRoles.Count != 0)
            {
                operation.Summary += "[Authentication: ";
                operation.Summary += requiredRoles.Aggregate("", (current, next) => current + "" + next);
                operation.Summary += "] ";
            }

            operation.Security.Add(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "JwtToken"}
                    },
                    new[] {"openid", "profile", "email", "offline_access"}
                }
            });
        }
    }
}
