using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.User;
using HashidsNet;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class UserMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PagedResult<User>, PagedResult<UserResponse>>();

        config.NewConfig<User, UserResponse>()
            .Map(dst => dst.ImageUrl, src => src.Image == null ? null : $"api/v1/images/{MapContext.Current.GetService<IHashids>().Encode(src.ImageId!.Value)}")
            .Map(dst => dst.Events, src => src.Events.Select(eu => eu.Event))
            .MaxDepth(4);

        config.NewConfig<UpdateUserMessage, User>()
            .Ignore(dst => dst.CreatedAt)
            .Map(dst => dst.UpdatedAt, src => DateTime.Now)
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.Email)
            .Ignore(dst => dst.ImageId!)
            .Ignore(dst => dst.Image!)
            .Ignore(dst => dst.Clan!)
            .Ignore(dst => dst.ClanId!)
            .Ignore(dst => dst.Events)
            ;

        config.NewConfig<ClanUser, UserPermissionResponse>();
    }
}
