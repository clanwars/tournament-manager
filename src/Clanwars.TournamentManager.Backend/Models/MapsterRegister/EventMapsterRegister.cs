using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Events;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class EventMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PagedResult<Event>, PagedResult<EventResponse>>();
        config.NewConfig<PagedResult<EventUser>, PagedResult<RegistrationResponse>>();

        config.NewConfig<Event, EventResponse>()
            .Map(dst => dst.ParticipantsCount, src => src.Participants.Count)
            .Map(dst => dst.TournamentsCount, src => src.Tournaments.Count)
            .MaxDepth(5);

        config.NewConfig<CreateEventMessage, Event>()
            .Ignore(ev => ev.Participants)
            .Ignore(ev => ev.Tournaments)
            .Ignore(ev => ev.Id);

        config.NewConfig<UpdateEventRequest, Event>()
            .Map(dst => dst.Id, src => src.EventId)
            .Ignore(ev => ev.Participants)
            .Ignore(ev => ev.Tournaments);

        config.NewConfig<EventUser, RegistrationResponse>()
            .MaxDepth(5);
    }
}
