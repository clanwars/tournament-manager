using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using HashidsNet;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class TournamentMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PagedResult<Tournament>, PagedResult<TournamentResponse>>();

        config.NewConfig<Tournament, TournamentResponse>()
            .Map(dst => dst.ImageUrl, src => src.Image == null ? null : $"api/v1/images/{MapContext.Current.GetService<IHashids>().Encode(src.ImageId!.Value)}")
            .MaxDepth(7);

        config.NewConfig<CreateTournamentMessage, Tournament>()
            .Map(dst => dst.State, src => TournamentState.Planning)
            .Map(dst => dst.CreatedAt, src => DateTime.Now)
            .Ignore(t => t.UpdatedAt!)
            .Ignore(t => t.Id)
            .Ignore(t => t.ImageId!)
            .Ignore(t => t.Image!)
            .Ignore(t => t.Teams)
            .Ignore(t => t.Rounds);

        config.NewConfig<UpdateTournamentRequest, Tournament>()
            .Map(dst => dst.Id, src => src.TournamentId)
            .Map(dst => dst.UpdatedAt, src => DateTime.Now)
            .Ignore(t => t.CreatedAt)
            .Ignore(t => t.ImageId!)
            .Ignore(t => t.Image!)
            .Ignore(t => t.EventId)
            .Ignore(t => t.Teams)
            .Ignore(t => t.Rounds)
            .IgnoreIf((request, _) => request.PlayerPerTeam == null, dst => dst.PlayerPerTeam!)
            .IgnoreIf((request, _) => request.Mode == null, dst => dst.Mode)
            .IgnoreIf((request, _) => request.State == null, dst => dst.State);

        config.NewConfig<ScoreMatchRequest, TournamentMatch>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.TournamentRound)
            .Ignore(dst => dst.TournamentRoundId)
            .Ignore(dst => dst.MatchIndex);

        config.NewConfig<SetTeamScore, TournamentMatchScore>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.TournamentMatch)
            .Ignore(dst => dst.TournamentMatchId)
            .Ignore(dst => dst.PreviousMatchIndex!)
            .Ignore(dst => dst.PreviousMatchRank!)
            .Ignore(dst => dst.TournamentTeam);

        config.NewConfig<TournamentRound, TournamentRoundResponse>()
            .MaxDepth(2);

        config.NewConfig<TournamentMatch, TournamentMatchResponse>()
            .MaxDepth(2);

        config.NewConfig<TournamentMatchScore, TournamentMatchScoreResponse>()
            .MaxDepth(2);
    }
}
