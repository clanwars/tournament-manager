using Clanwars.TournamentManager.Backend.Entities.Rooms;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums.Rooms;
using Clanwars.TournamentManager.Lib.Messages.Rooms;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class RoomsMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PagedResult<Room>, PagedResult<RoomResponse>>();

        config.NewConfig<Room, RoomResponse>()
            .Map(dst => dst.CountSeats, src => src.GridSquares.Count(
                gs => gs.Type == GridSquareType.SeatDefault ||
                      gs.Type == GridSquareType.SeatTechSupport ||
                      gs.Type == GridSquareType.SeatTournamentSupport ||
                      gs.Type == GridSquareType.SeatCashier)
            );

        config.NewConfig<GridSquare, GridSquareResponse>();

        config.NewConfig<CreateRoomMessage, Room>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.Event)
            .Ignore(dst => dst.GridSquares);

        config.NewConfig<UpdateRoomRequest, Room>()
            .Map(dst => dst.Id, src => src.RoomId)
            .Ignore(dst => dst.Event)
            .Ignore(dst => dst.EventId)
            .Ignore(dst => dst.GridSquares);
        
        config.NewConfig<UpdateGridRequest, GridSquare>()
            .Map(dst => dst.X, src => src.Grid.X)
            .Map(dst => dst.Y, src => src.Grid.Y)
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.Room)
            .Ignore(dst => dst.User!)
            .Ignore(dst => dst.UserId!)
            ;
    }
}
