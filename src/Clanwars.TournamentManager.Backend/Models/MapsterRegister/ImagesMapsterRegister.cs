using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Images;
using HashidsNet;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class ImagesMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PagedResult<Image>, PagedResult<ImageResponse>>();

        config.NewConfig<Image, ImageResponse>()
            .Map(dest => dest.HashId, src => MapContext.Current.GetService<IHashids>().Encode(src.Id));
    }
}
