using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib.Messages.Pipeline;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class PipelineMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<IList<PipelineModuleState>, IList<PipelineModuleStateResponse>>();
        config.NewConfig<PipelineModuleState, PipelineModuleStateResponse>();
    }
}
