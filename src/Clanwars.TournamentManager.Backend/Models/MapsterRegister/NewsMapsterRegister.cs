using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.News;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class NewsMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PagedResult<News>, PagedResult<NewsResponse>>();

        config.NewConfig<News, NewsResponse>()
            .Map(dst => dst.AuthorName, src => src.Author.Nickname);

        config.NewConfig<CreateNewsMessage, News>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.UpdatedAt!)
            .Ignore(dst => dst.Author)
            .Map(dst => dst.CreatedAt, src => DateTime.Now);
        
        config.NewConfig<UpdateNewsRequest, News>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.CreatedAt)
            .Ignore(dst => dst.Author)
            .Map(dst => dst.UpdatedAt, src => DateTime.Now);
    }
}
