using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Feedback;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class FeedbackMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<CreateFeedbackMessage, Feedback>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.User)
            .Ignore(dst => dst.UserId)
            .Ignore(dst => dst.CreatedAt)
            .Ignore(dst => dst.Resolved);
        
        config.NewConfig<PagedResult<Feedback>, PagedResult<FeedbackResponse>>();

        config.NewConfig<Feedback, FeedbackResponse>()
            .Map(dst => dst.Username, src => src.User.Nickname ?? $"{src.User.FirstName} {src.User.LastName}");
    }
}
