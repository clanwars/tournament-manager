using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;

namespace Clanwars.TournamentManager.Backend.Models;

public record UpdateTournamentRequest : UpdateTournamentMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public int TournamentId { get; set; }
}
