using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Lib.Messages.News;

namespace Clanwars.TournamentManager.Backend.Models;

public record UpdateNewsRequest : UpdateNewsMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public int NewsId { get; set; }
}
