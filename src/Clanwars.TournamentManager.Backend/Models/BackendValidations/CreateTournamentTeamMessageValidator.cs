using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class CreateTournamentTeamMessageValidator : AbstractValidator<CreateTournamentTeamMessage>
{
    private readonly ITournamentTeamRepository _tournamentTeamRepository;
    private readonly ITournamentRepository _tournamentRepository;

    public CreateTournamentTeamMessageValidator(
        ITournamentTeamRepository tournamentTeamRepository,
        ITournamentRepository tournamentRepository
    )
    {
        _tournamentTeamRepository = tournamentTeamRepository;
        _tournamentRepository = tournamentRepository;

        RuleFor(x => x.Name)
            .Must(HaveUniqueTournamentTeamName())
            .WithMessage("tournament team name is already in use");

        RuleFor(x => x.Name)
            .Matches("^(?!(" + TournamentTeamResponseExtensions.ByeTeamPrefix + ")).+")
            .WithMessage("tournament team name should not start with " + TournamentTeamResponseExtensions.ByeTeamPrefix + ".");

        RuleFor(x => x.TournamentId)
            .Must(BeValidTournamentId())
            .WithMessage("tournamentId is not valid");

        RuleFor(x => x.TournamentId)
            .Must(BeOpenForRegistration())
            .WithMessage("tournament is not open for registration");
    }

    private Func<CreateTournamentTeamMessage, string, bool> HaveUniqueTournamentTeamName()
    {
        return (createTournamentTeamMessage, teamName) =>

        {
            var result = _tournamentTeamRepository
                .GetTournamentTeams()
                .Where(tt => tt.TournamentId == createTournamentTeamMessage.TournamentId)
                .SingleOrDefault(tt => tt.Name == teamName);

            return result is null;
        };
    }

    private Func<int, bool> BeValidTournamentId()
    {
        return tournamentId =>
        {
            var tournament = _tournamentRepository
                .GetTournaments()
                .SingleOrDefault(t => t.Id == tournamentId);

            return tournament is not null;
        };
    }

    private Func<int, bool> BeOpenForRegistration()
    {
        return tournamentId =>
        {
            var tournament = _tournamentRepository
                .GetTournaments()
                .SingleOrDefault(t => t.Id == tournamentId);
            return tournament is null || tournament.State == TournamentState.OpenForRegistration;
        };
    }
}
