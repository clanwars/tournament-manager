using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class ScoreMatchRequestValidator : AbstractValidator<ScoreMatchRequest>
{
    private readonly ITournamentMatchRepository _tournamentMatchRepository;

    public ScoreMatchRequestValidator(
        ITournamentMatchRepository tournamentMatchRepository
    )
    {
        _tournamentMatchRepository = tournamentMatchRepository;

        RuleFor(x => x.TournamentMatchId)
            .Must(MatchIsReadyToBeScored())
            .WithMessage("Match is not ready to be scored yet");

        RuleFor(x => x.Scores)
            .Must(ScoresMustDiffer())
            .WithMessage("Scores do not differ so it is not possible to determine a winner and a loser.");
    }

    private Func<int, bool> MatchIsReadyToBeScored()
    {
        return tournamentMatchId =>
        {
            var tournamentMatch = _tournamentMatchRepository.GetTournamentMatches()
                .SingleOrDefault(tm => tm.Id == tournamentMatchId);
            if (tournamentMatch is null)
            {
                return true;
            }

            return !tournamentMatch.Scores.Any(s => s.TournamentTeamId is null || s.Score is not null);
        };
    }

    private static Func<IList<SetTeamScore>, bool> ScoresMustDiffer()
    {
        return scores => { return scores.Min(ts => ts.Score) != scores.Max(ts => ts.Score); };
    }
}
