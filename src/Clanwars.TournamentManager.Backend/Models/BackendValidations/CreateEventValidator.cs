using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Messages.Events;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class CreateEventValidator : AbstractValidator<CreateEventMessage>
{
    private readonly IEventRepository _eventRepository;

    public CreateEventValidator(
        IEventRepository eventRepository
    )
    {
        _eventRepository = eventRepository;

        RuleFor(x => x.Name)
            .Must(NotSameNameLikeOtherEvents())
            .WithMessage("The same event name cannot be used twice");
    }

    private Func<string, bool> NotSameNameLikeOtherEvents()
    {
        return eventName =>
        {
            var elementWithSameName = _eventRepository
                .GetEvents()
                .FirstOrDefault(ev => ev.Name == eventName);

            return elementWithSameName is null;
        };
    }
}
