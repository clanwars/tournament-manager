using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class UpdateTournamentValidator : AbstractValidator<UpdateTournamentRequest>
{
    private readonly ITournamentRepository _tournamentRepository;

    private readonly List<KeyValuePair<TournamentState, TournamentState>> _possibleStateChanges =
    [
        new KeyValuePair<TournamentState, TournamentState>(TournamentState.Planning, TournamentState.OpenForRegistration),
        new KeyValuePair<TournamentState, TournamentState>(TournamentState.OpenForRegistration, TournamentState.Planning),
        new KeyValuePair<TournamentState, TournamentState>(TournamentState.Planning, TournamentState.Closed),
        new KeyValuePair<TournamentState, TournamentState>(TournamentState.OpenForRegistration, TournamentState.Closed),
        new KeyValuePair<TournamentState, TournamentState>(TournamentState.Running, TournamentState.Closed)
    ];

    public UpdateTournamentValidator(
        ITournamentRepository tournamentRepository
    )
    {
        _tournamentRepository = tournamentRepository;

        RuleFor(x => x.Mode)
            .Must(BeChangeable())
            .WithMessage("The tournament mode cannot be changed at this time");
        RuleFor(x => x.State)
            .Must(BeAllowedTournamentState())
            .WithMessage(utr => $"The tournament mode cannot be changed from the current state to the state '{utr.State}' manually");
    }

    private Func<UpdateTournamentRequest, TournamentState?, bool> BeAllowedTournamentState()
    {
        return (cem, state) =>
        {
            if (state == null)
                return true;

            var tm = _tournamentRepository.GetTournamentByIdAsync(cem.TournamentId).Result;

            return tm is null || _possibleStateChanges.Exists(kvp => kvp.Key == tm.State && kvp.Value == state);
        };
    }

    private Func<UpdateTournamentRequest, TournamentMode?, bool> BeChangeable()
    {
        return (cem, mode) =>
        {
            if (mode == null)
            {
                return true;
            }

            var tm = _tournamentRepository
                .GetTournaments()
                .SingleOrDefault(t => t.Id == cem.TournamentId);

            return tm is null || tm.State is TournamentState.Planning or TournamentState.OpenForRegistration;
        };
    }
}
