using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Messages.Clans;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class CreateClanValidator : AbstractValidator<CreateClanMessage>
{
    private readonly IClanRepository _clanRepository;

    public CreateClanValidator(
        IClanRepository clanRepository
    )
    {
        _clanRepository = clanRepository;

        RuleFor(x => x.Name)
            .Must(NotSameNameLikeOtherClans())
            .WithMessage("The same clan name cannot be used twice");
        RuleFor(x => x.ClanTag)
            .Must(NotSameClanTagLikeOtherClans())
            .WithMessage("The same clan tag cannot be used twice");
    }

    private Func<string, bool> NotSameNameLikeOtherClans()
    {
        return name =>
        {
            var elementWithSameName = _clanRepository
                .GetClans()
                .FirstOrDefault(clan => clan.Name == name);
            return elementWithSameName is null;
        };
    }

    private Func<string, bool> NotSameClanTagLikeOtherClans()
    {
        return name =>
        {
            var elementWithSameName = _clanRepository
                .GetClans()
                .FirstOrDefault(clan => clan.ClanTag == name);
            return elementWithSameName is null;
        };
    }
}
