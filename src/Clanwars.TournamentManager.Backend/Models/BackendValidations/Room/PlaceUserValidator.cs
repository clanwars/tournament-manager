using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Enums.Rooms;
using Clanwars.TournamentManager.Lib.Messages.Rooms;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations.Room;

public class PlaceUserValidator : AbstractValidator<PlaceUserRequest>
{
    private readonly IRoomRepository _roomRepository;
    private readonly IUserRepository _userRepository;

    private readonly List<GridSquareType> _placeableGridSquares =
    [
        GridSquareType.SeatDefault,
        GridSquareType.SeatTechSupport,
        GridSquareType.SeatTournamentSupport,
        GridSquareType.SeatCashier
    ];

    public PlaceUserValidator(
        IRoomRepository roomRepository,
        IUserRepository userRepository
    )
    {
        _roomRepository = roomRepository;
        _userRepository = userRepository;

        RuleFor(x => x.RoomId)
            .Must(BeValidRoomId())
            .WithMessage("Room does not exist.");

        RuleFor(x => x.Grid.X)
            .Must(BeInsideRoomCoordinateX())
            .WithMessage("The X coordinate is not inside the Room");

        RuleFor(x => x.Grid.Y)
            .Must(BeInsideRoomCoordinateY())
            .WithMessage("The Y coordinate is not inside the Room");

        RuleFor(x => x.Grid)
            .Must(BeSeat())
            .WithMessage("The grid square type must be a seat");

        RuleFor(x => x.UserId)
            .Must(BeValidUserId())
            .WithMessage("Provided user id does not exist.");
    }

    private Func<PlaceUserRequest, GridCoordinate, bool> BeSeat()
    {
        return (placeUserRequest, gridCoordinate) =>
        {
            var room = _roomRepository.GetRoomByIdAsync(placeUserRequest.RoomId).Result;

            if (room is null)
            {
                return true;
            }

            var targetGridSquare = room.GridSquares
                .Where(gs => gs.X == gridCoordinate.X)
                .SingleOrDefault(gs => gs.Y == gridCoordinate.Y);

            if (targetGridSquare is null || _placeableGridSquares.TrueForAll(t => t != targetGridSquare.Type))
            {
                return false;
            }

            return true;
        };
    }

    private Func<PlaceUserRequest, int, bool> BeInsideRoomCoordinateX()
    {
        return (placeUserRequest, coordinateX) =>
        {
            var room = _roomRepository
                .GetRooms()
                .SingleOrDefault(r => r.Id == placeUserRequest.RoomId);

            return room is null || coordinateX <= room.SizeX;
        };
    }

    private Func<PlaceUserRequest, int, bool> BeInsideRoomCoordinateY()
    {
        return (placeUserRequest, coordinateY) =>
        {
            var room = _roomRepository
                .GetRooms()
                .SingleOrDefault(r => r.Id == placeUserRequest.RoomId);

            return room is null || coordinateY <= room.SizeY;
        };
    }

    private Func<int, bool> BeValidRoomId()
    {
        return roomId =>
        {
            var room = _roomRepository
                .GetRooms()
                .SingleOrDefault(r => r.Id == roomId);

            return room is not null;
        };
    }

    private Func<int?, bool> BeValidUserId()
    {
        return userId =>
        {
            var uid = userId!.Value;
            var user = _userRepository
                .GetUsers()
                .SingleOrDefault(u => u.Id == uid);

            return user is not null;
        };
    }
}
