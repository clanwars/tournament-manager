using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Messages.Rooms;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations.Room;

public class CreateRoomValidator : AbstractValidator<CreateRoomMessage>
{
    private readonly IEventRepository _eventRepository;

    public CreateRoomValidator(
        IEventRepository eventRepository
    )
    {
        _eventRepository = eventRepository;

        RuleFor(x => x.EventId)
            .Must(IsValidEventId())
            .WithMessage("The event id is unknown");
    }

    private Func<CreateRoomMessage, int, bool> IsValidEventId()
    {
        return (_, eventId) =>
        {
            var @event = _eventRepository
                .GetEvents()
                .SingleOrDefault(e => e.Id == eventId);

            return @event is not null;
        };
    }
}
