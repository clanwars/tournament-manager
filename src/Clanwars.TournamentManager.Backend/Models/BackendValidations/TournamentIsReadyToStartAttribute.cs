using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class TournamentIsReadyToStartAttribute : ValidationAttribute
{
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null)
        {
            return ValidationResult.Success;
        }

        var tournamentId = (int) value;

        var repository = validationContext.GetRequiredService<ITournamentRepository>();
        var element = repository
            .GetTournaments()
            .SingleOrDefault(t => t.Id == tournamentId);
        if (element is null) return ValidationResult.Success;
        if (element.State != TournamentState.OpenForRegistration) return new ValidationResult(GetErrorMessageState(tournamentId));

        if (element.PlayerPerTeam is not null && element.Teams.Any(t => t.Members.Count == 0 || t.Members.Count(m => m.Permission != EntityPermission.Pending) != element.PlayerPerTeam))
        {
            return new ValidationResult(GetErrorMessageTeamPlayersCount(element));
        }

        if (element.Mode is TournamentMode.DoubleElimination or TournamentMode.SingleElimination && element.Teams.Count < 3)
        {
            return new ValidationResult(GetErrorMessageInsufficientTeams(element));
        }

        return ValidationResult.Success;
    }

    private static string GetErrorMessageTeamPlayersCount(Tournament tournament)
    {
        return $"The tournament {tournament.Name} cannot be started as not all teams are complete.";
    }


    private static string GetErrorMessageInsufficientTeams(Tournament tournament)
    {
        return $"The tournament {tournament.Name} cannot be started as there are not enough teams. Double and single elimination tournaments need at least 3 teams.";
    }

    private static string GetErrorMessageState(int tournamentId)
    {
        return $"The tournament with the id {tournamentId} is not in the state 'OpenForRegistration' and thus cannot be started now.";
    }
}
