using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Lib.Messages.Events;

namespace Clanwars.TournamentManager.Backend.Models;

public record UpdateEventRequest : UpdateEventMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public int EventId { get; set; }
}
