using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Lib.Messages.Clans;

namespace Clanwars.TournamentManager.Backend.Models;

public record ClanUpdateRequest : UpdateClanMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public int ClanId { get; set; }
}
