using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Backend.Entities;

public class PipelineModuleState
{
    [Key]
    public int Id { get; set; }

    public ModuleType Module { get; set; }

    public bool IsEnabled { get; set; }

    [Required]
    [Range(10, 3600)]
    public uint TimeoutInitial { get; set; }

    [Required]
    [Range(10, 3600)]
    public uint TimeoutRecurring { get; set; }
}
