namespace Clanwars.TournamentManager.Backend.Entities.Tournaments;

public class TournamentMatch
{
    public int Id { get; set; }

    public int TournamentRoundId { get; set; }

    public TournamentRound TournamentRound { get; set; } = null!;

    public int MatchIndex { get; set; }

    public IList<TournamentMatchScore> Scores { get; set; } = new List<TournamentMatchScore>();
}

public static class TournamentMatchExtensions
{
    public static bool IsFinishedMatch(this TournamentMatch tournamentMatch)
    {
        return tournamentMatch.Scores.All(s => s.Score != null);
    }

    public static bool IsByeMatch(this TournamentMatch tournamentMatch)
    {
        return tournamentMatch.Scores.Any(tms => tms.TournamentTeam.IsByeTeam);
    }
}
