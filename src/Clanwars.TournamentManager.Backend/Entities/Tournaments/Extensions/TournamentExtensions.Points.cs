using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments.Extensions;

public static partial class TournamentExtensions
{
    public static void GeneratePointsMatches(this Tournament tournament)
    {
        var shuffledTeams = ShuffleTeams(tournament.Teams);

        // Tournament mode Points only has one Round
        var rounds = new List<TournamentRound>
        {
            new()
            {
                Matches = new List<TournamentMatch>
                {
                    // Tournament mode Points only has "one match"
                    new()
                    {
                        MatchIndex = 0,
                        Scores = shuffledTeams.Select(t => new TournamentMatchScore
                        {
                            TournamentTeamId = t.Id
                        }).ToList()
                    }
                },
                Order = 1.0,
                Type = TournamentRoundType.Final
            }
        };

        tournament.Rounds = rounds;
    }
}
