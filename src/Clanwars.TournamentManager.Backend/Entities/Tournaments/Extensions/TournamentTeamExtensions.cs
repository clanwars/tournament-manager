using Clanwars.TournamentManager.Lib.Enums;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments.Extensions;

public static class TournamentTeamExtensions
{
    public static bool IsOwner(this TournamentTeam tournamentTeam, User user)
    {
        return tournamentTeam.Members.Any(ttm => ttm.MemberId.Equals(user.Id) && ttm.Permission == EntityPermission.Owner);
    }

    public static bool IsJoinPending(this TournamentTeam tournamentTeam, User user)
    {
        return tournamentTeam.Members.Any(ttm => ttm.MemberId.Equals(user.Id) && ttm.Permission == EntityPermission.Pending);
    }

    public static bool HasPendingJoins(this TournamentTeam tournamentTeam)
    {
        return tournamentTeam.Members.Any(ttm => ttm.Permission == EntityPermission.Pending);
    }

    public static TournamentTeamMember? GetTournamentTeamMember(this TournamentTeam tournamentTeam, int userId)
    {
        return tournamentTeam.Members.SingleOrDefault(ttm => ttm.MemberId == userId);
    }
}
