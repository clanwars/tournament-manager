using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments.Extensions;

public static partial class TournamentExtensions
{
    public static void GenerateDoubleEliminationMatches(this Tournament tournament)
    {
        var roundsCount = Convert.ToInt32(Math.Log(tournament.Teams.Count, 2)) + 1;

        var shuffledTeams = ShuffleTeams(tournament.Teams);

        // Round 0
        var rounds = new List<TournamentRound>
        {
            new()
            {
                Matches = new List<TournamentMatch>(),
                Order = 1.0,
                Type = TournamentRoundType.WinnerBracket
            }
        };

        for (var i = 0; i < shuffledTeams.Length; i += 2)
        {
            rounds[0].Matches.Add(new TournamentMatch
            {
                MatchIndex = rounds[0].Matches.Count,
                Scores = new List<TournamentMatchScore>
                {
                    new()
                    {
                        TournamentTeamId = shuffledTeams[i].Id
                    },
                    new()
                    {
                        TournamentTeamId = shuffledTeams[i + 1].Id
                    }
                }
            });
        }

        var matchIndex = rounds[0].Matches.Count;
        if (roundsCount > 2)
        {
            // second round
            var winnerRound = new TournamentRound
            {
                Matches = new List<TournamentMatch>(),
                Order = 2,
                Type = TournamentRoundType.WinnerBracket
            };
            rounds.Add(winnerRound);
            var loserRound = new TournamentRound
            {
                Matches = new List<TournamentMatch>(),
                Order = 2,
                Type = TournamentRoundType.LoserBracket
            };
            rounds.Add(loserRound);
            var loserRound2 = new TournamentRound
            {
                Matches = new List<TournamentMatch>(),
                Order = 2.5,
                Type = TournamentRoundType.LoserBracket
            };
            rounds.Add(loserRound2);

            // winner bracket
            for (var i = 0; i < shuffledTeams.Length / 2; i++)
            {
                var m = new TournamentMatch
                {
                    MatchIndex = matchIndex++,
                    Scores = new List<TournamentMatchScore>
                    {
                        new()
                        {
                            PreviousMatchIndex = i,
                            PreviousMatchRank = MatchRank.Winner
                        },
                        new()
                        {
                            PreviousMatchIndex = ++i,
                            PreviousMatchRank = MatchRank.Winner
                        }
                    }
                };
                winnerRound.Matches.Add(m);
            }

            // loser bracket 2.0
            for (var i = 0; i < shuffledTeams.Length / 2; i++)
            {
                var m = new TournamentMatch
                {
                    MatchIndex = matchIndex++,
                    Scores = new List<TournamentMatchScore>
                    {
                        new()
                        {
                            PreviousMatchIndex = i,
                            PreviousMatchRank = MatchRank.Loser
                        },
                        new()
                        {
                            PreviousMatchIndex = ++i,
                            PreviousMatchRank = MatchRank.Loser
                        }
                    }
                };
                loserRound.Matches.Add(m);
            }

            // loser bracket 2.5
            var loserRoundCount = loserRound.Matches.Count;
            for (var i = 0; i < loserRoundCount; i++)
            {
                var m = new TournamentMatch
                {
                    MatchIndex = matchIndex++,
                    Scores = new List<TournamentMatchScore>
                    {
                        new()
                        {
                            PreviousMatchIndex = i + shuffledTeams.Length / 2,
                            PreviousMatchRank = MatchRank.Loser
                        },
                        new()
                        {
                            PreviousMatchIndex = i + shuffledTeams.Length / 2 + shuffledTeams.Length / 2 / 2,
                            PreviousMatchRank = MatchRank.Winner
                        }
                    }
                };
                loserRound2.Matches.Add(m);
            }
        }

        if (roundsCount > 3)
        {
            // subsequent rounds
            for (var i = 0; i < roundsCount - 3; i++)
            {
                var matchesCountRound = (int) Math.Pow(2, roundsCount - 3 - i);
                var matchesCountTotalBefore = rounds.Take(rounds.Count - 3).Sum(r => r.Matches.Count);
                var matchesCountTotal = rounds.Sum(r => r.Matches.Count);

                var winnerRound = new TournamentRound
                {
                    Matches = new List<TournamentMatch>(),
                    Order = 3 + i,
                    Type = TournamentRoundType.WinnerBracket
                };
                rounds.Add(winnerRound);
                var loserRound = new TournamentRound
                {
                    Matches = new List<TournamentMatch>(),
                    Order = 3 + i,
                    Type = TournamentRoundType.LoserBracket
                };
                rounds.Add(loserRound);
                var loserRound2 = new TournamentRound
                {
                    Matches = new List<TournamentMatch>(),
                    Order = 3.5 + i,
                    Type = TournamentRoundType.LoserBracket
                };
                rounds.Add(loserRound2);

                // winner bracket
                for (var j = 0; j < matchesCountRound; j++)
                {
                    var m = new TournamentMatch
                    {
                        MatchIndex = matchIndex++,
                        Scores = new List<TournamentMatchScore>
                        {
                            new()
                            {
                                PreviousMatchIndex = j + matchesCountTotalBefore,
                                PreviousMatchRank = MatchRank.Winner
                            },
                            new()
                            {
                                PreviousMatchIndex = ++j + matchesCountTotalBefore,
                                PreviousMatchRank = MatchRank.Winner
                            }
                        }
                    };
                    winnerRound.Matches.Add(m);
                }

                // loser bracket X.0
                for (var j = 0; j < matchesCountRound; j++)
                {
                    var m = new TournamentMatch
                    {
                        MatchIndex = matchIndex++,
                        Scores = new List<TournamentMatchScore>
                        {
                            new()
                            {
                                PreviousMatchIndex = j + matchesCountTotalBefore + matchesCountRound * 2,
                                PreviousMatchRank = MatchRank.Winner
                            },
                            new()
                            {
                                PreviousMatchIndex = ++j + matchesCountTotalBefore + matchesCountRound * 2,
                                PreviousMatchRank = MatchRank.Winner
                            }
                        }
                    };
                    loserRound.Matches.Add(m);
                }

                // loser bracket X.5
                for (var j = 0; j < matchesCountRound / 2; j++)
                {
                    var m = new TournamentMatch
                    {
                        MatchIndex = matchIndex++,
                        Scores = new List<TournamentMatchScore>
                        {
                            new()
                            {
                                PreviousMatchIndex = j + matchesCountTotal,
                                PreviousMatchRank = MatchRank.Loser
                            },
                            new()
                            {
                                PreviousMatchIndex = j + matchesCountTotal + matchesCountRound / 2,
                                PreviousMatchRank = MatchRank.Winner
                            }
                        }
                    };
                    loserRound2.Matches.Add(m);
                }
            }
        }

        // finals
        if (roundsCount > 1)
        {
            var winnerRound = new TournamentRound
            {
                Matches = new List<TournamentMatch>(),
                Order = rounds.Count / 3 + 2,
                Type = TournamentRoundType.Final
            };
            rounds.Add(winnerRound);

            var m = new TournamentMatch
            {
                MatchIndex = matchIndex,
                Scores = new List<TournamentMatchScore>
                {
                    new()
                    {
                        PreviousMatchIndex = matchIndex - 3,
                        PreviousMatchRank = MatchRank.Winner
                    },
                    new()
                    {
                        PreviousMatchIndex = matchIndex - 1,
                        PreviousMatchRank = MatchRank.Winner
                    }
                }
            };
            winnerRound.Matches.Add(m);
        }

        tournament.Rounds = rounds;
    }
}
