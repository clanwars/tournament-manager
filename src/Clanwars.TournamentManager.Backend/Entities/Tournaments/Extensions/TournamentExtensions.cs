namespace Clanwars.TournamentManager.Backend.Entities.Tournaments.Extensions;

public static partial class TournamentExtensions
{
    private static TournamentTeam[] ShuffleTeams(IEnumerable<TournamentTeam> teamsList)
    {
        var rand = new Random();

        var tournamentTeams = teamsList as TournamentTeam[] ?? teamsList.ToArray();

        var shuffledTeams = tournamentTeams.OrderBy(_ => rand.Next()).ToArray();
        // check if not two Bye Teams are going to play vs each other
        var doubleByes = false;
        for (var i = 0; i < shuffledTeams.Length; i += 2)
        {
            if (shuffledTeams[i].IsByeTeam && shuffledTeams[i + 1].IsByeTeam)
            {
                doubleByes = true;
            }
        }

        if (doubleByes)
        {
            shuffledTeams = ShuffleTeams(tournamentTeams);
        }

        return shuffledTeams;
    }
}
