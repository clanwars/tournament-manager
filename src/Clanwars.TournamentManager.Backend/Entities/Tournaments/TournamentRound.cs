using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments;

public class TournamentRound
{
    public int Id { get; set; }
    public double Order { get; set; }
    public TournamentRoundType? Type { get; set; }
    public IList<TournamentMatch> Matches { get; set; } = new List<TournamentMatch>();

    public int TournamentId { get; set; }

    public Tournament Tournament { get; set; } = null!;
}
