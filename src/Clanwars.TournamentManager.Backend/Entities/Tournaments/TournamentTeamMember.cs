using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments;

public class TournamentTeamMember
{
    public int Id { get; set; }

    public TournamentTeam TournamentTeam { get; set; } = null!;

    [Required]
    public int TournamentTeamId { get; set; }

    public User Member { get; set; } = null!;

    [Required]
    public int MemberId { get; set; }

    public EntityPermission Permission { get; set; }
}
