using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments;

public class TournamentTeam
{
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public string Name { get; set; } = null!;

    public Image? Image { get; set; }
    public int? ImageId { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public int TournamentId { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public bool Sealed { get; set; }

    public Tournament Tournament { get; set; } = null!;

    public IList<TournamentTeamMember> Members { get; set; } = new List<TournamentTeamMember>();

    public virtual bool IsByeTeam => Name.StartsWith(TournamentTeamResponseExtensions.ByeTeamPrefix);
    public virtual bool IsSinglePersonTeam => Name.StartsWith(TournamentTeamResponseExtensions.SinglePersonTeamPrefix);
}
