using System.ComponentModel.DataAnnotations;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

public class EventUser
{
    public int Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public int UserId { get; set; }

    public User User { get; set; } = null!;

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public int EventId { get; set; }

    public Event Event { get; set; } = null!;

    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime? CheckinTime { get; set; }
}
