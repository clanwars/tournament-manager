using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

[AdditionalFilterableEntities(nameof(SieveCustomFilterMethods.ParticipatingEventId))]
public class User
{
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    [Required]
    [EmailAddress]
    [MaxLength(255)]
    public string Email { get; set; } = null!;

    [Sieve(CanFilter = true, CanSort = true)]
    [MemberNotNull]
    [MaxLength(255)]
    public string? FirstName { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    [MemberNotNull]
    [MaxLength(255)]
    public string? LastName { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    [MemberNotNull]
    [MaxLength(255)]
    public string? Nickname { get; set; }

    public Image? Image { get; set; }
    public int? ImageId { get; set; }

    public Clan? Clan { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public int? ClanId { get; set; }

    public IList<EventUser> Events { get; set; } = new List<EventUser>();
    
    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime CreatedAt { get; set; }
    
    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime? UpdatedAt { get; set; }

    public bool IsIncompleteProfile() => FirstName is null or "" || LastName is null or "" || Nickname is null or "";
}
