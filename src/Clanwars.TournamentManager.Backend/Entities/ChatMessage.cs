using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Backend.Entities;

public class ChatMessage
{
    public Guid Id { get; set; } = Guid.Empty;

    [Required]
    public DateTime Date { get; set; }

    [Required]
    public string Message { get; set; } = null!;

    [Required]
    public int SenderId { get; set; }

    public User Sender { get; set; } = null!;

    [Required]
    public int ReceiverId { get; set; }

    public User Receiver { get; set; } = null!;
}
