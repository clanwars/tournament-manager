using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

public class Event
{
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    [MaxLength(255)]
    public string Name { get; set; } = null!;

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime StartDate { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime EndDate { get; set; }

    public IList<EventUser> Participants { get; set; } = new List<EventUser>();

    public IList<Tournament> Tournaments { get; set; } = new List<Tournament>();
}
