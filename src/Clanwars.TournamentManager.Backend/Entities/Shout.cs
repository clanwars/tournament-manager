using System.ComponentModel.DataAnnotations;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

public class Shout
{
    [Key]
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public int UserId { get; set; }

    public User User { get; set; } = null!;

    public string Text { get; set; } = null!;

    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime CreatedAt { get; set; }
}
