using System.ComponentModel.DataAnnotations;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

public class News
{
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Required]
    [StringLength(255, MinimumLength = 8)]
    [Sieve(CanFilter = true, CanSort = true)]
    public string Headline { get; set; } = null!;

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime CreatedAt { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public User Author { get; set; } = null!;

    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime? UpdatedAt { get; set; }

    [Required]
    [Sieve(CanFilter = true)]
    public string Body { get; set; } = null!;
}
