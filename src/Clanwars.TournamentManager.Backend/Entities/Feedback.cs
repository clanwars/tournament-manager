using System.ComponentModel.DataAnnotations;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

public class Feedback
{
    [Key]
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public int UserId { get; set; }

    public User User { get; set; } = null!;

    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime CreatedAt { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public int Rating { get; set; }

    [MaxLength(4096)]
    public string Features { get; set; } = null!;

    [MaxLength(8192)]
    public string GoodBad { get; set; } = null!;

    public bool Resolved { get; set; } = false;
}
