using System.ComponentModel.DataAnnotations;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities.Rooms;

public class Room
{
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public string Name { get; set; } = null!;

    [Required]
    public int SizeX { get; set; }

    [Required]
    public int SizeY { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public int EventId { get; set; }

    public Event Event { get; set; } = null!;

    public IList<GridSquare> GridSquares { get; set; } = new List<GridSquare>();
}
