using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums.Rooms;

namespace Clanwars.TournamentManager.Backend.Entities.Rooms;

public class GridSquare
{
    public int Id { get; set; }

    [Required]
    public int X { get; set; }

    [Required]
    public int Y { get; set; }

    public GridSquareType Type { get; set; }

    public GridSquareRotation Rotation { get; set; }

    [Required]
    public int RoomId { get; set; }

    public Room Room { get; set; } = null!;

    public int? UserId { get; set; }
    public User? User { get; set; }

    public override string ToString()
    {
        return $"{nameof(GridSquare)}: {nameof(Id)}: {Id}, {nameof(X)}: {X}, {nameof(Y)}: {Y}, {nameof(Type)}: {Type}, {nameof(Rotation)}: {Rotation}, {nameof(RoomId)}: {RoomId}, {nameof(UserId)}: {UserId}";
    }
}
