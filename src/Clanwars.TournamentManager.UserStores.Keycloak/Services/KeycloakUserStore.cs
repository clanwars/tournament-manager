using System.Collections.ObjectModel;
using Clanwars.TournamentManager.Lib.Extensions;
using Clanwars.TournamentManager.Lib.Messages.UserStore;
using Clanwars.TournamentManager.UserStores.Interfaces;
using Flurl.Http;
using Keycloak.Net;
using Keycloak.Net.Models.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Clanwars.TournamentManager.UserStores.Keycloak.Services;

public class KeycloakUserStore : IExternalUserStore
{
    private readonly KeycloakClient _client;
    private readonly KeycloakOptions _keycloakOptions;
    private const string RealmName = "master";

    public KeycloakUserStore(
        IOptions<KeycloakOptions> options
    )
    {
        _keycloakOptions = options.Value;

        if (_keycloakOptions.ServerAddress is null || _keycloakOptions.AdminUser is null || _keycloakOptions.AdminPassword is null || _keycloakOptions.Realm is null)
        {
            throw new ArgumentException("KeycloakOptions are not set up correctly", nameof(options));
        }

        _client = new KeycloakClient(_keycloakOptions.ServerAddress, _keycloakOptions.AdminUser, _keycloakOptions.AdminPassword);
    }

    public async Task<IEnumerable<UserStoreResponse>> GetUsersAsync(string searchString)
    {
        var emailFinder = _client.GetUsersAsync(_keycloakOptions.Realm, email: searchString);
        var firstnameFinder = _client.GetUsersAsync(_keycloakOptions.Realm, firstName: searchString);
        var lastnameFinder = _client.GetUsersAsync(_keycloakOptions.Realm, lastName: searchString);

        var all = await Task.WhenAll(emailFinder, firstnameFinder, lastnameFinder);
        var results = all.SelectMany(u => u).DistinctBy(u => u.Id).OrderBy(u => u.FirstName);

        return results.Select(ku => new UserStoreResponse
        {
            Id = ku.Id,
            FirstName = ku.FirstName,
            LastName = ku.LastName,
            Email = ku.Email,
        });
    }

    public async Task<UserStoreResponse> CreateUserAsync(string firstName, string lastName, string email, string password)
    {
        var user = new User
        {
            UserName = email,
            Email = email,
            FirstName = firstName,
            LastName = lastName,
            EmailVerified = true,
            Enabled = true,
            Credentials = new List<Credentials>
            {
                new()
                {
                    Type = "rawPassword",
                    Value = password
                }
            },
            RequiredActions = new ReadOnlyCollection<string>(new List<string> {"UPDATE_PASSWORD"})
        };
        try
        {
            if (!await _client.CreateUserAsync(_keycloakOptions.Realm, user))
            {
                throw new Exception($"Something went wrong creating a new user in keycloak user store: {user.SerializeToJsonString()}");
            }
        }
        catch (FlurlHttpException ex) when (ex.StatusCode == StatusCodes.Status409Conflict)
        {
            throw new UserAlreadyExistsException($"User with unique name '{user.UserName}' or email address '{user.Email}' already exists", ex);
        }

        var newUserSearch = await _client.GetUsersAsync(_keycloakOptions.Realm, email: user.Email);
        var userSearch = newUserSearch.ToList();
        if (newUserSearch == null || userSearch.Count != 1)
        {
            throw new Exception($"Newly created user cannot be found in user store: {user.SerializeToJsonString()}");
        }

        var newUser = userSearch.First();
        var userResponse = new UserStoreResponse
        {
            Id = newUser.Id,
            Email = newUser.Email,
            FirstName = newUser.FirstName,
            LastName = newUser.LastName
        };

        return userResponse;
    }

    public async Task SetUserPasswordAsync(string userId, string password, bool temporary)
    {
        await _client.ResetUserPasswordAsync(RealmName, userId, password, temporary);
    }
}
