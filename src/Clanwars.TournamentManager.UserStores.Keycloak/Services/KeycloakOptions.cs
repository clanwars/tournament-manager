using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.UserStores.Keycloak.Services;

public record KeycloakOptions
{
    public const string Option = "Keycloak";

    [Required]
    public string ServerAddress { get; set; } = null!;

    [Required]
    public string AdminUser { get; set; } = null!;

    [Required]
    public string AdminPassword { get; set; } = null!;

    [Required]
    public string Realm { get; set; } = null!;
}
