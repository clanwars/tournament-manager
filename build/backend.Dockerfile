FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS runtime
WORKDIR /app

ENV ASPNETCORE_URLS=http://+:5000
ENV ASPNETCORE_ENVIRONMENT=Production


COPY out .

USER $APP_UID

EXPOSE 5000

CMD ["dotnet", "Clanwars.TournamentManager.Backend.dll"]
