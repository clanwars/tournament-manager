FROM docker.io/library/nginx:1.27.4-alpine

COPY ./out/Clanwars.TournamentManager.Frontend/wwwroot /usr/share/nginx/html
COPY ./Frontend/nginx.conf /etc/nginx/nginx.conf
RUN chown -R nginx:nginx /usr/share/nginx/html 
